IWING MoteLib
=============
C library for sensor mote development.

Directory Structure
-------------------

    $(MOTELIB)
      |
      + Makerules -- generic rules to combine with platform-specific make rules
      |
      + include/
      |  |
      |  + motelib/ -- contains headers for MoteLib's main APIs
      |     |
      |     + sys/  -- contains headers for MoteLib's system tasks
      |
      + doxygen/ -- contains configuration for document generation
      |
      + examples/ -- contains example code
      |
      + lib/ -- contains platform-independent system code and libraries
      |
      + platforms/ -- contains platform-dependent code
      |   + iwing-mrf/
      |   + iwing-jn/
      |   + sim/
      |   + :
      |
      + support/ -- contains miscelleneous tools
      |
      + test/ -- contains platform-independent source files for testing
