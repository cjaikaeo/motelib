#!/bin/bash

# Setup the environment variables needed by MoteLib make system and simulation

script_dir=`dirname "${BASH_SOURCE[0]}"`
export MOTELIB_DIR=$(cd "$script_dir"; pwd)
export PYTHONPATH=${PYTHONPATH}:${MOTELIB_DIR}/platforms/sim/python
export PYTHONPATH=${PYTHONPATH}:${MOTELIB_DIR}/lib/python

echo "Variables MOTELIB_DIR and PYTHONPATH have been set for MoteLib..."

#echo "MOTELIB_DIR=$MOTELIB_DIR"
#echo "PYTHONPATH=$PYTHONPATH"
