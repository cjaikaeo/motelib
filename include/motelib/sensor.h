/** 
 * @file
 *
 * @brief Sensor reading API
 *
 * Definitions for for sensor module.
 *
 * @author Chaiporn Jaikaeo <chaiporn.j@ku.ac.th>
 * @author Morakot Saravanee <gu__p@hotmail.com>
 * @author Patra Poome <pop.pp@live.com>
 */

#ifndef __MOTELIB_SENSOR_H__
#define __MOTELIB_SENSOR_H__

#include <stdint.h>
#include <stdbool.h>

// Platform-specific SensorType must be defined in the following header file
#include <platform.h>

/**
 * @typedef SensorStatus
 * Defines sensor-related statuses to be used with function returns or callbacks
 */
typedef enum
{
    SENSOR_OK,   ///< Previous request is ready or successful
    SENSOR_BUSY, ///< Previous request is not ready or not successful
} SensorStatus;

/**
 * Datatype to hold no-return function pointer. 
 * The pointed function is called when sensed value is ready.
 * @param value The returned value of previous sense-request.
 */
typedef void (*SensorDataReady)(uint16_t value);

/**
 * Call to request sensing anlog environment-value.
 * @param sensor Specifies the requested sensor type.
 * @param readDone The pointer to function that be called 
 * when sensed value is ready. 
 *
 * @return Status indicating whether request is successful
 */
SensorStatus sensorRequestAnalog(SensorType sensor, SensorDataReady readDone);

/**
 * Return the most recent analog reading for a particular sensor channel.
 * The function sensorRequestAnalog must be called prior to the invocation of
 * this function.  Otherwise the returned value is meaningless.
 *
 * @param sensor Specifies the requested sensor type.
 */
uint16_t sensorAnalogResult(SensorType sensor);

/**
 * Call to read digital sensing value
 * @param sensor Specifies the requested sensor type.
 * @return Digital reading value
 */
uint8_t sensorReadDigital(SensorType sensor);

/**
 * Determine whether sensor on a specified channel is still waiting for data
 * @param sensor
 * Specifies the requested sensor type.
 *
 * @retval true the sensor is not done reading
 * @retval false the sensor is done reading
 *
 */
bool sensorAnalogWaiting(SensorType sensor);

/**
 * Determine whether sensor on a specified channel is readily available for
 * read without waiting
 * @param sensor
 * Specifies the requested sensor type.
 *
 * @retval true the sensor is available
 * @retval false the sensor is not available
 *
 */
bool sensorAnalogAvailable(SensorType sensor);

#endif
