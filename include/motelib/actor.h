/** 
 * @file
 * @brief Actor Control API
 *
 * Definitions for (digital) actor control.
 *
 * @author Chaiporn Jaikaeo <chaiporn.j@ku.ac.th>
 * @author Morakot Saravanee <gu__p@hotmail.com>
 * @author Patra Poome <pop.pp@live.com>
 */

#ifndef __MOTELIB_ACTOR_H__
#define __MOTELIB_ACTOR_H__

#include <stdint.h>

// Platform-specific ActorType must be defined in the following header file
#include <platform.h>

/**
 * Change state of specified actor
 * @param actor Specifies the desired actor type
 * @param state Specifies the logic level to be sent to the actor
 */
void actorSetState(ActorType actor, uint8_t state);

/**
 * Check the current state of an actor pin
 * @param actor Specifies the desired actor type
 * @return current state of the actor
 */
uint8_t actorGetState(ActorType actor);

#endif
