/** 
 * @file
 *
 * @brief UART access API
 *
 * Definitions for sending and receiving data via UART interface.
 *
 * @author Chaiporn Jaikaeo <chaiporn.j@ku.ac.th>
 */
#ifndef __MOTELIB_UART_H__
#define __MOTELIB_UART_H__

#include <stdint.h>
#include <stdbool.h>

#include "system.h"

/**
 * Defines the type for event handler function called when UART has done
 * reading
 *
 * @param markerFound
 * Indicating whether a certain character (i.e, marker) being watched has been
 * encountered in the input buffer
 */
typedef void (*UartInputReady)(uint8_t markerFound);

/**
 * Enables or disables UART TX/RX modules
 *
 * @param tx
 * Enable or disable TX module
 *
 * @param rx
 * Enable or disable RX module
 */
void uartEnable(bool tx, bool rx);

/**
 * Accepts a request to write a single byte to UART
 * @param data Data byte to be written
 *
 * @return
 * actual number of bytes written
 *
 */
uint8_t uartWriteByte(uint8_t data);

/**
 * Accepts a request to write a byte sequence to UART
 * @param data Pointer to the buffer holding data to be written
 * @param len Number of bytes to be written
 *
 * @return
 * actual number of bytes written
 *
 */
uint16_t uartWrite(void* data, uint16_t len);

/**
 * Sets a handler for monitoring UART input buffer until specified number of
 * bytes are availble.  If a previous watch request is being served, the
 * library will cancel the previous request and proceeds with the new one.
 * Once invoked, the specified callback pointer will be reset.
 *
 * @param len 
 * Length of the data to trigger callback
 *
 * @param inputReady 
 * Function to be called when the specified number of bytes are available in
 * the input buffer
 */
void uartWatchInputBytes(uint16_t len, UartInputReady inputReady);

/**
 * Sets a handler for monitoring UART input buffer until a specified character
 * (i.e., marker) appears in the input stream.  If a previous watch request is
 * being served, the library will cancel the previous request and proceeds
 * with the new one.  Once invoked, the specified callback pointer will be reset.
 *
 * @param marker
 * Character to trigger callback when found in the input buffer
 *
 * @param maxLen 
 * Maximum number of bytes to be read.  If maxLen is zero, callback will not
 * be invoked until marker is encountered.
 *
 * @param inputReady 
 * Function to be called when the specified number of bytes have been read or
 * the specified marker is encountered.
 */
void uartWatchInputMarker(char marker, uint16_t maxLen, UartInputReady inputReady);

/**
 * Returns the number of free bytes available in the output buffer
 */
uint16_t uartOutputFree();

/**
 * Returns the number of bytes in the input buffer waiting to be read
 */
uint16_t uartInputLen();

/**
 * Reads contents from the input buffer
 *
 * @param data
 * Pointer to memory location to store bytes being read
 *
 * @param len
 * Maximum number of bytes to be read from the buffer
 *
 * @return
 * Actual number of bytes that were read
 */
uint16_t uartRead(void *data, uint16_t len);

/**
 * Reads a single byte from the input buffer
 *
 * @return
 * The first byte in the UART input queue, or zero if the input queue is
 * currently empty
 */
uint8_t uartReadByte();

/**
 * Flushes the output buffer
 */
void uartFlushOutput();

/**
 * Flushes the input buffer
 */
void uartFlushInput();

#endif
