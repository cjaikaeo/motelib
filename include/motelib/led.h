/** 
 * @file
 *
 * @brief LED Control API
 *
 * Definitions for controlling LEDs and their status inquiry.
 *
 * @author Chaiporn Jaikaeo <chaiporn.j@ku.ac.th>
 * @author Morakot Saravanee <gu__p@hotmail.com>
 * @author Patra Poome <pop.pp@live.com>
 */
#ifndef __MOTELIB_LED_H__
#define __MOTELIB_LED_H__

#include <stdint.h>

/**
 * Controls the status of leds with status variable.
 * @param no the number of led such as 0,1 and 2.
 * @param status sets for turn on th led and clears for turn off.
 */
void ledSet(uint8_t no, uint8_t status);

/**
 * Called to get the status of leds.
 * @param no the number of led such as 0,1 and 2.
 * @return The status of the led.
 * (the status 0 means 'turning off' and 1 means 'turning on')
 */
uint8_t ledGet(uint8_t no);

/**
 * Called to toggle the status of leds.
 * @param no the number of led such as 0,1 and 2.
 */
void ledToggle(uint8_t no);

/**
 * Displays the 3 LSBs on LEDs.
 * @param val Value whose 3 LSBs are to be output
 */
void ledSetValue(uint8_t val);

#endif
