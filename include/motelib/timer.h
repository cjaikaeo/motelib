/** 
 * @file
 *
 * @brief Virtualized timer API
 *
 * Definitions for virtualized timer module.
 *
 * @author Chaiporn Jaikaeo <chaiporn.j@ku.ac.th>
 * @author Morakot Saravanee <gu__p@hotmail.com>
 * @author Patra Poome <pop.pp@live.com>
 */
#ifndef __MOTELIB_TIMER_H__
#define __MOTELIB_TIMER_H__

#include <stdint.h>
#include <platform.h>

/**
 * @typedef TimerType
 * Represents behavier of timer.
 */
typedef enum
{
    TIMER_ONESHOT,
    TIMER_PERIODIC
} TimerType;

typedef struct Timer Timer;

/**
 * Datatype to hold no-return function pointer. 
 * The pointed function is called when the timer is fired.  Corresponding
 * timer handler will also be passed during callback.
 */
typedef void (*TimerFired)(Timer*);

/**
 * Defines a structure for each timer handler
 */
struct Timer
{
    uint16_t   interval;  ///< Number of total ticks before expiration
    uint16_t   remained;  ///< Number of ticks remained
    TimerFired fired;     ///< Callback when timer expired
    TimerType  type;      ///< Either TIMER_ONESHOT or TIMER_PERIODIC
    Timer     *next;
};

/**
 * Call to create a new timer.
 * @param timer Pointer to timer handler
 */
void timerCreate(Timer *timer);

/**
 * Call to start counting time.
 * @param timer Pointer to timer handler
 * @param type Specifies behavier of the timer such as TIMER_PERIODIC and
 * TIMER_ONESHOT.
 * @param interval The interval of counting time in milliseconds.
 * @param timerFired The pointer to function that be called
 * when the timer is fired.
 */
void timerStart(Timer *timer, TimerType type, uint16_t interval, TimerFired timerFired);

/**
 * Call to stop counting time.
 * @param timer Pointer to timer handler
 */
void timerStop(Timer *timer);

/**
 * Return 16-bit global tick count in milliseconds since booted.  The
 * resolution depends on each platform, as specified by
 * PLATFORM_TIMER_TICK_INTERVAL.
 */
uint16_t timerTicks();

/**
 * Determine whether a given timer has already expired.  This function is
 * meaningful only when the timer type is set to be one-shot.
 * @param t Pointer to timer handler
 */
#define timerExpired(t) ((t)->remained == 0)

/**
 * Compute the amount of error in milliseconds for the given interval.
 * This function is useful to take into account timing inaccuracy due to given
 * interval not divisible by platform's timer resolution.
 */
#define timerError(interval) \
    ((((interval+PLATFORM_TIMER_TICK_INTERVAL/2)/PLATFORM_TIMER_TICK_INTERVAL) \
    * PLATFORM_TIMER_TICK_INTERVAL) - interval)

#endif
