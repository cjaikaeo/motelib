/** 
 * @file
 *
 * @brief MoteLib Main API
 *
 * Definitions for general application commands and callbacks.  It also defines
 * common constants to be used throughout the entire library.
 *
 * @author Chaiporn Jaikaeo <chaiporn.j@ku.ac.th>
 * @author Morakot Saravanee <gu__p@hotmail.com>
 * @author Patra Poome <pop.pp@live.com>
 *
 * @mainpage
 * Welcome to IWING's MoteLib API documentation.  Use the navigation bar on
 * top of the page to browse available functions and classes.
 */
#ifndef __SYSTEM_H__
#define __SYSTEM_H__

#include <stdint.h>
#include "platform.h"

#ifndef NULL
#define NULL  (void*)0 ///< NULL is used for represent the empty pointer.
#endif

typedef uint16_t Address;   ///< Datatype to hold node addresses

typedef uint8_t MessageType; ///< Datatype to hold message type.


/**
 * @typedef LoopRoutinePtr
 * Type for a pointer pointing to a function that is invoked continuously by
 * the main loop
 */
typedef void (*LoopRoutinePtr)();

/**
 * Returns node's current address
 */
Address getAddress();

/**
 * Returns node's current PAN ID
 */
uint16_t getPanId();

/**
 * Returns node's current RF channel
 */
uint8_t getChannel();

#ifdef NDEBUG
#   define debug(fmt,...) ((void)0)
#else
#   ifndef debug  // platform may implement its own debugging system
/**
 * Takes a printf-like format and variable-length arguments, then produces a
 * corresponding debug message to be sent over UART or standard error, or
 * captured and processed by simulator.  All debugging messages may be
 * suppressed by defining the macro NDEBUG.
 *
 * <b>Notes to platform developers:</b> Platforms <em>must</em> check for the
 * definition of the macro NDEBUG and <em>must not</em> define the
 * <tt>debug</tt> function if NDEBUG is defined.  Platforms that do not
 * support debug messages may define an empty function for this one.
 */
void debug(char *fmt, ...);
#   endif
#endif

/**
 * Specifies a routine that is invoked in every iteration of the main loop
 * @param proc Pointer to the function to be invoked
 */
void setLoopRoutine(LoopRoutinePtr proc);

/** 
 * <em>(Must be defined by app programmer)</em> 
 * Called when mote is done with the booting process. 
 * This function usually initializes global variables and creates timers to be
 * used in the application. */
extern void boot();

#endif
