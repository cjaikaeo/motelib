/** 
 * @file
 *
 * @brief Radio transmission and reception API
 *
 * Definitions for radio package transmission and reception.
 *
 * @author Chaiporn Jaikaeo <chaiporn.j@ku.ac.th>
 * @author Morakot Saravanee <gu__p@hotmail.com>
 * @author Patra Poome <pop.pp@live.com>
 */
#ifndef __MOTELIB_RADIO_H__
#define __MOTELIB_RADIO_H__

#include "system.h"
#include <platform.h>

/**
 * Broadcast address.
 */
#define BROADCAST_ADDR ((Address)0xFFFF)

/**
 * Maximum message (payload) length allowed by the platform
 */
#define MAX_MSG_LEN PLATFORM_MAX_MSG_LEN

/**
 * @typedef RadioStatus
 * Defines radio-related statuses to be used with function returns or callbacks
 */
typedef enum
{
    RADIO_OK,     ///< Previous request is ready or successful
    RADIO_FAILED, ///< Previous request is not ready or not successful
} RadioStatus;

/**
 * Datatype to hold no-return function pointer. 
 * The pointed function is called when an acknowledgement is received.
 * @param status The status of lastest sent message.
 */
typedef void (*RadioTxDone)(RadioStatus status);

/**
 * Datatype to hold no-return function pointer.
 * The pointed function is called when a packet is received.
 * @param source The address of source node.
 * @param type Type of message.
 * @param message A message of received packet.
 * @param len Length of message. 
 */
typedef void (*RadioRxHandler)(
        Address source, MessageType type, void *message, uint8_t len);

/**
 * Request a message transmission over radio
 * @param dst The address of destination.
 * Use BROADCAST_ADDR for sending a packet to all node in the network.
 * @param type Type of message.
 * @param msg The pointer to message of packet.
 * @param len Length of message.
 * @param txDone The pointer to function that be called 
 * when an acknowledge of this packet is received.
 * @return The status of sending request. 
 */
RadioStatus radioRequestTx(
        Address dst,
        MessageType type,
        const void *msg,
        uint8_t len,
        RadioTxDone txDone);

/**
 * Request a message transmission over radio with additional header
 * @param dst The address of destination.
 * Use BROADCAST_ADDR for sending a packet to all node in the network.
 * @param type Type of message.
 * @param header The pointer to message's additional header
 * @param headerLen Length of the header
 * @param msg The pointer to message of packet.
 * @param len Length of message.
 * @param txDone The pointer to function that be called 
 * when an acknowledge of this packet is received.
 * @return The status of sending request. 
 */
RadioStatus radioRequestTxWithHeader(
        Address dst,
        MessageType type,
        const void *header,
        uint8_t headerLen,
        const void *msg,
        uint8_t len,
        RadioTxDone txDone);
/**
 * Call to set the pointer to receive handler.
 * @param rxHandler The function pointer to receive handler. 
 */
void radioSetRxHandler(RadioRxHandler rxHandler); 

/**
 * Retrieve status about the most recent transmission
 * @param status A pointer to platform-dependent tx status struct
 */
void radioGetTxStatus(RadioTxStatus* status); 

/**
 * Retrieve status about the most recent reception
 * @param status A pointer to platform-dependent rx status struct
 */
void radioGetRxStatus(RadioRxStatus* status); 

#endif
