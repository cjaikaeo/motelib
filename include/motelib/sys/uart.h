#ifndef __MOTELIB_SYS_UART_H__
#define __MOTELIB_SYS_UART_H__

#include <stdbool.h>

#include "../cirbuf.h"

#ifndef UART_INPUT_BUF_SIZE
#define UART_INPUT_BUF_SIZE 128 ///< UART input buffer size
#endif

#ifndef UART_OUTPUT_BUF_SIZE
#define UART_OUTPUT_BUF_SIZE 128 ///< UART output buffer size
#endif

extern CirBuf  _sys_uart_cb_in;
extern CirBuf  _sys_uart_cb_out;
extern char    _sys_uart_marker;
extern uint8_t _sys_uart_useMarker;

void _platform_uart_init();
void _platform_uart_enable(bool tx, bool rx);
void _sys_uart_init();
void _sys_uart_pollInput();
void _sys_uart_notifyMarkerFound();

#endif
