#ifndef __MOTELIB_SYS_RADIO_H__
#define __MOTELIB_SYS_RADIO_H__

#include <stdint.h>
#include "../radio.h"

//#define TINYOS_IFRAME_TYPE  ///< Interoperable with TinyOS's 6LowPAN

/** 
 * The 6LowPAN NALP ID for a TinyOS network is 63 (TEP 125).
 */
#ifndef TINYOS_6LOWPAN_NETWORK_ID
#define TINYOS_6LOWPAN_NETWORK_ID 0x3f
#endif

/**
 * Number of entries for keeping track of received frames
 * (Must be power of two so it can be optimized by the compiler)
 */
#ifndef RADIO_RX_HISTORY_SIZE
#define RADIO_RX_HISTORY_SIZE  4
#endif

/**
 * TX circular buffer size
 * (Must be power of two for best performance)
 */
#ifndef RADIO_TX_BUF_SIZE
#define RADIO_TX_BUF_SIZE 256
#endif

/**
 * @typedef
 * Keeps information of radio module's various statuses
 */
typedef struct
{
    uint8_t sending:1;    ///< Indicates that the module is sending a frame
    uint8_t receiveing:1; ///< Indicates that the module is receiving a frame
} RadioStackStatus;

/**
 * @typedef
 * Keeps information of received messages for detecting duplicate frames
 */
typedef struct
{
    Address source;  ///< Message's source
    uint8_t frameId; ///< Message's frame ID
} ReceiveInfo;


/**
 * @typedef
 * Defines header fields for every MoteLib message encapsulated in 802.15.4
 * frame
 */
typedef struct __attribute__((packed))
{
#ifdef TINYOS_IFRAME_TYPE
  uint8_t  network;
#endif
  uint8_t  type;
  uint8_t  seqno;
} MessageHeader;

///////////////////////////////////////////////////////////////////////////
// Variables defined by the system and to be shared with platform-specific
// code
///////////////////////////////////////////////////////////////////////////
extern RadioRxStatus _platform_radio_rxStatus;
extern RadioTxStatus _platform_radio_txStatus;

void _sys_radio_init();
void _sys_radio_sendDone(RadioStatus status);
void _sys_radio_receiveDone(Address src, void *msg, uint8_t len);
void _sys_radio_pollTxQueue();

/**
 * Initialize platform-dependent radio subsystem
 * (To be implemented by platform)
 */
void _platform_radio_init();

/**
 * Sends a raw packet prepared by MoteLib over RF
 * (To be implemented by platform)
 * @param dst Destination address
 * @param header Pointer to MoteLib's message header
 * @param data Pointer to user data
 * @param dlen Length of user data
 */
void _platform_radio_send(
        Address dst,
        MessageHeader *header,
        void const *data,
        uint8_t len);
#endif
