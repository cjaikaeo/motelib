#ifndef __MOTELIB_SYS_STORAGE_H__
#define __MOTELIB_SYS_STORAGE_H__

#include <stdint.h>
#include <stdbool.h>

/*
 * Definitions expected from platform.h
 * - StorageAddr data type for addressing a byte in the storage
 */
#include <platform.h>

/**
 * Give total size of persistent storage in the node
 * @retval 0 if the node has no persistent storage
 * @retval bytes the total number of bytes
 */
extern StorageAddr _platform_storage_size;

/**
 * Determine whether the storage subsystem is ready for access
 * @retval true the storage is ready for access
 * @retval false the storage is not ready for access
 */
bool _platform_storage_is_ready();

/**
 * Write a single byte to the specified address
 *
 * The request will have no effect if _platform_storage_is_ready() is still
 * false, or the specified addr is not in a valid range.
 *
 * @param addr Storage address to which the specified byte value will be
 * written
 *
 * @param byte Byte value to be written
 */
void _platform_storage_write_byte(StorageAddr addr, uint8_t byte);

/**
 * Read a single byte from the specified address
 *
 * The request will  if _platform_storage_is_ready() is still
 * false, or the specified addr is not in a valid range.
 *
 * @param addr Storage address from which byte value will be retrieved
 * @param byte Pointer to memory location where the retrieved byte value will
 * be stored
 */
void _platform_storage_read_byte(StorageAddr addr, uint8_t *byte);

#endif
