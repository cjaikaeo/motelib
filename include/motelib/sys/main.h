#ifndef __MOTELIB_SYS_MAIN_H__
#define __MOTELIB_SYS_MAIN_H__

#include "../system.h"

extern Address  _platform_address; ///< Global variable holding node address
extern uint16_t _platform_panid;   ///< Global variable holding node's PAN ID
extern uint8_t  _platform_channel; ///< Global variable holding RF channel

void _sys_init_routine();
void _sys_main_routine();
void _sys_timer_init();
void _sys_led_init();
void _sys_radio_init();
void _sys_uart_init();

#endif
