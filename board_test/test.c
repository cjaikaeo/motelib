/** 
 * @file
 *
 * @brief Test basic functionalities of IWING-MRF board
 *
 * This firmware interacts with the user via UART.  Functionalities to be
 * tested include:
 *    - UART
 *    - LEDs
 *    - Sensor inputs
 *    - Radio TX/RX
 */
#include <stdio.h>
#include <string.h>

#include <motelib/system.h>
#include <motelib/button.h>
#include <motelib/uart.h>
#include <motelib/sensor.h>
#include <motelib/actor.h>
#include <motelib/led.h>
#include <motelib/radio.h>

#include "pt/pt.h"

#if defined(PLATFORM_IWING_JN)
#define SENSOR_DIGITAL_START SENSOR_D0
#define SENSOR_DIGITAL_COUNT 18
#define SENSOR_ANALOG_START  SENSOR_A1
#define SENSOR_ANALOG_COUNT  6
#define ACTOR_START  ACTOR_0
#define ACTOR_COUNT  18
#elif defined(PLATFORM_IWING_MRF)
#define SENSOR_DIGITAL_START SENSOR_0
#define SENSOR_DIGITAL_COUNT 6
#define SENSOR_ANALOG_START  SENSOR_0
#define SENSOR_ANALOG_COUNT  6
#define ACTOR_START  ACTOR_0
#define ACTOR_COUNT  6
#else
#define SENSOR_DIGITAL_START 0
#define SENSOR_DIGITAL_COUNT 6
#define SENSOR_ANALOG_START  0
#define SENSOR_ANALOG_COUNT  6
#define ACTOR_START  0
#define ACTOR_COUNT  3
#endif

struct pt sensor_pt;

//////////////////////////////////////////////////
#ifdef PLATFORM_IWING_MRF
#include <avr/pgmspace.h>
#define printf(fmt,args...) printf_P(PSTR(fmt),##args)
#endif

//////////////////////////////////////////////////
#define READ_ANALOG(sensor, stored_var) \
    sensorRequestAnalog(sensor, NULL); \
    PT_WAIT_WHILE(pt, sensorAnalogWaiting(sensor)); \
    stored_var = sensorAnalogResult(sensor);

//////////////////////////////////////////////////
#define WAIT_INPUT(stored_var) \
    PT_WAIT_UNTIL(pt, uartInputLen() > 0); \
    stored_var = uartReadByte();

char *MSG = "Hello";
char *actorKeys = "0123456789abcdefghijklmn";


//////////////////////////////////////////////////
void buttonHandler(ButtonStatus status)
{
    if (status == BUTTON_PRESSED)
        printf("USER BUTTON is pressed.\r\n");
    else
        printf("USER BUTTON is released.\r\n");
}

//////////////////////////////////////////////////
void rxHandler(Address source, MessageType type, void *message, uint8_t len)
{
    RadioRxStatus rxStat;

#if defined(PLATFORM_IWING_MRF)
    radioGetRxStatus(&rxStat);
    printf("Received pkt from Node ID %u, type=%d, len=%d, rssi=%d\r\n",
            source, type, len, rxStat.rssi);
#else
    printf("Received pkt from Node ID %u, type=%d, len=%d\r\n",
            source, type, len);
#endif
}

//////////////////////////////////////////////////
PT_THREAD(sensorTask(struct pt *pt))
{
    static uint8_t i, msgType;
    static uint8_t MSG_LEN;
    char key;
    uint16_t val;

    PT_BEGIN(pt);

    MSG_LEN = strlen(MSG);

    while (1)
    {
        printf(
                "\r\n"
                "Main Menu\r\n"
                "1. Test LEDs\r\n"
                "2. Test button\r\n"
                "3. Test digital sensor pins\r\n"
                "4. Test analog sensor pins\r\n"
                "5. Test radio\r\n"
                "6. Test actor pins\r\n"
                "> "
              );
        WAIT_INPUT(key);
        printf("%c\r\n", key);

        ///////////////////
        // Test LEDs
        ///////////////////
        if (key == '1')
        {
            printf("Press '0','1','2' to toggle LED; 'q' to quit\r\n");
            while (1)
            {
                // Show current status of LEDs
                for (i = 0; i < 3; i++)
                    printf("%d->%-3s  ", i, ledGet(i) ? "ON" : "OFF");
                printf("\r\n");
                WAIT_INPUT(key);
                if (key == 'q') break;
                if ((key < '0') || (key > '2')) continue;
                key = key - '0';
                ledToggle(key);
            }
        }

        ///////////////////
        // Test user button
        ///////////////////
        else if (key == '2')
        {
            printf("Press and release button on board; 'q' to quit\r\n");
            buttonSetHandler(buttonHandler);
            while (1)
            {
                WAIT_INPUT(key);
                if (key == 'q') break;
            }
            buttonSetHandler(NULL);
        }

        ///////////////////////////////
        // Test sensor pins (digital)
        ///////////////////////////////
        else if (key == '3')
        {
            static uint8_t sensorId;

            for (i = 0; i < SENSOR_DIGITAL_COUNT; i++)
            {
                sensorId = i + SENSOR_DIGITAL_START;
                if ( i && (i%8 == 0) ) printf("\r\n");
                printf("S%02d->%d ", sensorId, sensorReadDigital(sensorId));
            }
            printf("\r\n");
        }

        ///////////////////////////////
        // Test sensor pins (analog)
        ///////////////////////////////
        else if (key == '4')
        {
            static uint8_t sensorId;

#ifdef PLATFORM_IWING_JN
            printf("----\r\n");
            printf("S00 <-> ADC1\r\n");
            printf("S01 <-> ADC2\r\n");
            printf("S02 <-> ADC3\r\n");
            printf("S03 <-> ADC4\r\n");
            printf("S04 <-> Internal Temperature\r\n");
            printf("S05 <-> Internal Voltage\r\n");
            printf("----\r\n");
#endif

            // Read all sensors
            for (i = 0; i < SENSOR_ANALOG_COUNT; i++)
            {
                sensorId = i + SENSOR_ANALOG_START;
                READ_ANALOG(sensorId, val);
                if ( i && (i%8 == 0) ) printf("\r\n");
                printf("S%02d->%d ", i, val);
            }
            printf("\r\n");
        }

        ///////////////////////////////
        // Test sensor pins (digital)
        ///////////////////////////////
        else if (key == '6')
        {
            static uint8_t actorId;

            printf("----\r\n");
            printf("Press a following key to toggle an actor; 'q' to quit\r\n");
            for (i = 0; i < ACTOR_COUNT; i++)
            {
                if ( i && (i%8 == 0) ) printf("\r\n");
                printf("'%c'-A%02d ", actorKeys[i], i);
            }
            printf("\r\n----\r\n");
            while (1)
            {
                // Show current status of actors
                for (i = 0; i < ACTOR_COUNT; i++)
                {
                    actorId = i + ACTOR_START;
                    if ( i && (i%8 == 0) ) printf("\r\n");
                    printf("A%02d->%-3s ", actorId, actorGetState(actorId) ? "ON" : "OFF");
                }
                printf("\r\n");
                WAIT_INPUT(key);
                if (key == 'q') break;
                if ((key >= '0') && (key <= '9'))
                    key = key - '0';
                else if (key >= 'a')
                    key = key - 'a' + 10;
                else
                    continue;

                if (key >= ACTOR_COUNT) continue;

                actorId = key+ACTOR_START;
                actorSetState(actorId, !actorGetState(actorId));
            }
        }

        ///////////////////
        // Test radio
        ///////////////////
        else if (key == '5')
        {
            printf("Radio settings: PANID=%u, NodeID=%u, Channel=%d\r\n",
                    getPanId(), getAddress(), getChannel());

            printf("Press 's' to send a broadcast packet; 'q' to exit\r\n");
            radioSetRxHandler(rxHandler); 
            msgType = 0;
            while (1)
            {
                WAIT_INPUT(key);
                if (key == 'q') break;
                if (key == 's')
                {
                    printf("Broadcast message of type %u, with len %d\r\n",
                            msgType, MSG_LEN);

                    radioRequestTx(
                            BROADCAST_ADDR,
                            msgType,
                            MSG,
                            MSG_LEN,
                            NULL);

                    msgType++;
                }
            }

            radioSetRxHandler(NULL); 
        }
    }

    PT_END(pt);
}

//////////////////////////////////////////////////
void scheduleTasks()
{
    sensorTask(&sensor_pt);
}

//////////////////////////////////////////////////
void boot()
{
    PT_INIT(&sensor_pt);
    uartEnable(true, true);  // tx - enabled, rx - enabled
    setLoopRoutine(scheduleTasks);
}
