#include <motelib/system.h>
#include <motelib/led.h>
#include <motelib/timer.h>
#include <motelib/radio.h>

typedef struct
{
    uint8_t counter;
} Data;

Timer t1;
Data data;

void timer1Fired(Timer *t)
{
    data.counter++;
    radioRequestTx(BROADCAST_ADDR, 0, (char*)&data, sizeof(data), NULL);
    ledSetValue(data.counter);
}

void receive(Address source,MessageType type,void *message,uint8_t len)
{
    ledSetValue(((Data*)message)->counter);
}

void boot()
{
    if (getAddress() == 0)
    {
        // Node ID 0 keeps counting and broadcasts current counter value
        data.counter = 0;
        timerCreate(&t1);
        timerStart(&t1, TIMER_PERIODIC, 500, timer1Fired);
    }
    else
        // Other nodes display 3 LSBs on LEDs
        radioSetRxHandler(receive);
}

