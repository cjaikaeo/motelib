#include <string.h>

#include <motelib/system.h>
#include <motelib/led.h>
#include <motelib/timer.h>
#include <motelib/radio.h>
#include <motelib/sensor.h>
#include <motelib/actor.h>

#define MSG_TYPE 2

Timer t1;

void sendDone(RadioStatus s)
{
    ledToggle(0);
}

void senseDone(uint16_t val)
{
    static uint16_t lightVal;

    // Record light reading and request radio send
    lightVal = val;
    radioRequestTx(0xFFFF, 1, (char*)&lightVal, 2, sendDone);

    // Deactivate the sensor controller
    actorSetState(0,0);
}

void timer1Fired(Timer *t)
{
    // activate the actor that controls the sensors on the sensor board
    actorSetState(0,1);

    // Start sensing light
    sensorRequestAnalog(1, senseDone);
}

void receive(Address source,MessageType type,void *message,uint8_t len)
{
    uint16_t val;

    // Display the 3 most-significant bits on LEDs
    memcpy(&val, message, 2);
    ledSetValue(val >> 7);
}

void boot()
{
    // Only node with ID 0 senses and broadcasts light reading periodically
    if (getAddress() == 0)
    {
        timerCreate(&t1);
        timerStart(&t1, TIMER_PERIODIC, 500, timer1Fired);
    }
    else
    {
        // Other nodes receive and display reading on LEDs
        radioSetRxHandler(receive);
    }
}

