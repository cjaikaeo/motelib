#include <motelib/system.h>
#include <motelib/led.h>
#include <motelib/timer.h>
#include <motelib/button.h>

Timer t;

void button(ButtonStatus s)
{
    // Toggle LED#1 whenever the button is pressed
    if (s == BUTTON_PRESSED)
        ledToggle(1);
}

void fired(Timer *t)
{
    // Do not toggle the LED if the button is being pressed
    if (!buttonIsPressed())
        ledToggle(0);
}

void boot()
{
    timerCreate(&t);
    timerStart(&t, TIMER_PERIODIC, 500, fired);
    buttonSetHandler(button);
}

