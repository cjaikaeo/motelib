from motesim import Simulator, Mote
from time import time, sleep

######################################
def extEvents():
    sleep(5)
    sim.nodes[1].shutdown()
    sleep(3)
    sim.nodes[1].boot()

######################################
sim = Simulator()
sim.addNode(Mote('build/sim/test-radio.elf'), (100,100))
sim.addNode(Mote('build/sim/test-radio.elf'), (200,100))
sim.addNode(Mote('build/sim/test-radio.elf'), (200,200))
sim.run(script=extEvents)
