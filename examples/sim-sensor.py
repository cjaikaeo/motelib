from motesim import Simulator, Mote
from time import time, sleep
from random import randint

######################################
class MyMote(Mote):
    def sense(self, sensor):
        return randint(0,100)

######################################
sim = Simulator()
sim.addNode(Mote('testsensor.elf', (100,100)))
sim.addNode(MyMote('testsensor.elf', (200,100)))
sim.run()
