/**
 * Test UART API implementation.
 *
 * The application will read from UART 5 characters at a time.  Once all 5
 * characters have been read, they will be echoed out via UART.  The whole
 * process is then repeated.
 */
#include <motelib/system.h>
#include <motelib/uart.h>
#include <motelib/led.h>
#include <motelib/button.h>

char buf[20];

void dataReady(uint8_t markerFound)
{
    uartRead(buf, sizeof(buf));
	uartWrite(buf, sizeof(buf));
}

void boot()
{
	uartWatchInputBytes(sizeof(buf), dataReady);
}
