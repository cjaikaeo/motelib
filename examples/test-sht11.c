#include <string.h>

#include <motelib/system.h>
#include <motelib/timer.h>
#include <motelib/actor.h>
#include <motelib/led.h>

Timer t1, tread, tsht;

#include "sht1x.h"

typedef enum
{
    LED_RED = 0,
    LED_YELLOW = 0,
    LED_GREEN = 0,
} LED;

void toggleIt(LED led)
{
    if (ledGet(led) == 0)
        ledSet(led, 1);
    else
        ledSet(led, 0);
}

void toggle()
{
    toggleIt(LED_GREEN);
}

void shtSuccess()
{    
    uint8_t data[3];    
    memcpy(data, "\0\0\0", 3);
    toggleIt(LED_YELLOW);

    if (sht_collect_raw(data) == 0)
    {        
        debug(">%X %X %X\n", data[0], data[1], data[2]);
        uint16_t temp = sht_temp_convert(data);
        debug("=%u C\n", temp);
    }
}

void readSht()
{    
    sht_measure_mode mode = (ledGet(LED_RED))? TEMP : HUMID;
    toggleIt(LED_RED);        
    
    if (s_measure(mode) == 0)    
    {
        //startTimer(tsht, ONESHOT, 80, shtSuccess); // Why this do not work ?       
        
        uint8_t data[3];    
        memcpy(data, "\0\0\0", 3);
        toggleIt(LED_YELLOW);

        if (sht_collect_raw(data) == 0) // TODO: implement in split-phase paradigm 
        {        
            debug(">%X %X %X\n", data[0], data[1], data[2]);
            
            if (mode == TEMP)
            {            
                uint16_t temp = sht_temp_convert(data);
                debug("Temp=%u.%u C\n", temp/100, temp%100);
            }
            else
            {
                uint16_t humid = sht_rh_convert(data);
                debug("Humid=%u.%u %%\n", humid/100, humid%100);
            }
            
            debug("crc=%X\n", crc_seed);
            debug("reverse crc=%X\n", SHT_BIT_REVERSAL( crc_seed ));
        }
    }
}

void boot()
{
    timerCreate(&t1);
    timerCreate(&tread);
    timerCreate(&tsht);
    
    //s_softreset();
    
    debug("\n\n\n\n\n");

    timerStart(&t1, TIMER_PERIODIC, 1000, toggle);
    timerStart(&tread, TIMER_PERIODIC, 10000, readSht);
}

