try:
    from motesim import Simulator, Mote
except ImportError:
    print "'motesim' module not found."
    print "Add path containing the motesim module to your PYTHONPATH variable"
    exit()
from time import time, sleep

######################################
def extEvents():
    sleep(3)
    sim.nodes[1].shutdown()
    sim.nodes[0].txRange = 150
    sleep(1)
    sim.nodes[1].boot()
    movingMote = sim.nodes[0]
    for i in range(100):
        movingMote.move(movingMote.pos[0]+2,movingMote.pos[1]+3)
        sleep(0.1)

######################################
sim = Simulator()
sim.addNode(Mote('testradio.elf', (100,100)))
sim.addNode(Mote('testradio.elf', (200,100)))
sim.addNode(Mote('testradio.elf', (200,200)))
sim.run(extScript=extEvents)
