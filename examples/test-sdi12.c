#include <motelib/system.h>
#include <motelib/uart.h>
#include <motelib/led.h>
#include <motelib/timer.h>

#include "sdi12.h"

#define tx_en() output_c_high(0) 
#define tx_dis() output_c_low(0)

#define tx_high() output_d_high(1)
#define tx_low() output_d_low(1)

Timer t1;

char buf[20];

// --------------------------------------------------------
void writeDone(UartStatus s)
{
    delay_ms(8); // Marking atlease 8.33 ms
    tx_dis();        
}

void tryit()
{
    tx_en();
    
    tx_low(); // Breaking with spacing atlease 12 ms
    delay_ms(12);
    tx_high(); // Prepare with marking atlease 8.33 ms
    delay_ms(8);
      
/*    uartWrite("?!\r\n", 4, writeDone); // Send command "Please return address"*/
    uartWrite("?!", 2, writeDone); // Send command "Please return address"
}

void readDone(char* readBuf, uint16_t len)
{
    ledToggle(0);
    uartRead(buf, 5, readDone);
}

void boot()
{
    Uart_Init_SDI12();
    tx_dis();
    
    t1 = timerCreate();   
    timerStart(t1, TIMER_PERIODIC, 2000, tryit); // Each command to SDI-12 device should delay about 1 sec. 
    
    uartRead(buf, 5, readDone);
}

