from motesim import Simulator, Mote
from time import time, sleep
from random import randint
from topovis import LineStyle, FillStyle

######################################
class MoteWithActor(Mote):
    linestyle = LineStyle(color=(1,.5,.5),width=3)
    def setActorState(self, actorNo, state):
        scene = sim.scene
        obj_id = 'MoteWithActor:%d:%d' % (self.id, actorNo)
        if actorNo == 1:
            if state == 0:
                scene.delshape(obj_id)
            else:
                scene.circle(self.pos[0], self.pos[1], 100, line=self.linestyle, id=obj_id)
        else:
            Mote.setActorState(self, actorNo, state)

######################################
sim = Simulator()
sim.addNode(MoteWithActor('build/sim/test-actor.elf', (100,100)))
sim.run()
