/** 
 * SHT15 Function
 *
 * @from http://share.psu.ac.th/blog/electronic/1466
 * @revised ipas at inbox dot com
 *
 * Important! R-pullup, 330 Ohm, on DATA-pin is need.
 *
 * TODO: CRC calculation is still not work.
 */

#include <avr/io.h>
#include <util/delay.h>
#include <util/delay_basic.h>

#define DAT 4
#define SCK 5

#define CRC_ENABLE 1

#define STATUS_REG_W 0x06 
#define STATUS_REG_R 0x07
#define SHT_RESET    0x1E

#define RELEASE_BUS(pin) DDRC &= ~(1 << pin) // To become input pin
#define PULLUP_DAT()    PORTC |=  (1 << DAT) // Pull up
#define CAPTURE_BUS(pin) DDRC |=  (1 << pin) // To become output pin

typedef enum {TEMP=0x03, HUMID=0x05} sht_measure_mode;

/**
 * CRC
 */
#include <avr/pgmspace.h>

const uint8_t crc_table[] PROGMEM = 
//{
//    0x00,0x85,0x8F,0x0A,0x9B,0x1E,0x14,0x91,0xB3,0x36,0x3C,0xB9,0x28,0xAD,0xA7,0x22,
//    0xE3,0x66,0x6C,0xE9,0x78,0xFD,0xF7,0x72,0x50,0xD5,0xDF,0x5A,0xCB,0x4E,0x44,0xC1,
//    0x43,0xC6,0xCC,0x49,0xD8,0x5D,0x57,0xD2,0xF0,0x75,0x7F,0xFA,0x6B,0xEE,0xE4,0x61,
//    0xA0,0x25,0x2F,0xAA,0x3B,0xBE,0xB4,0x31,0x13,0x96,0x9C,0x19,0x88,0x0D,0x07,0x82,
//    0x86,0x03,0x09,0x8C,0x1D,0x98,0x92,0x17,0x35,0xB0,0xBA,0x3F,0xAE,0x2B,0x21,0xA4,
//    0x65,0xE0,0xEA,0x6F,0xFE,0x7B,0x71,0xF4,0xD6,0x53,0x59,0xDC,0x4D,0xC8,0xC2,0x47,
//    0xC5,0x40,0x4A,0xCF,0x5E,0xDB,0xD1,0x54,0x76,0xF3,0xF9,0x7C,0xED,0x68,0x62,0xE7,
//    0x26,0xA3,0xA9,0x2C,0xBD,0x38,0x32,0xB7,0x95,0x10,0x1A,0x9F,0x0E,0x8B,0x81,0x04,
//    0x89,0x0C,0x06,0x83,0x12,0x97,0x9D,0x18,0x3A,0xBF,0xB5,0x30,0xA1,0x24,0x2E,0xAB,
//    0x6A,0xEF,0xE5,0x60,0xF1,0x74,0x7E,0xFB,0xD9,0x5C,0x56,0xD3,0x42,0xC7,0xCD,0x48,
//    0xCA,0x4F,0x45,0xC0,0x51,0xD4,0xDE,0x5B,0x79,0xFC,0xF6,0x73,0xE2,0x67,0x6D,0xE8,
//    0x29,0xAC,0xA6,0x23,0xB2,0x37,0x3D,0xB8,0x9A,0x1F,0x15,0x90,0x01,0x84,0x8E,0x0B,
//    0x0F,0x8A,0x80,0x05,0x94,0x11,0x1B,0x9E,0xBC,0x39,0x33,0xB6,0x27,0xA2,0xA8,0x2D,
//    0xEC,0x69,0x63,0xE6,0x77,0xF2,0xF8,0x7D,0x5F,0xDA,0xD0,0x55,0xC4,0x41,0x4B,0xCE,
//    0x4C,0xC9,0xC3,0x46,0xD7,0x52,0x58,0xDD,0xFF,0x7A,0x70,0xF5,0x64,0xE1,0xEB,0x6E,
//    0xAF,0x2A,0x20,0xA5,0x34,0xB1,0xBB,0x3E,0x1C,0x99,0x93,0x16,0x87,0x02,0x08,0x8D
//};
{ // Reverse look-up crc table.
    0, 49,
    98, 83, 196, 245, 166, 151, 185, 136, 219,
    234, 125, 76, 31, 46, 67, 114, 33, 16, 135,
    182, 229, 212, 250, 203, 152, 169, 62, 15,
    92, 109, 134, 183, 228, 213, 66, 115, 32,
    17, 63, 14, 93,108, 251, 202, 153, 168,
    197, 244, 167, 150, 1, 48, 99, 82, 124, 77,
    30, 47, 184, 137, 218, 235, 61, 12, 95,
    110, 249, 200, 155, 170, 132, 181, 230,
    215, 64, 113, 34, 19, 126, 79, 28, 45, 186,
    139, 216, 233, 199, 246, 165, 148, 3, 50,
    97, 80, 187, 138, 217, 232, 127, 78, 29,
    44, 2, 51, 96, 81, 198, 247, 164, 149, 248,
    201, 154, 171, 60, 13, 94, 111, 65, 112,
    35, 18, 133, 180, 231, 214, 122, 75, 24,
    41, 190, 143, 220, 237, 195, 242, 161, 144,
    7, 54, 101, 84, 57, 8, 91, 106, 253, 204,
    159, 174, 128, 177, 226, 211, 68, 117, 38,
    23, 252, 205, 158, 175, 56, 9, 90, 107, 69,
    116, 39, 22, 129, 176, 227, 210, 191, 142,
    221, 236, 123, 74, 25, 40, 6, 55, 100, 85,
    194, 243, 160, 145, 71, 118, 37, 20, 131,
    178, 225, 208, 254, 207, 156, 173, 58, 11,
    88, 105, 4, 53, 102, 87, 192, 241, 162,
    147, 189, 140, 223, 238, 121, 72, 27, 42,
    193, 240, 163, 146, 5, 52, 103, 86, 120,
    73, 26, 43, 188, 141, 222, 239, 130, 179,
    224, 209, 70, 119, 36, 21, 59, 10, 89, 104,
    255, 206, 157, 172
};

unsigned char crc_seed = 0;
#define SHT_BIT_REVERSAL(value) pgm_read_byte( &crc_table[ value ] )
#define SHT_CRC(value)       crc_seed  = SHT_BIT_REVERSAL( crc_seed ^ value  )
#define SHT_CHECK_CRC(value) crc_seed ^= SHT_BIT_REVERSAL( value )

/**
 * Auxilary function
 */
void delay_us(unsigned long int t)
{
//    _delay_us(t);
//    _delay_loop_1(t*2);     
    _delay_loop_2(t*3);
}

void delay_ms(unsigned long int t)
{
//    _delay_ms(t);
    t *= 1000;
    while (t--)
        delay_us(1);        
} 

void output_high(uint8_t pin)
{
    CAPTURE_BUS(pin);
    PORTC |= (1 << pin); // activate '1'    
    delay_us(10);
}

void output_low(uint8_t pin)
{    
    CAPTURE_BUS(pin);
    PORTC &= ~(1 << pin); // deactivate '0'
    delay_us(10);
}

uint8_t input(uint8_t pin)
{
    return (PINC & (1 << pin)) ? 1 : 0;
} 

/**
 * Writes a byte on the bus and checks the acknowledge 
 */
char s_write_byte(unsigned char value)
{
    unsigned char i, error=0;  

    for (i = 0x80; i > 0; i >>= 1) /* shift bit for masking */
    {
        if (i & value)
            output_high(DAT); /* masking value with i , write to BUS */
        else
            output_low(DAT);                        
        
        output_high(SCK); /* clk for BUS */
        output_low(SCK);
    }
   
    RELEASE_BUS(DAT);     /* release DATA-line */    
    PULLUP_DAT();         // Pull high DATA-bus  
      
    output_high(SCK);     /* clk #9 for 1 */          
    delay_us(10);
    error = input(DAT);   /* check 1 (DATA will be pulled down by SHT11) */      
    delay_us(10);
    output_low(SCK);        
    
    #ifdef CRC_ENABLE
    if (!error) SHT_CRC(value);    
    #endif
    
    return error;         /* error=1 in case of no ACK */
}

/**
 * Reads a byte form the bus and gives an 1nowledge in case of "1=1" 
 */
char s_read_byte(unsigned char ack)
{
    unsigned char i, val=0;

    RELEASE_BUS(DAT);     /* release DATA-line */    
    PULLUP_DAT();         // Pull high DATA-bus    
    
    for (i = 0x80; i > 0; i >>= 1) /* shift bit for masking */
    {
        output_high(SCK); /* clk for BUS */
        
        if (input(DAT))
            val |= i; /* read bit  */
            
        output_low(SCK);                      
    }
    
    CAPTURE_BUS(DAT);
    
    if (ack)
        output_low(DAT); /* in case of "1==1" pull down DATA-Line */
    else
        output_high(DAT);
    
    output_high(SCK); /* clk #9 for 1 */
    output_low(SCK);
    
    RELEASE_BUS(DAT);     /* release DATA-line */    
    PULLUP_DAT();         // Pull high DATA-bus    
    
    #ifdef CRC_ENABLE
    SHT_CRC(val);
    #endif
    
    return val;
}

/**
 * Generates a transmission start 
 */
void s_transstart(void)
{
    output_high(DAT);
    output_low(SCK); /* Initial state */

    output_high(SCK);
    output_low(DAT);
    output_low(SCK);  
    output_high(SCK);
    output_high(DAT);           
    output_low(SCK);           
    
    RELEASE_BUS(DAT); /* release DATA-line */    
    PULLUP_DAT();     // Pull high DATA-bus    
}

/**
 * Communication reset: 
 * DATA-line=1 and at least 9 SCK cycles followed by transstart 
 */
void s_connectionreset(void)
{  
    unsigned char i;

    delay_ms(11); // Minimum waiting time for ready  

    output_high(DAT); /* Initial state */
    output_low(SCK); 
    
    for(i = 0; i < 9; i++) /* nine SCK cycles or more */
    {
        output_high(SCK);
        output_low(SCK);
    }
    
    s_transstart(); /* transmission start */
}

/**
 * Resets the sensor by a softreset
 */
char s_softreset(void)
{
    crc_seed = 0;
    
    s_connectionreset(); //reset communication    
    return s_write_byte(SHT_RESET); //send RESET-command to sensor:
    //return=1 in case of no response form the sensor
} 

/**
 * Make a measurement (humidity/temperature) with checksum 
 *
 * @param mode: look sht_measure_mode typedef
 * @return error code, 0 for no error
 */
char s_measure(sht_measure_mode mode)
{
    char error = 0;

    s_transstart(); /* transmission start */    
    error = s_write_byte(mode); // Request measuring operation.
        
    RELEASE_BUS(DAT);     /* release DATA-line */    
    PULLUP_DAT();         // Pull high DATA-bus  
                
    if (error)    
        debug("write error>%d\n", error);
        
    return error; // Wait for end-of-conversion ...      
}

/**
 * Collect raw data
 *
 * @param p_value: pointer to data array.
 * @return error: on success return 0.
 */
char sht_collect_raw(uint8_t *p_value) 
{
    char error = 0;
    unsigned int i;

    for (i = 0; i < 20; i++)
    {    
        delay_ms(100);
        if(input(DAT) == 0) 
        {   
            i = 0;        
            break; // wait until sensor has finished the measurement            
        }
    }            
    
    if (i)
    {
        debug("no response\n");
        return error=2;
    }
                   
    p_value[0] = s_read_byte(1); /* read the first byte (MSB) */                           
    p_value[1] = s_read_byte(1); /* read the second byte (LSB) */
    p_value[2] = s_read_byte(1); /* read checksum */
    
    return error;
}

/**
 * Temp convert
 */
uint16_t sht_temp_convert(unsigned char *temp)
{
    uint16_t x;
    x   = temp[0];
    x <<= 8;
    x  += temp[1];
    x  -= 4000; // 00.01 C
    return x;
}

/**
 * RH convert
 */
uint16_t sht_rh_convert(unsigned char *rh)
{
    uint16_t x;
    const float C1 = -4, 
                C2 =  0.0405, 
                C3 = -0.0000028;
    float val;         
    x   = rh[0];
    x <<= 8;
    x  += rh[1];    
    val = C1 + x*C2 + x*C3*x;    
    x   = (uint16_t)(val*100); // 00.01 %
    return x;
}

