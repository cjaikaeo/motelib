/*
 * Each node broadcasts a message of type 2 and toggles its LED0 periodically.
 * Upon receiving a message of type 2, LED1 is toggled.
 */
#include <motelib/system.h>
#include <motelib/led.h>
#include <motelib/timer.h>
#include <motelib/radio.h>

#define MSG_TYPE 2

Timer t;

void recv(Address source, MessageType type, void* msg, uint8_t len)
{
    ledToggle(1);
}

void sendDone(RadioStatus t)
{
}

void timerFired(Timer *t)
{
    ledToggle(0);
    radioRequestTx(BROADCAST_ADDR, MSG_TYPE, "hello", 6, sendDone);
}

void boot()
{
    timerCreate(&t);
    timerStart(&t, TIMER_PERIODIC, 500, timerFired);
    radioSetRxHandler(recv);
}
