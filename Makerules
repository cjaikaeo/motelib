# vim:syntax=make

#############################################################################
# This is the main Makerules for MoteLib's make system
#
# To use the make system, just create a Makefile with an include statement to
# include this Makerules in your working directory.
#
# Supported parameters are:
#
#  o MOTELIB_DIR -- the MoteLib's root directory.
#
#  o PLATFORM -- the platform to which the built application will be targeted.
#
#  o TARGET -- the main C source file(s) without the .c extension.
#
#  o OBJS -- additional object files to be built by MoteLib's make system and
#            linked into the target application.
#
#  o EXTRA_DEPS -- list of extra dependencies that must be built before
#                  building the main application.
#
#  o CFLAGS -- Extra flags to be passed to the compiler
#
#  o LDFLAGS -- Extra flags to be passed to the linker
#
#  o LIBS -- List of extra object files from $MOTELIB/lib directory
#
#  o DEBUG -- Flag indicating whether the debugging feature is turned on.
#
#  o VERBOSE -- Flag indicating whether compiler commands are fully shown
#
#############################################################################

#######################################
# Prepare default settings and target #
#######################################
FORMAT ?= elf
DEBUG ?= 0
VERBOSE ?= 0
UART_BAUD ?= 9600

ifeq ($(DEBUG),0)
	NDEBUG ?= 1
endif

BUILD_DIR   := build/$(PLATFORM)
TARGET_FILES := $(patsubst %,$(BUILD_DIR)/%.$(FORMAT),$(TARGET))
APP_BUILD_DIR := $(BUILD_DIR)/app
PLATFORM_BUILD_DIR := $(BUILD_DIR)/platform
LIB_BUILD_DIR := $(BUILD_DIR)/lib

# default target
# Note: .PHONY is used to make sure these targets work properly even though
# there is a file with the same name in the current dir
.PHONY: all
all: $(EXTRA_DEPS) $(TARGET_FILES)

# prevent intermediate files from being removed
.SECONDARY:

################################################
# Process platform-specific rules and settings #
################################################
APP_SRCS := $(patsubst %,%.c,$(TARGET)) $(patsubst %.o,%.c,$(OBJS))
PLATFORM_SRC_DIR := $(MOTELIB_DIR)/platforms/$(PLATFORM)
LIB_SRC_DIR := $(MOTELIB_DIR)/lib
LIBS += sys/main.o sys/led.o sys/radio.o sys/timer.o sys/cirbuf.o sys/uart.o
include $(PLATFORM_SRC_DIR)/Makerules
CFLAGS += -I$(MOTELIB_DIR)/include -I$(PLATFORM_SRC_DIR) -I. -DUART_BAUD=$(UART_BAUD)
OBJCOPY_FLAGS ?= -j .text -j .data
ASFLAGS := $(CFLAGS)

ifeq ($(NDEBUG),1)
	CFLAGS += -DNDEBUG
endif

APP_OBJS_IN_BUILD_DIR := $(patsubst %.o, $(APP_BUILD_DIR)/%.o, $(OBJS))
LIB_OBJS_IN_BUILD_DIR := $(patsubst %.o, $(LIB_BUILD_DIR)/%.o, $(LIBS))
PLATFORM_OBJS_IN_BUILD_DIR := $(patsubst %.o, $(PLATFORM_BUILD_DIR)/%.o, $(PLATFORM_OBJS))

######################################
# Set default tools if not specified #
######################################
CC      ?= gcc
OBJCOPY ?= objcopy
SIZE    ?= size

######################################
# Miscellaneous variables and macros #
######################################
GEN_DEPS = -MMD -MP
DEPEND_OBJS = $(APP_OBJS_IN_BUILD_DIR) $(PLATFORM_OBJS_IN_BUILD_DIR) $(LIB_OBJS_IN_BUILD_DIR)

ifeq ($(VERBOSE),0)
  define COMPILE
    $(info COMPILING $< -> $@)
    @mkdir -p $(dir $@)
    @$(CC) -c $(CFLAGS) -o $@ $< $(GEN_DEPS)
  endef
  define ASSEMBLE
    $(info ASSEMBLING $< -> $@)
    @mkdir -p $(dir $@)
    @$(CC) -c -x assembler-with-cpp $(CFLAGS) -o $@ $< $(GEN_DEPS)
  endef
  define LINK
    $(info LINKING $^ -> $@)
    @$(CC) -o $@ $^ $(LDFLAGS)
    @$(SIZE) $(SIZE_OPTIONS) $@
  endef
else
  define COMPILE
    $(info COMPILING $< -> $@)
    mkdir -p $(dir $@)
    $(CC) -c $(CFLAGS) -o $@ $< $(GEN_DEPS)
  endef
  define ASSEMBLE
    $(info ASSEMBLING $< -> $@)
    mkdir -p $(dir $@)
    $(CC) -c -x assembler-with-cpp $(CFLAGS) -o $@ $< $(GEN_DEPS)
  endef
  define LINK
    $(info LINKING $^ -> $@)
    $(CC) -o $@ $^ $(LDFLAGS)
    $(SIZE) $(SIZE_OPTIONS) $@
  endef
endif


######################
# Common build rules #
######################

# 
# import all dependencies rules
#
-include $(DEPEND_OBJS:.o=.d)

#
# rules to build binary from an executable
#
$(BUILD_DIR)/%.hex: $(BUILD_DIR)/%.elf
	$(info EXTRACTING $< -> $@)
	@$(OBJCOPY) $(OBJCOPY_FLAGS) -O ihex $< $@

$(BUILD_DIR)/%.bin: $(BUILD_DIR)/%.elf
	$(info EXTRACTING $< -> $@)
	@$(OBJCOPY) $(OBJCOPY_FLAGS) -O binary $< $@

#
# rule to build an executable from all depended objects
#
$(BUILD_DIR)/%.elf: $(APP_BUILD_DIR)/%.o $(DEPEND_OBJS)
	$(LINK)

#
# rules to build platform-specific objects from C or ASM source
#
$(PLATFORM_BUILD_DIR)/%.o: $(PLATFORM_SRC_DIR)/%.c
	$(COMPILE)

$(PLATFORM_BUILD_DIR)/%.o: $(PLATFORM_SRC_DIR)/%.S
	$(ASSEMBLE)

#
# rules to build app-specific objects file from C or ASM source
#
$(APP_BUILD_DIR)/%.o: %.c
	$(COMPILE)

$(APP_BUILD_DIR)/%.o: %.S
	$(ASSEMBLE)

#
# rules to build platform-independent library objects from C or ASM source
#
$(LIB_BUILD_DIR)/%.o: $(LIB_SRC_DIR)/%.c
	$(COMPILE)

$(LIB_BUILD_DIR)/%.o: $(LIB_SRC_DIR)/%.S
	$(ASSEMBLE)

#
# rules to build objects not matching all the above rules
#
%.o: %.c
	$(COMPILE)

%.o: %.S
	$(ASSEMBLE)

size: $(TARGET_FILES)
	$(SIZE) $(SIZE_OPTIONS) $<

#
# make .o files needing to be rebuilt phony
#
ifdef PLATFORM_REBUILD_OBJS
PLATFORM_REBUILD_OBJS_IN_BUILD_DIR := $(patsubst %.o, $(PLATFORM_BUILD_DIR)/%.o, $(PLATFORM_REBUILD_OBJS))
$(PLATFORM_REBUILD_OBJS_IN_BUILD_DIR): FORCE
.PHONY: FORCE
endif

.PHONY: clean
clean:
	rm -rf $(BUILD_DIR) *~
