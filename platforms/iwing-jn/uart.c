#include <jendefs.h>
#include <AppHardwareApi.h>
#include <motelib/system.h>
#include <motelib/uart.h>
#include <motelib/cirbuf.h>
#include <motelib/sys/uart.h>

#define CAT1(x,y) x ## y
#define CAT2(x,y) CAT1(x,y)
#define JENNIC_BAUD_RATE CAT2(E_AHI_UART_RATE_,UART_BAUD)

static uint8_t txBuf[16];
static uint8_t rxBuf[16];
static bool tx_enabled, rx_enabled;

void _platform_uart_init()
{
}

void _platform_uart_enable(bool tx, bool rx)
{
    if (tx || rx)
    {
        // Make the two pins input and disable pullups by default
        vAHI_DioSetDirection(1<<4|1<<5,0);
        vAHI_DioSetPullup(0,1<<4|1<<5);


        bAHI_UartEnable(
                E_AHI_UART_0,
                txBuf,
                sizeof(txBuf),
                rxBuf,
                sizeof(rxBuf)
                );

        vAHI_UartSetInterrupt(
                E_AHI_UART_0,
                FALSE,
                FALSE,
                FALSE,
                TRUE,  // RX data available
                E_AHI_UART_FIFO_LEVEL_1);  // trigger when 1 byte is received

        vAHI_UartSetBaudRate(E_AHI_UART_0, JENNIC_BAUD_RATE);
    }
    else
    {
        vAHI_UartDisable(E_AHI_UART_0);
    }
    tx_enabled = tx;
    rx_enabled = rx;
}

////////////////////////////////////////////////////////
void _uart_poll()
{
    if (!tx_enabled) return;

    // Make sure there is data to be written and the tx buffer is available
    if (!cirbufEmpty(&_sys_uart_cb_out)
            && u16AHI_UartReadTxFifoLevel(E_AHI_UART_0) < sizeof(txBuf))
    {
        // send out the first byte to Jennic's tx buffer
        vAHI_UartWriteData(E_AHI_UART_0, cirbufReadByte(&_sys_uart_cb_out));
    }
}

////////////////////////////////////////////////////////
void _uart_rx_data_avail()
{
    uint8_t current_byte;

    // Append recently read bytes into the buffer
    // TODO: Investigate the interpretation of Rx FIFO status
    do
    {
        current_byte = u8AHI_UartReadData(E_AHI_UART_0);

        // Do nothing if RX is not enabled
        if (!rx_enabled) return;

        // If the buffer is already full, remove the oldest byte
        if (cirbufFull(&_sys_uart_cb_in))
            cirbufReadByte(&_sys_uart_cb_in);

        cirbufWriteByte(&_sys_uart_cb_in, current_byte);

        // Notify sys if marker watch is in progress and a specified marker is
        // encounter
        if (_sys_uart_useMarker && (current_byte == _sys_uart_marker))
            _sys_uart_notifyMarkerFound();
    }
    while (u8AHI_UartReadLineStatus(E_AHI_UART_0) & (1<<E_AHI_UART_LS_DR));
}

////////////////////////////////////////////////////////
int printf_jennic(char *fmt, ...)
{
    char buf[1024];
    int len;

    // gather variable-length argument list and pass it to vsprintf()
    va_list args;
    va_start(args, fmt);
    len = vsprintf(buf, fmt, args);
    va_end(args);

    uartWrite(buf, len);

    return len;
}

