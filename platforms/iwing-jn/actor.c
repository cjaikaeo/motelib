#include <stdbool.h>

#include <jendefs.h>
#include <AppHardwareApi.h>

#include <motelib/actor.h>
#include "platform.h"

void actorSetState(ActorType actor, uint8_t state)
{
    // perform sanity check for parameters
    if (actor > ACTOR_17) return;

    // make the pin output
    vAHI_DioSetDirection(0,1<<actor);

    if (state) 
        vAHI_DioSetOutput(1<<actor,0); // activate actor
    else
        vAHI_DioSetOutput(0,1<<actor); // deactivate actor
}

uint8_t actorGetState(ActorType actor)
{
    // perform sanity check for parameters
    if (actor > ACTOR_17) return 0xFF;

    return (u32AHI_DioReadInput() & (1<<actor)) >> actor;
}
