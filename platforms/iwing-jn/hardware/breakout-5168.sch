<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="6.2">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="yes" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="yes" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="yes" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="yes" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="yes" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="yes" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="yes" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="yes" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="yes" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="yes" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="yes" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="yes" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="yes" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="yes" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="yes" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="yes" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="yes" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="yes" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="yes" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="yes" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="yes" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="yes" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="yes" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="yes" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="yes" active="no"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="frames">
<packages>
</packages>
<symbols>
<symbol name="DINA4_L">
<frame x1="0" y1="0" x2="264.16" y2="180.34" columns="4" rows="4" layer="94" border-left="no" border-top="no" border-right="no" border-bottom="no"/>
</symbol>
<symbol name="DOCFIELD">
<wire x1="0" y1="0" x2="71.12" y2="0" width="0.1016" layer="94"/>
<wire x1="101.6" y1="15.24" x2="87.63" y2="15.24" width="0.1016" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="5.08" width="0.1016" layer="94"/>
<wire x1="0" y1="5.08" x2="71.12" y2="5.08" width="0.1016" layer="94"/>
<wire x1="0" y1="5.08" x2="0" y2="15.24" width="0.1016" layer="94"/>
<wire x1="101.6" y1="15.24" x2="101.6" y2="5.08" width="0.1016" layer="94"/>
<wire x1="71.12" y1="5.08" x2="71.12" y2="0" width="0.1016" layer="94"/>
<wire x1="71.12" y1="5.08" x2="87.63" y2="5.08" width="0.1016" layer="94"/>
<wire x1="71.12" y1="0" x2="101.6" y2="0" width="0.1016" layer="94"/>
<wire x1="87.63" y1="15.24" x2="87.63" y2="5.08" width="0.1016" layer="94"/>
<wire x1="87.63" y1="15.24" x2="0" y2="15.24" width="0.1016" layer="94"/>
<wire x1="87.63" y1="5.08" x2="101.6" y2="5.08" width="0.1016" layer="94"/>
<wire x1="101.6" y1="5.08" x2="101.6" y2="0" width="0.1016" layer="94"/>
<wire x1="0" y1="15.24" x2="0" y2="22.86" width="0.1016" layer="94"/>
<wire x1="101.6" y1="35.56" x2="0" y2="35.56" width="0.1016" layer="94"/>
<wire x1="101.6" y1="35.56" x2="101.6" y2="22.86" width="0.1016" layer="94"/>
<wire x1="0" y1="22.86" x2="101.6" y2="22.86" width="0.1016" layer="94"/>
<wire x1="0" y1="22.86" x2="0" y2="35.56" width="0.1016" layer="94"/>
<wire x1="101.6" y1="22.86" x2="101.6" y2="15.24" width="0.1016" layer="94"/>
<text x="1.27" y="1.27" size="2.54" layer="94" font="vector">Date:</text>
<text x="12.7" y="1.27" size="2.54" layer="94" font="vector">&gt;LAST_DATE_TIME</text>
<text x="72.39" y="1.27" size="2.54" layer="94" font="vector">Sheet:</text>
<text x="86.36" y="1.27" size="2.54" layer="94" font="vector">&gt;SHEET</text>
<text x="88.9" y="11.43" size="2.54" layer="94" font="vector">REV:</text>
<text x="1.27" y="19.05" size="2.54" layer="94" font="vector">TITLE:</text>
<text x="1.27" y="11.43" size="2.54" layer="94" font="vector">Document Number:</text>
<text x="17.78" y="19.05" size="2.54" layer="94" font="vector">&gt;DRAWING_NAME</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="DINA4_L" prefix="FRAME" uservalue="yes">
<description>&lt;b&gt;FRAME&lt;/b&gt;&lt;p&gt;
DIN A4, landscape with extra doc field</description>
<gates>
<gate name="G$1" symbol="DINA4_L" x="0" y="0"/>
<gate name="G$2" symbol="DOCFIELD" x="162.56" y="0" addlevel="must"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="pinhead">
<description>&lt;b&gt;Pin Header Connectors&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="1X01">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="1.27" y1="0.635" x2="1.27" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="-1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.1524" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" shape="octagon"/>
<text x="-1.3462" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="PINHD1">
<wire x1="-6.35" y1="-2.54" x2="1.27" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="1.27" y2="2.54" width="0.4064" layer="94"/>
<wire x1="1.27" y1="2.54" x2="-6.35" y2="2.54" width="0.4064" layer="94"/>
<wire x1="-6.35" y1="2.54" x2="-6.35" y2="-2.54" width="0.4064" layer="94"/>
<text x="-6.35" y="3.175" size="1.778" layer="95">&gt;NAME</text>
<text x="-6.35" y="-5.08" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-2.54" y="0" visible="pad" length="short" direction="pas" function="dot"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="PINHD-1X1" prefix="JP" uservalue="yes">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="PINHD1" x="0" y="0"/>
</gates>
<devices>
<device name="" package="1X01">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="jennic">
<description>Jennic 5168 802.15.4/Zigbee Modules</description>
<packages>
<package name="JENNIC5168MODULE">
<description>Footprint for the Jennic 5168 based modules</description>
<smd name="DIO4" x="2.54" y="0" dx="0.8" dy="1.5" layer="1" roundness="100" rot="R180"/>
<smd name="ADC1" x="0" y="12.7" dx="0.8" dy="1.5" layer="1" roundness="100" rot="R90"/>
<smd name="DIO11" x="16" y="2.54" dx="0.8" dy="1.5" layer="1" roundness="100" rot="R90"/>
<text x="1.64" y="1.27" size="1.778" layer="21">JN5168MO</text>
<wire x1="0" y1="13.335" x2="0" y2="12.065" width="0.254" layer="51" curve="-180"/>
<wire x1="0" y1="12.065" x2="0" y2="10.795" width="0.254" layer="51" curve="-180"/>
<wire x1="0" y1="10.795" x2="0" y2="9.525" width="0.254" layer="51" curve="-180"/>
<wire x1="0" y1="9.525" x2="0" y2="8.255" width="0.254" layer="51" curve="-180"/>
<wire x1="0" y1="8.255" x2="0" y2="6.985" width="0.254" layer="51" curve="-180"/>
<wire x1="0" y1="6.985" x2="0" y2="5.715" width="0.254" layer="51" curve="-180"/>
<wire x1="0" y1="5.715" x2="0" y2="4.445" width="0.254" layer="51" curve="-180"/>
<wire x1="0" y1="4.445" x2="0" y2="3.175" width="0.254" layer="51" curve="-180"/>
<wire x1="0" y1="3.175" x2="0" y2="1.905" width="0.254" layer="51" curve="-180"/>
<wire x1="1.905" y1="0" x2="3.175" y2="0" width="0.254" layer="51" curve="-180"/>
<wire x1="3.175" y1="0" x2="4.445" y2="0" width="0.254" layer="51" curve="-180"/>
<wire x1="4.445" y1="0" x2="5.715" y2="0" width="0.254" layer="51" curve="-180"/>
<wire x1="5.715" y1="0" x2="6.985" y2="0" width="0.254" layer="51" curve="-180"/>
<wire x1="6.985" y1="0" x2="8.255" y2="0" width="0.254" layer="51" curve="-180"/>
<wire x1="8.255" y1="0" x2="9.525" y2="0" width="0.254" layer="51" curve="-180"/>
<wire x1="9.525" y1="0" x2="10.795" y2="0" width="0.254" layer="51" curve="-180"/>
<wire x1="10.795" y1="0" x2="12.065" y2="0" width="0.254" layer="51" curve="-180"/>
<wire x1="12.065" y1="0" x2="13.335" y2="0" width="0.254" layer="51" curve="-180"/>
<wire x1="16" y1="12.065" x2="16" y2="13.335" width="0.254" layer="51" curve="-180"/>
<wire x1="16" y1="10.795" x2="16" y2="12.065" width="0.254" layer="51" curve="-180"/>
<wire x1="16" y1="9.525" x2="16" y2="10.795" width="0.254" layer="51" curve="-180"/>
<wire x1="16" y1="8.255" x2="16" y2="9.525" width="0.254" layer="51" curve="-180"/>
<wire x1="16" y1="6.985" x2="16" y2="8.255" width="0.254" layer="51" curve="-180"/>
<wire x1="16" y1="5.715" x2="16" y2="6.985" width="0.254" layer="51" curve="-180"/>
<wire x1="16" y1="4.445" x2="16" y2="5.715" width="0.254" layer="51" curve="-180"/>
<wire x1="16" y1="3.175" x2="16" y2="4.445" width="0.254" layer="51" curve="-180"/>
<wire x1="16" y1="1.905" x2="16" y2="3.175" width="0.254" layer="51" curve="-180"/>
<wire x1="13.335" y1="0" x2="16" y2="0" width="0.4064" layer="21"/>
<wire x1="16" y1="0" x2="16" y2="1.905" width="0.4064" layer="21"/>
<wire x1="0" y1="13.335" x2="0" y2="29.845" width="0.4064" layer="21"/>
<wire x1="0" y1="29.845" x2="16" y2="29.845" width="0.4064" layer="21"/>
<wire x1="16" y1="29.845" x2="16" y2="13.335" width="0.4064" layer="21"/>
<wire x1="1.905" y1="0" x2="0" y2="0" width="0.4064" layer="21"/>
<wire x1="0" y1="0" x2="0" y2="1.905" width="0.4064" layer="21"/>
<smd name="SPICLK" x="0" y="11.43" dx="0.8" dy="1.5" layer="1" roundness="100" rot="R90"/>
<smd name="SPIMISO" x="0" y="10.16" dx="0.8" dy="1.5" layer="1" roundness="100" rot="R90"/>
<smd name="DIO12" x="16" y="3.81" dx="0.8" dy="1.5" layer="1" roundness="100" rot="R90"/>
<smd name="DIO13" x="16" y="5.08" dx="0.8" dy="1.5" layer="1" roundness="100" rot="R90"/>
<smd name="RESETN" x="16" y="6.35" dx="0.8" dy="1.5" layer="1" roundness="100" rot="R90"/>
<smd name="DIO14" x="16" y="7.62" dx="0.8" dy="1.5" layer="1" roundness="100" rot="R90"/>
<smd name="DIO15" x="16" y="8.89" dx="0.8" dy="1.5" layer="1" roundness="100" rot="R90"/>
<smd name="DIO16" x="16" y="10.16" dx="0.8" dy="1.5" layer="1" roundness="100" rot="R90"/>
<smd name="DIO17" x="16" y="11.43" dx="0.8" dy="1.5" layer="1" roundness="100" rot="R90"/>
<smd name="ADC2" x="16" y="12.7" dx="0.8" dy="1.5" layer="1" roundness="100" rot="R90"/>
<smd name="SPIMOSI" x="0" y="8.89" dx="0.8" dy="1.5" layer="1" roundness="100" rot="R90"/>
<smd name="SPISSZ" x="0" y="7.62" dx="0.8" dy="1.5" layer="1" roundness="100" rot="R90"/>
<smd name="DIO0" x="0" y="6.35" dx="0.8" dy="1.5" layer="1" roundness="100" rot="R90"/>
<smd name="DIO1" x="0" y="5.08" dx="0.8" dy="1.5" layer="1" roundness="100" rot="R90"/>
<smd name="DIO2" x="0" y="3.81" dx="0.8" dy="1.5" layer="1" roundness="100" rot="R90"/>
<smd name="DIO3" x="0" y="2.54" dx="0.8" dy="1.5" layer="1" roundness="100" rot="R90"/>
<smd name="DIO5" x="3.81" y="0" dx="0.8" dy="1.5" layer="1" roundness="100" rot="R180"/>
<smd name="DIO6" x="5.08" y="0" dx="0.8" dy="1.5" layer="1" roundness="100" rot="R180"/>
<smd name="DIO7" x="6.35" y="0" dx="0.8" dy="1.5" layer="1" roundness="100" rot="R180"/>
<smd name="DIO8" x="7.62" y="0" dx="0.8" dy="1.5" layer="1" roundness="100" rot="R180"/>
<smd name="DIO9" x="8.89" y="0" dx="0.8" dy="1.5" layer="1" roundness="100" rot="R180"/>
<smd name="DIO10" x="10.16" y="0" dx="0.8" dy="1.5" layer="1" roundness="100" rot="R180"/>
<smd name="VDD" x="11.43" y="0" dx="0.8" dy="1.5" layer="1" roundness="100" rot="R180"/>
<smd name="GND" x="12.7" y="0" dx="0.8" dy="1.5" layer="1" roundness="100" rot="R180"/>
<wire x1="0.05" y1="21.67" x2="15.94" y2="21.67" width="0.4064" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="JN5168-001-M0X">
<wire x1="-17.78" y1="30.48" x2="38.1" y2="30.48" width="0.254" layer="94"/>
<wire x1="38.1" y1="30.48" x2="38.1" y2="-22.86" width="0.254" layer="94"/>
<wire x1="38.1" y1="-22.86" x2="-17.78" y2="-22.86" width="0.254" layer="94"/>
<wire x1="-17.78" y1="-22.86" x2="-17.78" y2="30.48" width="0.254" layer="94"/>
<text x="12.7" y="-20.32" size="1.778" layer="94">JN5168-001-M0x</text>
<pin name="DIO14/SIF_CLK" x="43.18" y="-7.62" length="middle" direction="in" rot="R180"/>
<pin name="DIO15/SIF_D" x="43.18" y="-10.16" length="middle" direction="in" rot="R180"/>
<pin name="DIO16/COMP1P" x="43.18" y="-12.7" length="middle" direction="in" rot="R180"/>
<pin name="DIO17/COMP1M/PWM4" x="43.18" y="-15.24" length="middle" direction="in" rot="R180"/>
<pin name="ADC1" x="-22.86" y="0" length="middle" direction="in"/>
<pin name="ADC2" x="-22.86" y="-2.54" length="middle" direction="in"/>
<pin name="DIO4/CTS0/JTAG_TCK/TIM0OUT/PC0" x="43.18" y="17.78" length="middle" rot="R180"/>
<pin name="DIO5/RTS0/JTAG_TMS/PWM1/PC1" x="43.18" y="15.24" length="middle" rot="R180"/>
<pin name="DIO6/TXD0/JTAG_TDO/PWM2" x="43.18" y="12.7" length="middle" rot="R180"/>
<pin name="DIO7/RXD0/JTAG_TDI/PWM3" x="43.18" y="10.16" length="middle" rot="R180"/>
<pin name="SPICLK/PWM2" x="-22.86" y="25.4" length="middle" direction="out"/>
<pin name="SPIMISO" x="-22.86" y="22.86" length="middle" direction="in"/>
<pin name="SPIMOSI/PWM3" x="-22.86" y="20.32" length="middle" direction="out"/>
<pin name="SPISEL0" x="-22.86" y="27.94" length="middle"/>
<pin name="DIO2/RFRX/TIM0CK_GT" x="43.18" y="22.86" length="middle" rot="R180"/>
<pin name="DIO3/RFTX/TIM0CAP" x="43.18" y="20.32" length="middle" rot="R180"/>
<pin name="DIO8/TIM0CK_GT/PC1/PWM4" x="43.18" y="7.62" length="middle" rot="R180"/>
<pin name="DIO9/TIM0CAP/32KXTALIN/RXD1/32KIN" x="43.18" y="5.08" length="middle" rot="R180"/>
<pin name="DIO10TIM0OUT/32KXTALOUT" x="43.18" y="2.54" length="middle" rot="R180"/>
<pin name="DIO11/PWM1/TXD1" x="43.18" y="0" length="middle" rot="R180"/>
<pin name="DIO12/PWM2/CTS0/JTAG_TCK/ADO" x="43.18" y="-2.54" length="middle" rot="R180"/>
<pin name="DIO13/PWM3/RTS0/JTAG_TMS/ADE" x="43.18" y="-5.08" length="middle" rot="R180"/>
<pin name="DIO1SPISEL2/ADC4/PC0" x="43.18" y="25.4" length="middle" rot="R180"/>
<pin name="DIO0/SPISEL1/ADC3" x="43.18" y="27.94" length="middle" rot="R180"/>
<pin name="VDD" x="-22.86" y="-17.78" length="middle" direction="pwr"/>
<pin name="RESETN" x="-22.86" y="12.7" length="middle" direction="in"/>
<pin name="GND" x="-22.86" y="-20.32" length="middle" direction="pwr"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="JN5168-001-M0X">
<description>Jennic 5139/5148 Zigbee Module</description>
<gates>
<gate name="G$1" symbol="JN5168-001-M0X" x="0" y="0"/>
</gates>
<devices>
<device name="MODULE" package="JENNIC5168MODULE">
<connects>
<connect gate="G$1" pin="ADC1" pad="ADC1"/>
<connect gate="G$1" pin="ADC2" pad="ADC2"/>
<connect gate="G$1" pin="DIO0/SPISEL1/ADC3" pad="DIO0"/>
<connect gate="G$1" pin="DIO10TIM0OUT/32KXTALOUT" pad="DIO10"/>
<connect gate="G$1" pin="DIO11/PWM1/TXD1" pad="DIO11"/>
<connect gate="G$1" pin="DIO12/PWM2/CTS0/JTAG_TCK/ADO" pad="DIO12"/>
<connect gate="G$1" pin="DIO13/PWM3/RTS0/JTAG_TMS/ADE" pad="DIO13"/>
<connect gate="G$1" pin="DIO14/SIF_CLK" pad="DIO14"/>
<connect gate="G$1" pin="DIO15/SIF_D" pad="DIO15"/>
<connect gate="G$1" pin="DIO16/COMP1P" pad="DIO16"/>
<connect gate="G$1" pin="DIO17/COMP1M/PWM4" pad="DIO17"/>
<connect gate="G$1" pin="DIO1SPISEL2/ADC4/PC0" pad="DIO1"/>
<connect gate="G$1" pin="DIO2/RFRX/TIM0CK_GT" pad="DIO2"/>
<connect gate="G$1" pin="DIO3/RFTX/TIM0CAP" pad="DIO3"/>
<connect gate="G$1" pin="DIO4/CTS0/JTAG_TCK/TIM0OUT/PC0" pad="DIO4"/>
<connect gate="G$1" pin="DIO5/RTS0/JTAG_TMS/PWM1/PC1" pad="DIO5"/>
<connect gate="G$1" pin="DIO6/TXD0/JTAG_TDO/PWM2" pad="DIO6"/>
<connect gate="G$1" pin="DIO7/RXD0/JTAG_TDI/PWM3" pad="DIO7"/>
<connect gate="G$1" pin="DIO8/TIM0CK_GT/PC1/PWM4" pad="DIO8"/>
<connect gate="G$1" pin="DIO9/TIM0CAP/32KXTALIN/RXD1/32KIN" pad="DIO9"/>
<connect gate="G$1" pin="GND" pad="GND"/>
<connect gate="G$1" pin="RESETN" pad="RESETN"/>
<connect gate="G$1" pin="SPICLK/PWM2" pad="SPICLK"/>
<connect gate="G$1" pin="SPIMISO" pad="SPIMISO"/>
<connect gate="G$1" pin="SPIMOSI/PWM3" pad="SPIMOSI"/>
<connect gate="G$1" pin="SPISEL0" pad="SPISSZ"/>
<connect gate="G$1" pin="VDD" pad="VDD"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="FRAME1" library="frames" deviceset="DINA4_L" device=""/>
<part name="JP1" library="pinhead" deviceset="PINHD-1X1" device=""/>
<part name="JP11" library="pinhead" deviceset="PINHD-1X1" device=""/>
<part name="JP12" library="pinhead" deviceset="PINHD-1X1" device=""/>
<part name="JP13" library="pinhead" deviceset="PINHD-1X1" device=""/>
<part name="JP14" library="pinhead" deviceset="PINHD-1X1" device=""/>
<part name="JP15" library="pinhead" deviceset="PINHD-1X1" device=""/>
<part name="JP16" library="pinhead" deviceset="PINHD-1X1" device=""/>
<part name="JP17" library="pinhead" deviceset="PINHD-1X1" device=""/>
<part name="JP18" library="pinhead" deviceset="PINHD-1X1" device=""/>
<part name="U$1" library="jennic" deviceset="JN5168-001-M0X" device="MODULE"/>
<part name="JP2" library="pinhead" deviceset="PINHD-1X1" device=""/>
<part name="JP3" library="pinhead" deviceset="PINHD-1X1" device=""/>
<part name="JP4" library="pinhead" deviceset="PINHD-1X1" device=""/>
<part name="JP5" library="pinhead" deviceset="PINHD-1X1" device=""/>
<part name="JP6" library="pinhead" deviceset="PINHD-1X1" device=""/>
<part name="JP7" library="pinhead" deviceset="PINHD-1X1" device=""/>
<part name="JP8" library="pinhead" deviceset="PINHD-1X1" device=""/>
<part name="JP9" library="pinhead" deviceset="PINHD-1X1" device=""/>
<part name="JP10" library="pinhead" deviceset="PINHD-1X1" device=""/>
<part name="JP19" library="pinhead" deviceset="PINHD-1X1" device=""/>
<part name="JP20" library="pinhead" deviceset="PINHD-1X1" device=""/>
<part name="JP21" library="pinhead" deviceset="PINHD-1X1" device=""/>
<part name="JP22" library="pinhead" deviceset="PINHD-1X1" device=""/>
<part name="JP23" library="pinhead" deviceset="PINHD-1X1" device=""/>
<part name="JP24" library="pinhead" deviceset="PINHD-1X1" device=""/>
<part name="JP25" library="pinhead" deviceset="PINHD-1X1" device=""/>
<part name="JP26" library="pinhead" deviceset="PINHD-1X1" device=""/>
<part name="JP27" library="pinhead" deviceset="PINHD-1X1" device=""/>
</parts>
<sheets>
<sheet>
<plain>
</plain>
<instances>
<instance part="FRAME1" gate="G$1" x="0" y="0"/>
<instance part="FRAME1" gate="G$2" x="162.56" y="0"/>
<instance part="JP1" gate="G$1" x="114.3" y="149.86" smashed="yes" rot="MR180">
<attribute name="VALUE" x="107.95" y="154.94" size="1.778" layer="96" rot="MR180"/>
</instance>
<instance part="JP11" gate="G$1" x="114.3" y="147.32" smashed="yes" rot="MR180">
<attribute name="VALUE" x="107.95" y="152.4" size="1.778" layer="96" rot="MR180"/>
</instance>
<instance part="JP12" gate="G$1" x="114.3" y="144.78" smashed="yes" rot="MR180">
<attribute name="VALUE" x="107.95" y="149.86" size="1.778" layer="96" rot="MR180"/>
</instance>
<instance part="JP13" gate="G$1" x="114.3" y="139.7" smashed="yes" rot="MR180">
<attribute name="VALUE" x="107.95" y="144.78" size="1.778" layer="96" rot="MR180"/>
</instance>
<instance part="JP14" gate="G$1" x="114.3" y="134.62" smashed="yes" rot="MR180">
<attribute name="VALUE" x="107.95" y="139.7" size="1.778" layer="96" rot="MR180"/>
</instance>
<instance part="JP15" gate="G$1" x="114.3" y="129.54" smashed="yes" rot="MR180">
<attribute name="VALUE" x="107.95" y="134.62" size="1.778" layer="96" rot="MR180"/>
</instance>
<instance part="JP16" gate="G$1" x="114.3" y="124.46" smashed="yes" rot="MR180">
<attribute name="VALUE" x="107.95" y="129.54" size="1.778" layer="96" rot="MR180"/>
</instance>
<instance part="JP17" gate="G$1" x="114.3" y="119.38" smashed="yes" rot="MR180">
<attribute name="VALUE" x="107.95" y="124.46" size="1.778" layer="96" rot="MR180"/>
</instance>
<instance part="JP18" gate="G$1" x="114.3" y="114.3" smashed="yes" rot="MR180">
<attribute name="VALUE" x="107.95" y="119.38" size="1.778" layer="96" rot="MR180"/>
</instance>
<instance part="U$1" gate="G$1" x="55.88" y="121.92"/>
<instance part="JP2" gate="G$1" x="114.3" y="109.22" smashed="yes" rot="MR180">
<attribute name="VALUE" x="107.95" y="114.3" size="1.778" layer="96" rot="MR180"/>
</instance>
<instance part="JP3" gate="G$1" x="114.3" y="106.68" smashed="yes" rot="MR180">
<attribute name="VALUE" x="107.95" y="111.76" size="1.778" layer="96" rot="MR180"/>
</instance>
<instance part="JP4" gate="G$1" x="20.32" y="149.86" smashed="yes" rot="R180">
<attribute name="VALUE" x="26.67" y="154.94" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="JP5" gate="G$1" x="20.32" y="147.32" smashed="yes" rot="R180">
<attribute name="VALUE" x="26.67" y="152.4" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="JP6" gate="G$1" x="20.32" y="144.78" smashed="yes" rot="R180">
<attribute name="VALUE" x="26.67" y="149.86" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="JP7" gate="G$1" x="20.32" y="142.24" smashed="yes" rot="R180">
<attribute name="VALUE" x="26.67" y="147.32" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="JP8" gate="G$1" x="20.32" y="134.62" smashed="yes" rot="R180">
<attribute name="VALUE" x="26.67" y="139.7" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="JP9" gate="G$1" x="20.32" y="121.92" smashed="yes" rot="R180">
<attribute name="VALUE" x="26.67" y="127" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="JP10" gate="G$1" x="20.32" y="104.14" smashed="yes" rot="R180">
<attribute name="VALUE" x="26.67" y="109.22" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="JP19" gate="G$1" x="20.32" y="101.6" smashed="yes" rot="R180">
<attribute name="VALUE" x="26.67" y="106.68" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="JP20" gate="G$1" x="114.3" y="137.16" smashed="yes" rot="MR180">
<attribute name="VALUE" x="107.95" y="142.24" size="1.778" layer="96" rot="MR180"/>
</instance>
<instance part="JP21" gate="G$1" x="114.3" y="142.24" smashed="yes" rot="MR180">
<attribute name="VALUE" x="107.95" y="147.32" size="1.778" layer="96" rot="MR180"/>
</instance>
<instance part="JP22" gate="G$1" x="114.3" y="132.08" smashed="yes" rot="MR180">
<attribute name="VALUE" x="107.95" y="137.16" size="1.778" layer="96" rot="MR180"/>
</instance>
<instance part="JP23" gate="G$1" x="114.3" y="127" smashed="yes" rot="MR180">
<attribute name="VALUE" x="107.95" y="132.08" size="1.778" layer="96" rot="MR180"/>
</instance>
<instance part="JP24" gate="G$1" x="114.3" y="121.92" smashed="yes" rot="MR180">
<attribute name="VALUE" x="107.95" y="127" size="1.778" layer="96" rot="MR180"/>
</instance>
<instance part="JP25" gate="G$1" x="114.3" y="116.84" smashed="yes" rot="MR180">
<attribute name="VALUE" x="107.95" y="121.92" size="1.778" layer="96" rot="MR180"/>
</instance>
<instance part="JP26" gate="G$1" x="114.3" y="111.76" smashed="yes" rot="MR180">
<attribute name="VALUE" x="107.95" y="116.84" size="1.778" layer="96" rot="MR180"/>
</instance>
<instance part="JP27" gate="G$1" x="20.32" y="119.38" smashed="yes" rot="R180">
<attribute name="VALUE" x="26.67" y="124.46" size="1.778" layer="96" rot="R180"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="DIO4" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="DIO4/CTS0/JTAG_TCK/TIM0OUT/PC0"/>
<wire x1="99.06" y1="139.7" x2="111.76" y2="139.7" width="0.1524" layer="91"/>
<pinref part="JP13" gate="G$1" pin="1"/>
</segment>
</net>
<net name="DIO6" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="DIO6/TXD0/JTAG_TDO/PWM2"/>
<wire x1="99.06" y1="134.62" x2="111.76" y2="134.62" width="0.1524" layer="91"/>
<pinref part="JP14" gate="G$1" pin="1"/>
</segment>
</net>
<net name="DIO5" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="DIO5/RTS0/JTAG_TMS/PWM1/PC1"/>
<wire x1="99.06" y1="137.16" x2="111.76" y2="137.16" width="0.1524" layer="91"/>
<pinref part="JP20" gate="G$1" pin="1"/>
</segment>
</net>
<net name="ADC2" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="ADC2"/>
<wire x1="33.02" y1="119.38" x2="22.86" y2="119.38" width="0.1524" layer="91"/>
<pinref part="JP27" gate="G$1" pin="1"/>
</segment>
</net>
<net name="DIO0" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="DIO0/SPISEL1/ADC3"/>
<wire x1="99.06" y1="149.86" x2="111.76" y2="149.86" width="0.1524" layer="91"/>
<pinref part="JP1" gate="G$1" pin="1"/>
</segment>
</net>
<net name="DIO1" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="DIO1SPISEL2/ADC4/PC0"/>
<wire x1="99.06" y1="147.32" x2="111.76" y2="147.32" width="0.1524" layer="91"/>
<pinref part="JP11" gate="G$1" pin="1"/>
</segment>
</net>
<net name="DIO2" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="DIO2/RFRX/TIM0CK_GT"/>
<wire x1="99.06" y1="144.78" x2="111.76" y2="144.78" width="0.1524" layer="91"/>
<pinref part="JP12" gate="G$1" pin="1"/>
</segment>
</net>
<net name="DIO3" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="DIO3/RFTX/TIM0CAP"/>
<wire x1="99.06" y1="142.24" x2="111.76" y2="142.24" width="0.1524" layer="91"/>
<pinref part="JP21" gate="G$1" pin="1"/>
</segment>
</net>
<net name="DIO7" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="DIO7/RXD0/JTAG_TDI/PWM3"/>
<wire x1="99.06" y1="132.08" x2="111.76" y2="132.08" width="0.1524" layer="91"/>
<pinref part="JP22" gate="G$1" pin="1"/>
</segment>
</net>
<net name="DIO8" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="DIO8/TIM0CK_GT/PC1/PWM4"/>
<wire x1="99.06" y1="129.54" x2="111.76" y2="129.54" width="0.1524" layer="91"/>
<pinref part="JP15" gate="G$1" pin="1"/>
</segment>
</net>
<net name="DIO9" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="DIO9/TIM0CAP/32KXTALIN/RXD1/32KIN"/>
<wire x1="99.06" y1="127" x2="111.76" y2="127" width="0.1524" layer="91"/>
<pinref part="JP23" gate="G$1" pin="1"/>
</segment>
</net>
<net name="DIO10" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="DIO10TIM0OUT/32KXTALOUT"/>
<wire x1="99.06" y1="124.46" x2="111.76" y2="124.46" width="0.1524" layer="91"/>
<pinref part="JP16" gate="G$1" pin="1"/>
</segment>
</net>
<net name="DIO11" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="DIO11/PWM1/TXD1"/>
<wire x1="99.06" y1="121.92" x2="111.76" y2="121.92" width="0.1524" layer="91"/>
<pinref part="JP24" gate="G$1" pin="1"/>
</segment>
</net>
<net name="DIO12" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="DIO12/PWM2/CTS0/JTAG_TCK/ADO"/>
<wire x1="99.06" y1="119.38" x2="111.76" y2="119.38" width="0.1524" layer="91"/>
<pinref part="JP17" gate="G$1" pin="1"/>
</segment>
</net>
<net name="DIO13" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="DIO13/PWM3/RTS0/JTAG_TMS/ADE"/>
<wire x1="99.06" y1="116.84" x2="111.76" y2="116.84" width="0.1524" layer="91"/>
<pinref part="JP25" gate="G$1" pin="1"/>
</segment>
</net>
<net name="DIO14" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="DIO14/SIF_CLK"/>
<wire x1="99.06" y1="114.3" x2="111.76" y2="114.3" width="0.1524" layer="91"/>
<pinref part="JP18" gate="G$1" pin="1"/>
</segment>
</net>
<net name="DIO15" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="DIO15/SIF_D"/>
<wire x1="99.06" y1="111.76" x2="111.76" y2="111.76" width="0.1524" layer="91"/>
<pinref part="JP26" gate="G$1" pin="1"/>
</segment>
</net>
<net name="DIO16" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="DIO16/COMP1P"/>
<wire x1="99.06" y1="109.22" x2="111.76" y2="109.22" width="0.1524" layer="91"/>
<pinref part="JP2" gate="G$1" pin="1"/>
</segment>
</net>
<net name="DIO17" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="DIO17/COMP1M/PWM4"/>
<wire x1="99.06" y1="106.68" x2="111.76" y2="106.68" width="0.1524" layer="91"/>
<pinref part="JP3" gate="G$1" pin="1"/>
</segment>
</net>
<net name="ADC1" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="ADC1"/>
<wire x1="33.02" y1="121.92" x2="22.86" y2="121.92" width="0.1524" layer="91"/>
<pinref part="JP9" gate="G$1" pin="1"/>
</segment>
</net>
<net name="RESETN" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="RESETN"/>
<wire x1="33.02" y1="134.62" x2="22.86" y2="134.62" width="0.1524" layer="91"/>
<pinref part="JP8" gate="G$1" pin="1"/>
</segment>
</net>
<net name="SPIMOSI" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="SPIMOSI/PWM3"/>
<wire x1="33.02" y1="142.24" x2="22.86" y2="142.24" width="0.1524" layer="91"/>
<pinref part="JP7" gate="G$1" pin="1"/>
</segment>
</net>
<net name="SPIMISO" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="SPIMISO"/>
<wire x1="33.02" y1="144.78" x2="22.86" y2="144.78" width="0.1524" layer="91"/>
<pinref part="JP6" gate="G$1" pin="1"/>
</segment>
</net>
<net name="SPICLK" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="SPICLK/PWM2"/>
<wire x1="33.02" y1="147.32" x2="22.86" y2="147.32" width="0.1524" layer="91"/>
<pinref part="JP5" gate="G$1" pin="1"/>
</segment>
</net>
<net name="SPISSZ" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="SPISEL0"/>
<wire x1="33.02" y1="149.86" x2="22.86" y2="149.86" width="0.1524" layer="91"/>
<pinref part="JP4" gate="G$1" pin="1"/>
</segment>
</net>
<net name="VDD" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="VDD"/>
<wire x1="33.02" y1="104.14" x2="22.86" y2="104.14" width="0.1524" layer="91"/>
<pinref part="JP10" gate="G$1" pin="1"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="GND"/>
<wire x1="33.02" y1="101.6" x2="22.86" y2="101.6" width="0.1524" layer="91"/>
<pinref part="JP19" gate="G$1" pin="1"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
</eagle>
