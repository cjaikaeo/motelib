<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="6.1">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="yes" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="jennic">
<packages>
<package name="JENNIC5139MODULE">
<description>Footprint for the Jennic 5139 based modules</description>
<smd name="VSSA" x="15.24" y="0" dx="0.8" dy="1.5" layer="1" roundness="100" rot="R180"/>
<smd name="DIO4/CTS0" x="2.54" y="0" dx="0.8" dy="1.5" layer="1" roundness="100" rot="R180"/>
<smd name="DIO5/RTS0" x="3.81" y="0" dx="0.8" dy="1.5" layer="1" roundness="100" rot="R180"/>
<smd name="DIO6/TXD0" x="5.08" y="0" dx="0.8" dy="1.5" layer="1" roundness="100" rot="R180"/>
<smd name="DIO7/RXD0" x="6.35" y="0" dx="0.8" dy="1.5" layer="1" roundness="100" rot="R180"/>
<smd name="DIO8/TIM0GT" x="7.62" y="0" dx="0.8" dy="1.5" layer="1" roundness="100" rot="R180"/>
<smd name="DIO9/TIM0_CAP" x="8.89" y="0" dx="0.8" dy="1.5" layer="1" roundness="100" rot="R180"/>
<smd name="DIO10/TIM0_OUT" x="10.16" y="0" dx="0.8" dy="1.5" layer="1" roundness="100" rot="R180"/>
<smd name="DIO11/TIM1GT" x="11.43" y="0" dx="0.8" dy="1.5" layer="1" roundness="100" rot="R180"/>
<smd name="VDD" x="12.7" y="0" dx="0.8" dy="1.5" layer="1" roundness="100" rot="R180"/>
<smd name="GND" x="13.97" y="0" dx="0.8" dy="1.5" layer="1" roundness="100" rot="R180"/>
<smd name="DIO3/SPISEL4" x="0" y="2.54" dx="0.8" dy="1.5" layer="1" roundness="100" rot="R90"/>
<smd name="SPISWP" x="0" y="3.81" dx="0.8" dy="1.5" layer="1" roundness="100" rot="R90"/>
<smd name="SPISSM" x="0" y="5.08" dx="0.8" dy="1.5" layer="1" roundness="100" rot="R90"/>
<smd name="DIO2/SPISEL3" x="0" y="6.35" dx="0.8" dy="1.5" layer="1" roundness="100" rot="R90"/>
<smd name="DIO1/SPISEL2" x="0" y="7.62" dx="0.8" dy="1.5" layer="1" roundness="100" rot="R90"/>
<smd name="DIO0/SPISEL1" x="0" y="8.89" dx="0.8" dy="1.5" layer="1" roundness="100" rot="R90"/>
<smd name="SPISSZ" x="0" y="10.16" dx="0.8" dy="1.5" layer="1" roundness="100" rot="R90"/>
<smd name="SPIMOSI" x="0" y="11.43" dx="0.8" dy="1.5" layer="1" roundness="100" rot="R90"/>
<smd name="SPIMISO" x="0" y="12.7" dx="0.8" dy="1.5" layer="1" roundness="100" rot="R90"/>
<smd name="SPICLK" x="0" y="13.97" dx="0.8" dy="1.5" layer="1" roundness="100" rot="R90"/>
<smd name="COMP2-" x="0" y="15.24" dx="0.8" dy="1.5" layer="1" roundness="100" rot="R90"/>
<smd name="COMP2+" x="0" y="16.51" dx="0.8" dy="1.5" layer="1" roundness="100" rot="R90"/>
<smd name="DAC2" x="0" y="17.78" dx="0.8" dy="1.5" layer="1" roundness="100" rot="R90"/>
<smd name="DAC1" x="0" y="19.05" dx="0.8" dy="1.5" layer="1" roundness="100" rot="R90"/>
<smd name="ADC4" x="0" y="20.32" dx="0.8" dy="1.5" layer="1" roundness="100" rot="R270"/>
<smd name="ADC3" x="18" y="20.32" dx="0.8" dy="1.5" layer="1" roundness="100" rot="R90"/>
<smd name="ADC2" x="18" y="19.05" dx="0.8" dy="1.5" layer="1" roundness="100" rot="R90"/>
<smd name="ADC1" x="18" y="17.78" dx="0.8" dy="1.5" layer="1" roundness="100" rot="R90"/>
<smd name="COMP1+" x="18" y="16.51" dx="0.8" dy="1.5" layer="1" roundness="100" rot="R90"/>
<smd name="COMP1-" x="18" y="15.24" dx="0.8" dy="1.5" layer="1" roundness="100" rot="R90"/>
<smd name="DIO20/RXD1" x="18" y="13.97" dx="0.8" dy="1.5" layer="1" roundness="100" rot="R90"/>
<smd name="DIO19/TXD1" x="18" y="12.7" dx="0.8" dy="1.5" layer="1" roundness="100" rot="R90"/>
<smd name="DIO18/RTS1" x="18" y="11.43" dx="0.8" dy="1.5" layer="1" roundness="100" rot="R90"/>
<smd name="DIO17/CTS1" x="18" y="10.16" dx="0.8" dy="1.5" layer="1" roundness="100" rot="R90"/>
<smd name="DIO16" x="18" y="8.89" dx="0.8" dy="1.5" layer="1" roundness="100" rot="R90"/>
<smd name="DIO15/SIF_D" x="18" y="7.62" dx="0.8" dy="1.5" layer="1" roundness="100" rot="R90"/>
<smd name="DIO14/SIF_CLK" x="18" y="6.35" dx="0.8" dy="1.5" layer="1" roundness="100" rot="R90"/>
<smd name="RESETN" x="18" y="5.08" dx="0.8" dy="1.5" layer="1" roundness="100" rot="R90"/>
<smd name="DIO13/TIM1_OUT" x="18" y="3.81" dx="0.8" dy="1.5" layer="1" roundness="100" rot="R90"/>
<smd name="DIO12/TIM1_CAP" x="18" y="2.54" dx="0.8" dy="1.5" layer="1" roundness="100" rot="R90"/>
<text x="2.54" y="1.27" size="1.778" layer="21">JN5148MO</text>
<wire x1="0" y1="20.955" x2="0" y2="19.685" width="0.254" layer="51" curve="-180"/>
<wire x1="0" y1="19.685" x2="0" y2="18.415" width="0.254" layer="51" curve="-180"/>
<wire x1="0" y1="18.415" x2="0" y2="17.145" width="0.254" layer="51" curve="-180"/>
<wire x1="0" y1="17.145" x2="0" y2="15.875" width="0.254" layer="51" curve="-180"/>
<wire x1="0" y1="15.875" x2="0" y2="14.605" width="0.254" layer="51" curve="-180"/>
<wire x1="0" y1="14.605" x2="0" y2="13.335" width="0.254" layer="51" curve="-180"/>
<wire x1="0" y1="13.335" x2="0" y2="12.065" width="0.254" layer="51" curve="-180"/>
<wire x1="0" y1="12.065" x2="0" y2="10.795" width="0.254" layer="51" curve="-180"/>
<wire x1="0" y1="10.795" x2="0" y2="9.525" width="0.254" layer="51" curve="-180"/>
<wire x1="0" y1="9.525" x2="0" y2="8.255" width="0.254" layer="51" curve="-180"/>
<wire x1="0" y1="8.255" x2="0" y2="6.985" width="0.254" layer="51" curve="-180"/>
<wire x1="0" y1="6.985" x2="0" y2="5.715" width="0.254" layer="51" curve="-180"/>
<wire x1="0" y1="5.715" x2="0" y2="4.445" width="0.254" layer="51" curve="-180"/>
<wire x1="0" y1="4.445" x2="0" y2="3.175" width="0.254" layer="51" curve="-180"/>
<wire x1="0" y1="3.175" x2="0" y2="1.905" width="0.254" layer="51" curve="-180"/>
<wire x1="18" y1="19.685" x2="18" y2="20.955" width="0.254" layer="51" curve="-180"/>
<wire x1="1.905" y1="0" x2="3.175" y2="0" width="0.254" layer="51" curve="-180"/>
<wire x1="3.175" y1="0" x2="4.445" y2="0" width="0.254" layer="51" curve="-180"/>
<wire x1="4.445" y1="0" x2="5.715" y2="0" width="0.254" layer="51" curve="-180"/>
<wire x1="5.715" y1="0" x2="6.985" y2="0" width="0.254" layer="51" curve="-180"/>
<wire x1="6.985" y1="0" x2="8.255" y2="0" width="0.254" layer="51" curve="-180"/>
<wire x1="8.255" y1="0" x2="9.525" y2="0" width="0.254" layer="51" curve="-180"/>
<wire x1="9.525" y1="0" x2="10.795" y2="0" width="0.254" layer="51" curve="-180"/>
<wire x1="10.795" y1="0" x2="12.065" y2="0" width="0.254" layer="51" curve="-180"/>
<wire x1="12.065" y1="0" x2="13.335" y2="0" width="0.254" layer="51" curve="-180"/>
<wire x1="13.335" y1="0" x2="14.605" y2="0" width="0.254" layer="51" curve="-180"/>
<wire x1="14.605" y1="0" x2="15.875" y2="0" width="0.254" layer="51" curve="-180"/>
<wire x1="18" y1="18.415" x2="18" y2="19.685" width="0.254" layer="51" curve="-180"/>
<wire x1="18" y1="17.145" x2="18" y2="18.415" width="0.254" layer="51" curve="-180"/>
<wire x1="18" y1="15.875" x2="18" y2="17.145" width="0.254" layer="51" curve="-180"/>
<wire x1="18" y1="14.605" x2="18" y2="15.875" width="0.254" layer="51" curve="-180"/>
<wire x1="18" y1="13.335" x2="18" y2="14.605" width="0.254" layer="51" curve="-180"/>
<wire x1="18" y1="12.065" x2="18" y2="13.335" width="0.254" layer="51" curve="-180"/>
<wire x1="18" y1="10.795" x2="18" y2="12.065" width="0.254" layer="51" curve="-180"/>
<wire x1="18" y1="9.525" x2="18" y2="10.795" width="0.254" layer="51" curve="-180"/>
<wire x1="18" y1="8.255" x2="18" y2="9.525" width="0.254" layer="51" curve="-180"/>
<wire x1="18" y1="6.985" x2="18" y2="8.255" width="0.254" layer="51" curve="-180"/>
<wire x1="18" y1="5.715" x2="18" y2="6.985" width="0.254" layer="51" curve="-180"/>
<wire x1="18" y1="4.445" x2="18" y2="5.715" width="0.254" layer="51" curve="-180"/>
<wire x1="18" y1="3.175" x2="18" y2="4.445" width="0.254" layer="51" curve="-180"/>
<wire x1="18" y1="1.905" x2="18" y2="3.175" width="0.254" layer="51" curve="-180"/>
<wire x1="15.875" y1="0" x2="18" y2="0" width="0.4064" layer="21"/>
<wire x1="18" y1="0" x2="18" y2="1.905" width="0.4064" layer="21"/>
<wire x1="0" y1="20.955" x2="0" y2="29.845" width="0.4064" layer="21"/>
<wire x1="0" y1="29.845" x2="18" y2="29.845" width="0.4064" layer="21"/>
<wire x1="18" y1="29.845" x2="18" y2="20.955" width="0.4064" layer="21"/>
<wire x1="1.905" y1="0" x2="0" y2="0" width="0.4064" layer="21"/>
<wire x1="0" y1="0" x2="0" y2="1.905" width="0.4064" layer="21"/>
</package>
<package name="IWING-JN-BREAKOUT">
<wire x1="0" y1="0" x2="0" y2="29.21" width="0.254" layer="21"/>
<wire x1="0" y1="29.21" x2="35.56" y2="29.21" width="0.254" layer="21"/>
<wire x1="35.56" y1="29.21" x2="35.56" y2="0" width="0.254" layer="21"/>
<wire x1="35.56" y1="0" x2="0" y2="0" width="0.254" layer="21"/>
<pad name="ADC4" x="2.54" y="25.4" drill="1.016" shape="octagon"/>
<pad name="DAC1" x="5.08" y="25.4" drill="1.016" shape="octagon"/>
<pad name="DAC2" x="2.54" y="22.86" drill="1.016" shape="octagon"/>
<pad name="COMP2+" x="5.08" y="22.86" drill="1.016" shape="octagon"/>
<pad name="COMP2-" x="2.54" y="20.32" drill="1.016" shape="octagon"/>
<pad name="SPICLK" x="5.08" y="20.32" drill="1.016" shape="octagon"/>
<pad name="SPIMISO" x="2.54" y="17.78" drill="1.016" shape="octagon"/>
<pad name="SPIMOSI" x="5.08" y="17.78" drill="1.016" shape="octagon"/>
<pad name="SPISSZ" x="2.54" y="15.24" drill="1.016" shape="octagon"/>
<pad name="DIO0" x="5.08" y="15.24" drill="1.016" shape="octagon"/>
<pad name="DIO1" x="2.54" y="12.7" drill="1.016" shape="octagon"/>
<pad name="DIO2" x="5.08" y="12.7" drill="1.016" shape="octagon"/>
<pad name="SPISSM" x="2.54" y="10.16" drill="1.016" shape="octagon"/>
<pad name="SPISWP" x="5.08" y="10.16" drill="1.016" shape="octagon"/>
<pad name="DIO3" x="2.54" y="7.62" drill="1.016" shape="octagon"/>
<pad name="DIO4" x="5.08" y="2.54" drill="1.016" shape="octagon"/>
<pad name="DIO5" x="7.62" y="2.54" drill="1.016" shape="octagon"/>
<pad name="DIO6" x="10.16" y="2.54" drill="1.016" shape="octagon"/>
<pad name="DIO7" x="12.7" y="2.54" drill="1.016" shape="octagon"/>
<pad name="DIO8" x="15.24" y="2.54" drill="1.016" shape="octagon"/>
<pad name="DIO9" x="17.78" y="2.54" drill="1.016" shape="octagon"/>
<pad name="DIO10" x="20.32" y="2.54" drill="1.016" shape="octagon"/>
<pad name="DIO11" x="22.86" y="2.54" drill="1.016" shape="octagon"/>
<pad name="VDD" x="25.4" y="2.54" drill="1.016" shape="octagon"/>
<pad name="GND" x="27.94" y="2.54" drill="1.016" shape="octagon"/>
<pad name="VSSA" x="30.48" y="2.54" drill="1.016" shape="octagon"/>
<pad name="DIO12" x="33.02" y="7.62" drill="1.016" shape="octagon"/>
<pad name="DIO13" x="30.48" y="10.16" drill="1.016" shape="octagon"/>
<pad name="RESETN" x="33.02" y="10.16" drill="1.016" shape="octagon"/>
<pad name="DIO14" x="30.48" y="12.7" drill="1.016" shape="octagon"/>
<pad name="DIO15" x="33.02" y="12.7" drill="1.016" shape="octagon"/>
<pad name="DIO16" x="30.48" y="15.24" drill="1.016" shape="octagon"/>
<pad name="DIO17" x="33.02" y="15.24" drill="1.016" shape="octagon"/>
<pad name="DIO18" x="30.48" y="17.78" drill="1.016" shape="octagon"/>
<pad name="DIO19" x="33.02" y="17.78" drill="1.016" shape="octagon"/>
<pad name="DIO20" x="30.48" y="20.32" drill="1.016" shape="octagon"/>
<pad name="COMP1-" x="33.02" y="20.32" drill="1.016" shape="octagon"/>
<pad name="COMP1+" x="30.48" y="22.86" drill="1.016" shape="octagon"/>
<pad name="ADC1" x="33.02" y="22.86" drill="1.016" shape="octagon"/>
<pad name="ADC2" x="30.48" y="25.4" drill="1.016" shape="octagon"/>
<pad name="ADC3" x="33.02" y="25.4" drill="1.016" shape="octagon"/>
<pad name="NA1" x="5.08" y="7.62" drill="1.016" shape="octagon"/>
<pad name="NA2" x="30.48" y="7.62" drill="1.016" shape="octagon"/>
<wire x1="8.89" y1="6.35" x2="8.89" y2="35.56" width="0.254" layer="51" style="shortdash"/>
<wire x1="8.89" y1="35.56" x2="26.67" y2="35.56" width="0.254" layer="51" style="shortdash"/>
<wire x1="26.67" y1="35.56" x2="26.67" y2="6.35" width="0.254" layer="51" style="shortdash"/>
<wire x1="26.67" y1="6.35" x2="8.89" y2="6.35" width="0.254" layer="51" style="shortdash"/>
<rectangle x1="4.826" y1="2.286" x2="5.334" y2="2.794" layer="51" rot="R90"/>
<rectangle x1="25.146" y1="2.286" x2="25.654" y2="2.794" layer="51" rot="R90"/>
<rectangle x1="7.366" y1="2.286" x2="7.874" y2="2.794" layer="51" rot="R90"/>
<rectangle x1="27.686" y1="2.286" x2="28.194" y2="2.794" layer="51" rot="R90"/>
<rectangle x1="9.906" y1="2.286" x2="10.414" y2="2.794" layer="51" rot="R90"/>
<rectangle x1="30.226" y1="2.286" x2="30.734" y2="2.794" layer="51" rot="R90"/>
<rectangle x1="12.446" y1="2.286" x2="12.954" y2="2.794" layer="51" rot="R90"/>
<rectangle x1="14.986" y1="2.286" x2="15.494" y2="2.794" layer="51" rot="R90"/>
<rectangle x1="17.526" y1="2.286" x2="18.034" y2="2.794" layer="51" rot="R90"/>
<rectangle x1="20.066" y1="2.286" x2="20.574" y2="2.794" layer="51" rot="R90"/>
<rectangle x1="22.606" y1="2.286" x2="23.114" y2="2.794" layer="51" rot="R90"/>
<rectangle x1="2.286" y1="25.146" x2="2.794" y2="25.654" layer="51"/>
<rectangle x1="4.826" y1="25.146" x2="5.334" y2="25.654" layer="51"/>
<rectangle x1="2.286" y1="22.606" x2="2.794" y2="23.114" layer="51"/>
<rectangle x1="4.826" y1="22.606" x2="5.334" y2="23.114" layer="51"/>
<rectangle x1="2.286" y1="20.066" x2="2.794" y2="20.574" layer="51"/>
<rectangle x1="4.826" y1="20.066" x2="5.334" y2="20.574" layer="51"/>
<rectangle x1="2.286" y1="17.526" x2="2.794" y2="18.034" layer="51"/>
<rectangle x1="4.826" y1="17.526" x2="5.334" y2="18.034" layer="51"/>
<rectangle x1="2.286" y1="14.986" x2="2.794" y2="15.494" layer="51"/>
<rectangle x1="4.826" y1="14.986" x2="5.334" y2="15.494" layer="51"/>
<rectangle x1="2.286" y1="12.446" x2="2.794" y2="12.954" layer="51"/>
<rectangle x1="4.826" y1="12.446" x2="5.334" y2="12.954" layer="51"/>
<rectangle x1="2.286" y1="9.906" x2="2.794" y2="10.414" layer="51"/>
<rectangle x1="4.826" y1="9.906" x2="5.334" y2="10.414" layer="51"/>
<rectangle x1="2.286" y1="7.366" x2="2.794" y2="7.874" layer="51"/>
<rectangle x1="4.826" y1="7.366" x2="5.334" y2="7.874" layer="51"/>
<rectangle x1="30.226" y1="25.146" x2="30.734" y2="25.654" layer="51"/>
<rectangle x1="32.766" y1="25.146" x2="33.274" y2="25.654" layer="51"/>
<rectangle x1="30.226" y1="22.606" x2="30.734" y2="23.114" layer="51"/>
<rectangle x1="32.766" y1="22.606" x2="33.274" y2="23.114" layer="51"/>
<rectangle x1="30.226" y1="20.066" x2="30.734" y2="20.574" layer="51"/>
<rectangle x1="32.766" y1="20.066" x2="33.274" y2="20.574" layer="51"/>
<rectangle x1="30.226" y1="17.526" x2="30.734" y2="18.034" layer="51"/>
<rectangle x1="32.766" y1="17.526" x2="33.274" y2="18.034" layer="51"/>
<rectangle x1="30.226" y1="14.986" x2="30.734" y2="15.494" layer="51"/>
<rectangle x1="32.766" y1="14.986" x2="33.274" y2="15.494" layer="51"/>
<rectangle x1="30.226" y1="12.446" x2="30.734" y2="12.954" layer="51"/>
<rectangle x1="32.766" y1="12.446" x2="33.274" y2="12.954" layer="51"/>
<rectangle x1="30.226" y1="9.906" x2="30.734" y2="10.414" layer="51"/>
<rectangle x1="32.766" y1="9.906" x2="33.274" y2="10.414" layer="51"/>
<rectangle x1="30.226" y1="7.366" x2="30.734" y2="7.874" layer="51"/>
<rectangle x1="32.766" y1="7.366" x2="33.274" y2="7.874" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="JN5139-Z01-M00R1">
<wire x1="-22.86" y1="63.5" x2="12.7" y2="63.5" width="0.254" layer="94"/>
<wire x1="12.7" y1="63.5" x2="12.7" y2="2.54" width="0.254" layer="94"/>
<wire x1="12.7" y1="2.54" x2="-22.86" y2="2.54" width="0.254" layer="94"/>
<wire x1="-22.86" y1="2.54" x2="-22.86" y2="63.5" width="0.254" layer="94"/>
<text x="-5.08" y="15.24" size="1.778" layer="94" rot="R90">JN5139-Z01-M00R1</text>
<pin name="COMP1-" x="-27.94" y="5.08" length="middle" direction="in"/>
<pin name="COMP1+" x="-27.94" y="7.62" length="middle" direction="in"/>
<pin name="COMP2-" x="-27.94" y="10.16" length="middle" direction="in"/>
<pin name="COMP2+" x="-27.94" y="12.7" length="middle" direction="in"/>
<pin name="DAC1" x="-27.94" y="17.78" length="middle" direction="out"/>
<pin name="DAC2" x="-27.94" y="20.32" length="middle" direction="out"/>
<pin name="ADC1" x="-27.94" y="25.4" length="middle" direction="in"/>
<pin name="ADC2" x="-27.94" y="27.94" length="middle" direction="in"/>
<pin name="ADC3" x="-27.94" y="30.48" length="middle" direction="in"/>
<pin name="ADC4" x="-27.94" y="33.02" length="middle" direction="in"/>
<pin name="DIO4/CTS0" x="17.78" y="5.08" length="middle" rot="R180"/>
<pin name="DIO5/RTS0" x="17.78" y="7.62" length="middle" rot="R180"/>
<pin name="DIO6/TXD0" x="17.78" y="10.16" length="middle" rot="R180"/>
<pin name="DIO7/RXD0" x="17.78" y="12.7" length="middle" rot="R180"/>
<pin name="DIO17/CTS1" x="17.78" y="17.78" length="middle" rot="R180"/>
<pin name="DIO18/RTS1" x="17.78" y="20.32" length="middle" rot="R180"/>
<pin name="DIO19/TXD1" x="17.78" y="22.86" length="middle" rot="R180"/>
<pin name="DIO20/RXD1" x="17.78" y="25.4" length="middle" rot="R180"/>
<pin name="SPICLK" x="17.78" y="30.48" length="middle" direction="out" rot="R180"/>
<pin name="SPIMISO" x="17.78" y="33.02" length="middle" direction="in" rot="R180"/>
<pin name="SPIMOSI" x="17.78" y="35.56" length="middle" direction="out" rot="R180"/>
<pin name="SPISSZ" x="17.78" y="38.1" length="middle" direction="out" rot="R180"/>
<pin name="SPISSM" x="17.78" y="40.64" length="middle" direction="in" rot="R180"/>
<pin name="SPISWP" x="17.78" y="43.18" length="middle" direction="in" rot="R180"/>
<pin name="DIO0/SPISEL1" x="17.78" y="45.72" length="middle" rot="R180"/>
<pin name="DIO1/SPISEL2" x="17.78" y="48.26" length="middle" rot="R180"/>
<pin name="DIO2/SPISEL3" x="17.78" y="50.8" length="middle" rot="R180"/>
<pin name="DIO3/SPISEL4" x="17.78" y="53.34" length="middle" rot="R180"/>
<pin name="DIO8/TIM0GT" x="-27.94" y="38.1" length="middle"/>
<pin name="DIO9/TIM0_CAP" x="-27.94" y="40.64" length="middle"/>
<pin name="DIO10/TIM0_OUT" x="-27.94" y="43.18" length="middle"/>
<pin name="DIO11/TIM1GT" x="-27.94" y="45.72" length="middle"/>
<pin name="DIO12/TIM1_CAP" x="-27.94" y="48.26" length="middle"/>
<pin name="DIO13/TIM1_OUT" x="-27.94" y="50.8" length="middle"/>
<pin name="DIO14/SIF_CLK" x="-27.94" y="55.88" length="middle"/>
<pin name="DIO15/SIF_D" x="-27.94" y="58.42" length="middle"/>
<pin name="DIO16" x="17.78" y="58.42" length="middle" rot="R180"/>
<pin name="VDD" x="-2.54" y="68.58" length="middle" direction="pwr" rot="R270"/>
<pin name="RESETN" x="-10.16" y="-2.54" length="middle" direction="in" rot="R90"/>
<pin name="GND" x="-5.08" y="-2.54" length="middle" direction="pwr" rot="R90"/>
<pin name="VSSA" x="-2.54" y="-2.54" length="middle" direction="pwr" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="JN5139-Z01-M00R1">
<description>Jennic 5139/5148 Zigbee Module</description>
<gates>
<gate name="G$1" symbol="JN5139-Z01-M00R1" x="5.08" y="-22.86"/>
</gates>
<devices>
<device name="MODULE" package="JENNIC5139MODULE">
<connects>
<connect gate="G$1" pin="ADC1" pad="ADC1"/>
<connect gate="G$1" pin="ADC2" pad="ADC2"/>
<connect gate="G$1" pin="ADC3" pad="ADC3"/>
<connect gate="G$1" pin="ADC4" pad="ADC4"/>
<connect gate="G$1" pin="COMP1+" pad="COMP1+"/>
<connect gate="G$1" pin="COMP1-" pad="COMP1-"/>
<connect gate="G$1" pin="COMP2+" pad="COMP2+"/>
<connect gate="G$1" pin="COMP2-" pad="COMP2-"/>
<connect gate="G$1" pin="DAC1" pad="DAC1"/>
<connect gate="G$1" pin="DAC2" pad="DAC2"/>
<connect gate="G$1" pin="DIO0/SPISEL1" pad="DIO0/SPISEL1"/>
<connect gate="G$1" pin="DIO1/SPISEL2" pad="DIO1/SPISEL2"/>
<connect gate="G$1" pin="DIO10/TIM0_OUT" pad="DIO10/TIM0_OUT"/>
<connect gate="G$1" pin="DIO11/TIM1GT" pad="DIO11/TIM1GT"/>
<connect gate="G$1" pin="DIO12/TIM1_CAP" pad="DIO12/TIM1_CAP"/>
<connect gate="G$1" pin="DIO13/TIM1_OUT" pad="DIO13/TIM1_OUT"/>
<connect gate="G$1" pin="DIO14/SIF_CLK" pad="DIO14/SIF_CLK"/>
<connect gate="G$1" pin="DIO15/SIF_D" pad="DIO15/SIF_D"/>
<connect gate="G$1" pin="DIO16" pad="DIO16"/>
<connect gate="G$1" pin="DIO17/CTS1" pad="DIO17/CTS1"/>
<connect gate="G$1" pin="DIO18/RTS1" pad="DIO18/RTS1"/>
<connect gate="G$1" pin="DIO19/TXD1" pad="DIO19/TXD1"/>
<connect gate="G$1" pin="DIO2/SPISEL3" pad="DIO2/SPISEL3"/>
<connect gate="G$1" pin="DIO20/RXD1" pad="DIO20/RXD1"/>
<connect gate="G$1" pin="DIO3/SPISEL4" pad="DIO3/SPISEL4"/>
<connect gate="G$1" pin="DIO4/CTS0" pad="DIO4/CTS0"/>
<connect gate="G$1" pin="DIO5/RTS0" pad="DIO5/RTS0"/>
<connect gate="G$1" pin="DIO6/TXD0" pad="DIO6/TXD0"/>
<connect gate="G$1" pin="DIO7/RXD0" pad="DIO7/RXD0"/>
<connect gate="G$1" pin="DIO8/TIM0GT" pad="DIO8/TIM0GT"/>
<connect gate="G$1" pin="DIO9/TIM0_CAP" pad="DIO9/TIM0_CAP"/>
<connect gate="G$1" pin="GND" pad="GND"/>
<connect gate="G$1" pin="RESETN" pad="RESETN"/>
<connect gate="G$1" pin="SPICLK" pad="SPICLK"/>
<connect gate="G$1" pin="SPIMISO" pad="SPIMISO"/>
<connect gate="G$1" pin="SPIMOSI" pad="SPIMOSI"/>
<connect gate="G$1" pin="SPISSM" pad="SPISSM"/>
<connect gate="G$1" pin="SPISSZ" pad="SPISSZ"/>
<connect gate="G$1" pin="SPISWP" pad="SPISWP"/>
<connect gate="G$1" pin="VDD" pad="VDD"/>
<connect gate="G$1" pin="VSSA" pad="VSSA"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="BREAKOUT" package="IWING-JN-BREAKOUT">
<connects>
<connect gate="G$1" pin="ADC1" pad="ADC1"/>
<connect gate="G$1" pin="ADC2" pad="ADC2"/>
<connect gate="G$1" pin="ADC3" pad="ADC3"/>
<connect gate="G$1" pin="ADC4" pad="ADC4"/>
<connect gate="G$1" pin="COMP1+" pad="COMP1+"/>
<connect gate="G$1" pin="COMP1-" pad="COMP1-"/>
<connect gate="G$1" pin="COMP2+" pad="COMP2+"/>
<connect gate="G$1" pin="COMP2-" pad="COMP2-"/>
<connect gate="G$1" pin="DAC1" pad="DAC1"/>
<connect gate="G$1" pin="DAC2" pad="DAC2"/>
<connect gate="G$1" pin="DIO0/SPISEL1" pad="DIO0"/>
<connect gate="G$1" pin="DIO1/SPISEL2" pad="DIO1"/>
<connect gate="G$1" pin="DIO10/TIM0_OUT" pad="DIO10"/>
<connect gate="G$1" pin="DIO11/TIM1GT" pad="DIO11"/>
<connect gate="G$1" pin="DIO12/TIM1_CAP" pad="DIO12"/>
<connect gate="G$1" pin="DIO13/TIM1_OUT" pad="DIO13"/>
<connect gate="G$1" pin="DIO14/SIF_CLK" pad="DIO14"/>
<connect gate="G$1" pin="DIO15/SIF_D" pad="DIO15"/>
<connect gate="G$1" pin="DIO16" pad="DIO16"/>
<connect gate="G$1" pin="DIO17/CTS1" pad="DIO17"/>
<connect gate="G$1" pin="DIO18/RTS1" pad="DIO18"/>
<connect gate="G$1" pin="DIO19/TXD1" pad="DIO19"/>
<connect gate="G$1" pin="DIO2/SPISEL3" pad="DIO2"/>
<connect gate="G$1" pin="DIO20/RXD1" pad="DIO20"/>
<connect gate="G$1" pin="DIO3/SPISEL4" pad="DIO3"/>
<connect gate="G$1" pin="DIO4/CTS0" pad="DIO4"/>
<connect gate="G$1" pin="DIO5/RTS0" pad="DIO5"/>
<connect gate="G$1" pin="DIO6/TXD0" pad="DIO6"/>
<connect gate="G$1" pin="DIO7/RXD0" pad="DIO7"/>
<connect gate="G$1" pin="DIO8/TIM0GT" pad="DIO8"/>
<connect gate="G$1" pin="DIO9/TIM0_CAP" pad="DIO9"/>
<connect gate="G$1" pin="GND" pad="GND"/>
<connect gate="G$1" pin="RESETN" pad="RESETN"/>
<connect gate="G$1" pin="SPICLK" pad="SPICLK"/>
<connect gate="G$1" pin="SPIMISO" pad="SPIMISO"/>
<connect gate="G$1" pin="SPIMOSI" pad="SPIMOSI"/>
<connect gate="G$1" pin="SPISSM" pad="SPISSM"/>
<connect gate="G$1" pin="SPISSZ" pad="SPISSZ"/>
<connect gate="G$1" pin="SPISWP" pad="SPISWP"/>
<connect gate="G$1" pin="VDD" pad="VDD"/>
<connect gate="G$1" pin="VSSA" pad="VSSA"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="frames">
<packages>
</packages>
<symbols>
<symbol name="DINA4_L">
<frame x1="0" y1="0" x2="264.16" y2="180.34" columns="4" rows="4" layer="94" border-left="no" border-top="no" border-right="no" border-bottom="no"/>
</symbol>
<symbol name="DOCFIELD">
<wire x1="0" y1="0" x2="71.12" y2="0" width="0.1016" layer="94"/>
<wire x1="101.6" y1="15.24" x2="87.63" y2="15.24" width="0.1016" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="5.08" width="0.1016" layer="94"/>
<wire x1="0" y1="5.08" x2="71.12" y2="5.08" width="0.1016" layer="94"/>
<wire x1="0" y1="5.08" x2="0" y2="15.24" width="0.1016" layer="94"/>
<wire x1="101.6" y1="15.24" x2="101.6" y2="5.08" width="0.1016" layer="94"/>
<wire x1="71.12" y1="5.08" x2="71.12" y2="0" width="0.1016" layer="94"/>
<wire x1="71.12" y1="5.08" x2="87.63" y2="5.08" width="0.1016" layer="94"/>
<wire x1="71.12" y1="0" x2="101.6" y2="0" width="0.1016" layer="94"/>
<wire x1="87.63" y1="15.24" x2="87.63" y2="5.08" width="0.1016" layer="94"/>
<wire x1="87.63" y1="15.24" x2="0" y2="15.24" width="0.1016" layer="94"/>
<wire x1="87.63" y1="5.08" x2="101.6" y2="5.08" width="0.1016" layer="94"/>
<wire x1="101.6" y1="5.08" x2="101.6" y2="0" width="0.1016" layer="94"/>
<wire x1="0" y1="15.24" x2="0" y2="22.86" width="0.1016" layer="94"/>
<wire x1="101.6" y1="35.56" x2="0" y2="35.56" width="0.1016" layer="94"/>
<wire x1="101.6" y1="35.56" x2="101.6" y2="22.86" width="0.1016" layer="94"/>
<wire x1="0" y1="22.86" x2="101.6" y2="22.86" width="0.1016" layer="94"/>
<wire x1="0" y1="22.86" x2="0" y2="35.56" width="0.1016" layer="94"/>
<wire x1="101.6" y1="22.86" x2="101.6" y2="15.24" width="0.1016" layer="94"/>
<text x="1.27" y="1.27" size="2.54" layer="94" font="vector">Date:</text>
<text x="12.7" y="1.27" size="2.54" layer="94" font="vector">&gt;LAST_DATE_TIME</text>
<text x="72.39" y="1.27" size="2.54" layer="94" font="vector">Sheet:</text>
<text x="86.36" y="1.27" size="2.54" layer="94" font="vector">&gt;SHEET</text>
<text x="88.9" y="11.43" size="2.54" layer="94" font="vector">REV:</text>
<text x="1.27" y="19.05" size="2.54" layer="94" font="vector">TITLE:</text>
<text x="1.27" y="11.43" size="2.54" layer="94" font="vector">Document Number:</text>
<text x="17.78" y="19.05" size="2.54" layer="94" font="vector">&gt;DRAWING_NAME</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="DINA4_L" prefix="FRAME" uservalue="yes">
<description>&lt;b&gt;FRAME&lt;/b&gt;&lt;p&gt;
DIN A4, landscape with extra doc field</description>
<gates>
<gate name="G$1" symbol="DINA4_L" x="0" y="0"/>
<gate name="G$2" symbol="DOCFIELD" x="162.56" y="0" addlevel="must"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="pinhead">
<description>&lt;b&gt;Pin Header Connectors&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="2X08">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="-10.16" y1="-1.905" x2="-9.525" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-8.255" y1="-2.54" x2="-7.62" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="-1.905" x2="-6.985" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-5.715" y1="-2.54" x2="-5.08" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-1.905" x2="-4.445" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="-2.54" x2="-2.54" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-1.905" x2="-1.905" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="-2.54" x2="0" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="0" y1="-1.905" x2="0.635" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="1.905" y1="-2.54" x2="2.54" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="3.175" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="4.445" y1="-2.54" x2="5.08" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="-1.905" x2="-10.16" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="1.905" x2="-9.525" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-9.525" y1="2.54" x2="-8.255" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-8.255" y1="2.54" x2="-7.62" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-6.985" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-6.985" y1="2.54" x2="-5.715" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-5.715" y1="2.54" x2="-5.08" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="1.905" x2="-4.445" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-4.445" y1="2.54" x2="-3.175" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="2.54" x2="-2.54" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="1.905" x2="-1.905" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="2.54" x2="-0.635" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="2.54" x2="0" y2="1.905" width="0.1524" layer="21"/>
<wire x1="0" y1="1.905" x2="0.635" y2="2.54" width="0.1524" layer="21"/>
<wire x1="0.635" y1="2.54" x2="1.905" y2="2.54" width="0.1524" layer="21"/>
<wire x1="1.905" y1="2.54" x2="2.54" y2="1.905" width="0.1524" layer="21"/>
<wire x1="2.54" y1="1.905" x2="3.175" y2="2.54" width="0.1524" layer="21"/>
<wire x1="3.175" y1="2.54" x2="4.445" y2="2.54" width="0.1524" layer="21"/>
<wire x1="4.445" y1="2.54" x2="5.08" y2="1.905" width="0.1524" layer="21"/>
<wire x1="5.08" y1="1.905" x2="5.715" y2="2.54" width="0.1524" layer="21"/>
<wire x1="5.715" y1="2.54" x2="6.985" y2="2.54" width="0.1524" layer="21"/>
<wire x1="6.985" y1="2.54" x2="7.62" y2="1.905" width="0.1524" layer="21"/>
<wire x1="7.62" y1="-1.905" x2="6.985" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="5.08" y1="-1.905" x2="5.715" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="1.905" x2="-5.08" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="1.905" x2="-2.54" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="0" y1="1.905" x2="0" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="2.54" y1="1.905" x2="2.54" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="5.08" y1="1.905" x2="5.08" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="7.62" y1="1.905" x2="7.62" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="5.715" y1="-2.54" x2="6.985" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-2.54" x2="4.445" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="0.635" y1="-2.54" x2="1.905" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="-2.54" x2="-0.635" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-4.445" y1="-2.54" x2="-3.175" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-6.985" y1="-2.54" x2="-5.715" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-9.525" y1="-2.54" x2="-8.255" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="7.62" y1="1.905" x2="8.255" y2="2.54" width="0.1524" layer="21"/>
<wire x1="8.255" y1="2.54" x2="9.525" y2="2.54" width="0.1524" layer="21"/>
<wire x1="9.525" y1="2.54" x2="10.16" y2="1.905" width="0.1524" layer="21"/>
<wire x1="10.16" y1="-1.905" x2="9.525" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="7.62" y1="-1.905" x2="8.255" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="10.16" y1="1.905" x2="10.16" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="8.255" y1="-2.54" x2="9.525" y2="-2.54" width="0.1524" layer="21"/>
<pad name="1" x="-8.89" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="2" x="-8.89" y="1.27" drill="1.016" shape="octagon"/>
<pad name="3" x="-6.35" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="4" x="-6.35" y="1.27" drill="1.016" shape="octagon"/>
<pad name="5" x="-3.81" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="6" x="-3.81" y="1.27" drill="1.016" shape="octagon"/>
<pad name="7" x="-1.27" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="8" x="-1.27" y="1.27" drill="1.016" shape="octagon"/>
<pad name="9" x="1.27" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="10" x="1.27" y="1.27" drill="1.016" shape="octagon"/>
<pad name="11" x="3.81" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="12" x="3.81" y="1.27" drill="1.016" shape="octagon"/>
<pad name="13" x="6.35" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="14" x="6.35" y="1.27" drill="1.016" shape="octagon"/>
<pad name="15" x="8.89" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="16" x="8.89" y="1.27" drill="1.016" shape="octagon"/>
<text x="-10.16" y="3.175" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-10.16" y="-4.445" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-9.144" y1="-1.524" x2="-8.636" y2="-1.016" layer="51"/>
<rectangle x1="-9.144" y1="1.016" x2="-8.636" y2="1.524" layer="51"/>
<rectangle x1="-6.604" y1="1.016" x2="-6.096" y2="1.524" layer="51"/>
<rectangle x1="-6.604" y1="-1.524" x2="-6.096" y2="-1.016" layer="51"/>
<rectangle x1="-4.064" y1="1.016" x2="-3.556" y2="1.524" layer="51"/>
<rectangle x1="-4.064" y1="-1.524" x2="-3.556" y2="-1.016" layer="51"/>
<rectangle x1="-1.524" y1="1.016" x2="-1.016" y2="1.524" layer="51"/>
<rectangle x1="1.016" y1="1.016" x2="1.524" y2="1.524" layer="51"/>
<rectangle x1="3.556" y1="1.016" x2="4.064" y2="1.524" layer="51"/>
<rectangle x1="-1.524" y1="-1.524" x2="-1.016" y2="-1.016" layer="51"/>
<rectangle x1="1.016" y1="-1.524" x2="1.524" y2="-1.016" layer="51"/>
<rectangle x1="3.556" y1="-1.524" x2="4.064" y2="-1.016" layer="51"/>
<rectangle x1="6.096" y1="1.016" x2="6.604" y2="1.524" layer="51"/>
<rectangle x1="6.096" y1="-1.524" x2="6.604" y2="-1.016" layer="51"/>
<rectangle x1="8.636" y1="1.016" x2="9.144" y2="1.524" layer="51"/>
<rectangle x1="8.636" y1="-1.524" x2="9.144" y2="-1.016" layer="51"/>
</package>
<package name="2X08/90">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="-10.16" y1="-1.905" x2="-7.62" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="-1.905" x2="-7.62" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="0.635" x2="-10.16" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="0.635" x2="-10.16" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-8.89" y1="6.985" x2="-8.89" y2="1.27" width="0.762" layer="21"/>
<wire x1="-7.62" y1="-1.905" x2="-5.08" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-1.905" x2="-5.08" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0.635" x2="-7.62" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-6.35" y1="6.985" x2="-6.35" y2="1.27" width="0.762" layer="21"/>
<wire x1="-5.08" y1="-1.905" x2="-2.54" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-1.905" x2="-2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="0.635" x2="-5.08" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="6.985" x2="-3.81" y2="1.27" width="0.762" layer="21"/>
<wire x1="-2.54" y1="-1.905" x2="0" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="0" y1="-1.905" x2="0" y2="0.635" width="0.1524" layer="21"/>
<wire x1="0" y1="0.635" x2="-2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="6.985" x2="-1.27" y2="1.27" width="0.762" layer="21"/>
<wire x1="0" y1="-1.905" x2="2.54" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="2.54" y1="0.635" x2="0" y2="0.635" width="0.1524" layer="21"/>
<wire x1="1.27" y1="6.985" x2="1.27" y2="1.27" width="0.762" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="5.08" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="5.08" y1="-1.905" x2="5.08" y2="0.635" width="0.1524" layer="21"/>
<wire x1="5.08" y1="0.635" x2="2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="3.81" y1="6.985" x2="3.81" y2="1.27" width="0.762" layer="21"/>
<wire x1="5.08" y1="-1.905" x2="7.62" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="7.62" y1="-1.905" x2="7.62" y2="0.635" width="0.1524" layer="21"/>
<wire x1="7.62" y1="0.635" x2="5.08" y2="0.635" width="0.1524" layer="21"/>
<wire x1="6.35" y1="6.985" x2="6.35" y2="1.27" width="0.762" layer="21"/>
<wire x1="7.62" y1="-1.905" x2="10.16" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="10.16" y1="-1.905" x2="10.16" y2="0.635" width="0.1524" layer="21"/>
<wire x1="10.16" y1="0.635" x2="7.62" y2="0.635" width="0.1524" layer="21"/>
<wire x1="8.89" y1="6.985" x2="8.89" y2="1.27" width="0.762" layer="21"/>
<pad name="2" x="-8.89" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="4" x="-6.35" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="6" x="-3.81" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="8" x="-1.27" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="10" x="1.27" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="12" x="3.81" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="14" x="6.35" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="16" x="8.89" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="1" x="-8.89" y="-6.35" drill="1.016" shape="octagon"/>
<pad name="3" x="-6.35" y="-6.35" drill="1.016" shape="octagon"/>
<pad name="5" x="-3.81" y="-6.35" drill="1.016" shape="octagon"/>
<pad name="7" x="-1.27" y="-6.35" drill="1.016" shape="octagon"/>
<pad name="9" x="1.27" y="-6.35" drill="1.016" shape="octagon"/>
<pad name="11" x="3.81" y="-6.35" drill="1.016" shape="octagon"/>
<pad name="13" x="6.35" y="-6.35" drill="1.016" shape="octagon"/>
<pad name="15" x="8.89" y="-6.35" drill="1.016" shape="octagon"/>
<text x="-10.795" y="-3.81" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="12.065" y="-3.81" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-9.271" y1="0.635" x2="-8.509" y2="1.143" layer="21"/>
<rectangle x1="-6.731" y1="0.635" x2="-5.969" y2="1.143" layer="21"/>
<rectangle x1="-4.191" y1="0.635" x2="-3.429" y2="1.143" layer="21"/>
<rectangle x1="-1.651" y1="0.635" x2="-0.889" y2="1.143" layer="21"/>
<rectangle x1="0.889" y1="0.635" x2="1.651" y2="1.143" layer="21"/>
<rectangle x1="3.429" y1="0.635" x2="4.191" y2="1.143" layer="21"/>
<rectangle x1="5.969" y1="0.635" x2="6.731" y2="1.143" layer="21"/>
<rectangle x1="8.509" y1="0.635" x2="9.271" y2="1.143" layer="21"/>
<rectangle x1="-9.271" y1="-2.921" x2="-8.509" y2="-1.905" layer="21"/>
<rectangle x1="-6.731" y1="-2.921" x2="-5.969" y2="-1.905" layer="21"/>
<rectangle x1="-9.271" y1="-5.461" x2="-8.509" y2="-4.699" layer="21"/>
<rectangle x1="-9.271" y1="-4.699" x2="-8.509" y2="-2.921" layer="51"/>
<rectangle x1="-6.731" y1="-4.699" x2="-5.969" y2="-2.921" layer="51"/>
<rectangle x1="-6.731" y1="-5.461" x2="-5.969" y2="-4.699" layer="21"/>
<rectangle x1="-4.191" y1="-2.921" x2="-3.429" y2="-1.905" layer="21"/>
<rectangle x1="-1.651" y1="-2.921" x2="-0.889" y2="-1.905" layer="21"/>
<rectangle x1="-4.191" y1="-5.461" x2="-3.429" y2="-4.699" layer="21"/>
<rectangle x1="-4.191" y1="-4.699" x2="-3.429" y2="-2.921" layer="51"/>
<rectangle x1="-1.651" y1="-4.699" x2="-0.889" y2="-2.921" layer="51"/>
<rectangle x1="-1.651" y1="-5.461" x2="-0.889" y2="-4.699" layer="21"/>
<rectangle x1="0.889" y1="-2.921" x2="1.651" y2="-1.905" layer="21"/>
<rectangle x1="3.429" y1="-2.921" x2="4.191" y2="-1.905" layer="21"/>
<rectangle x1="0.889" y1="-5.461" x2="1.651" y2="-4.699" layer="21"/>
<rectangle x1="0.889" y1="-4.699" x2="1.651" y2="-2.921" layer="51"/>
<rectangle x1="3.429" y1="-4.699" x2="4.191" y2="-2.921" layer="51"/>
<rectangle x1="3.429" y1="-5.461" x2="4.191" y2="-4.699" layer="21"/>
<rectangle x1="5.969" y1="-2.921" x2="6.731" y2="-1.905" layer="21"/>
<rectangle x1="8.509" y1="-2.921" x2="9.271" y2="-1.905" layer="21"/>
<rectangle x1="5.969" y1="-5.461" x2="6.731" y2="-4.699" layer="21"/>
<rectangle x1="5.969" y1="-4.699" x2="6.731" y2="-2.921" layer="51"/>
<rectangle x1="8.509" y1="-4.699" x2="9.271" y2="-2.921" layer="51"/>
<rectangle x1="8.509" y1="-5.461" x2="9.271" y2="-4.699" layer="21"/>
</package>
<package name="1X01">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="1.27" y1="0.635" x2="1.27" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="-1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.1524" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" shape="octagon"/>
<text x="-1.3462" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="PINH2X8">
<wire x1="-6.35" y1="-12.7" x2="8.89" y2="-12.7" width="0.4064" layer="94"/>
<wire x1="8.89" y1="-12.7" x2="8.89" y2="10.16" width="0.4064" layer="94"/>
<wire x1="8.89" y1="10.16" x2="-6.35" y2="10.16" width="0.4064" layer="94"/>
<wire x1="-6.35" y1="10.16" x2="-6.35" y2="-12.7" width="0.4064" layer="94"/>
<text x="-6.35" y="10.795" size="1.778" layer="95">&gt;NAME</text>
<text x="-6.35" y="-15.24" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-2.54" y="7.62" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="2" x="5.08" y="7.62" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="3" x="-2.54" y="5.08" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="4" x="5.08" y="5.08" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="5" x="-2.54" y="2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="6" x="5.08" y="2.54" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="7" x="-2.54" y="0" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="8" x="5.08" y="0" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="9" x="-2.54" y="-2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="10" x="5.08" y="-2.54" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="11" x="-2.54" y="-5.08" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="12" x="5.08" y="-5.08" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="13" x="-2.54" y="-7.62" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="14" x="5.08" y="-7.62" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="15" x="-2.54" y="-10.16" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="16" x="5.08" y="-10.16" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
</symbol>
<symbol name="PINHD1">
<wire x1="-6.35" y1="-2.54" x2="1.27" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="1.27" y2="2.54" width="0.4064" layer="94"/>
<wire x1="1.27" y1="2.54" x2="-6.35" y2="2.54" width="0.4064" layer="94"/>
<wire x1="-6.35" y1="2.54" x2="-6.35" y2="-2.54" width="0.4064" layer="94"/>
<text x="-6.35" y="3.175" size="1.778" layer="95">&gt;NAME</text>
<text x="-6.35" y="-5.08" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-2.54" y="0" visible="pad" length="short" direction="pas" function="dot"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="PINHD-2X8" prefix="JP" uservalue="yes">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<gates>
<gate name="A" symbol="PINH2X8" x="0" y="0"/>
</gates>
<devices>
<device name="" package="2X08">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="10" pad="10"/>
<connect gate="A" pin="11" pad="11"/>
<connect gate="A" pin="12" pad="12"/>
<connect gate="A" pin="13" pad="13"/>
<connect gate="A" pin="14" pad="14"/>
<connect gate="A" pin="15" pad="15"/>
<connect gate="A" pin="16" pad="16"/>
<connect gate="A" pin="2" pad="2"/>
<connect gate="A" pin="3" pad="3"/>
<connect gate="A" pin="4" pad="4"/>
<connect gate="A" pin="5" pad="5"/>
<connect gate="A" pin="6" pad="6"/>
<connect gate="A" pin="7" pad="7"/>
<connect gate="A" pin="8" pad="8"/>
<connect gate="A" pin="9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="/90" package="2X08/90">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="10" pad="10"/>
<connect gate="A" pin="11" pad="11"/>
<connect gate="A" pin="12" pad="12"/>
<connect gate="A" pin="13" pad="13"/>
<connect gate="A" pin="14" pad="14"/>
<connect gate="A" pin="15" pad="15"/>
<connect gate="A" pin="16" pad="16"/>
<connect gate="A" pin="2" pad="2"/>
<connect gate="A" pin="3" pad="3"/>
<connect gate="A" pin="4" pad="4"/>
<connect gate="A" pin="5" pad="5"/>
<connect gate="A" pin="6" pad="6"/>
<connect gate="A" pin="7" pad="7"/>
<connect gate="A" pin="8" pad="8"/>
<connect gate="A" pin="9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="PINHD-1X1" prefix="JP" uservalue="yes">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="PINHD1" x="0" y="0"/>
</gates>
<devices>
<device name="" package="1X01">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="U$1" library="jennic" deviceset="JN5139-Z01-M00R1" device="MODULE"/>
<part name="FRAME1" library="frames" deviceset="DINA4_L" device=""/>
<part name="JP1" library="pinhead" deviceset="PINHD-2X8" device=""/>
<part name="JP2" library="pinhead" deviceset="PINHD-1X1" device=""/>
<part name="JP3" library="pinhead" deviceset="PINHD-1X1" device=""/>
<part name="JP4" library="pinhead" deviceset="PINHD-1X1" device=""/>
<part name="JP5" library="pinhead" deviceset="PINHD-1X1" device=""/>
<part name="JP6" library="pinhead" deviceset="PINHD-1X1" device=""/>
<part name="JP7" library="pinhead" deviceset="PINHD-1X1" device=""/>
<part name="JP8" library="pinhead" deviceset="PINHD-1X1" device=""/>
<part name="JP9" library="pinhead" deviceset="PINHD-1X1" device=""/>
<part name="JP10" library="pinhead" deviceset="PINHD-1X1" device=""/>
<part name="JP11" library="pinhead" deviceset="PINHD-1X1" device=""/>
<part name="JP12" library="pinhead" deviceset="PINHD-1X1" device=""/>
<part name="JP13" library="pinhead" deviceset="PINHD-2X8" device=""/>
</parts>
<sheets>
<sheet>
<plain>
</plain>
<instances>
<instance part="U$1" gate="G$1" x="76.2" y="91.44"/>
<instance part="FRAME1" gate="G$1" x="0" y="0"/>
<instance part="FRAME1" gate="G$2" x="162.56" y="0"/>
<instance part="JP1" gate="A" x="172.72" y="142.24"/>
<instance part="JP2" gate="G$1" x="93.98" y="83.82" rot="R90"/>
<instance part="JP3" gate="G$1" x="96.52" y="83.82" rot="R90"/>
<instance part="JP4" gate="G$1" x="99.06" y="83.82" rot="R90"/>
<instance part="JP5" gate="G$1" x="101.6" y="83.82" rot="R90"/>
<instance part="JP6" gate="G$1" x="104.14" y="83.82" rot="R90"/>
<instance part="JP7" gate="G$1" x="106.68" y="83.82" rot="R90"/>
<instance part="JP8" gate="G$1" x="109.22" y="83.82" rot="R90"/>
<instance part="JP9" gate="G$1" x="111.76" y="83.82" rot="R90"/>
<instance part="JP10" gate="G$1" x="114.3" y="83.82" rot="R90"/>
<instance part="JP11" gate="G$1" x="116.84" y="83.82" rot="R90"/>
<instance part="JP12" gate="G$1" x="119.38" y="83.82" rot="R90"/>
<instance part="JP13" gate="A" x="172.72" y="101.6" rot="MR0"/>
</instances>
<busses>
</busses>
<nets>
<net name="SPIMISO" class="0">
<segment>
<wire x1="93.98" y1="124.46" x2="124.46" y2="124.46" width="0.1524" layer="91"/>
<label x="119.38" y="124.46" size="1.778" layer="95"/>
<pinref part="U$1" gate="G$1" pin="SPIMISO"/>
<wire x1="157.48" y1="142.24" x2="170.18" y2="142.24" width="0.1524" layer="91"/>
<pinref part="JP1" gate="A" pin="7"/>
</segment>
</net>
<net name="RESETN" class="0">
<segment>
<wire x1="66.04" y1="88.9" x2="66.04" y2="81.28" width="0.1524" layer="91"/>
<pinref part="U$1" gate="G$1" pin="RESETN"/>
<wire x1="137.16" y1="93.98" x2="167.64" y2="93.98" width="0.1524" layer="91"/>
<pinref part="JP13" gate="A" pin="14"/>
</segment>
</net>
<net name="DIO9" class="0">
<segment>
<wire x1="48.26" y1="132.08" x2="17.78" y2="132.08" width="0.1524" layer="91"/>
<label x="17.78" y="132.08" size="1.778" layer="95"/>
<pinref part="U$1" gate="G$1" pin="DIO9/TIM0_CAP"/>
<wire x1="106.68" y1="81.28" x2="106.68" y2="50.8" width="0.1524" layer="91"/>
<pinref part="JP7" gate="G$1" pin="1"/>
</segment>
</net>
<net name="DIO10" class="0">
<segment>
<wire x1="48.26" y1="134.62" x2="17.78" y2="134.62" width="0.1524" layer="91"/>
<label x="17.78" y="134.62" size="1.778" layer="95"/>
<pinref part="U$1" gate="G$1" pin="DIO10/TIM0_OUT"/>
<wire x1="109.22" y1="81.28" x2="109.22" y2="50.8" width="0.1524" layer="91"/>
<pinref part="JP8" gate="G$1" pin="1"/>
</segment>
</net>
<net name="DIO12" class="0">
<segment>
<wire x1="48.26" y1="139.7" x2="17.78" y2="139.7" width="0.1524" layer="91"/>
<label x="17.78" y="139.7" size="1.778" layer="95"/>
<pinref part="U$1" gate="G$1" pin="DIO12/TIM1_CAP"/>
<wire x1="167.64" y1="91.44" x2="137.16" y2="91.44" width="0.1524" layer="91"/>
<pinref part="JP13" gate="A" pin="16"/>
</segment>
</net>
<net name="DIO13" class="0">
<segment>
<wire x1="48.26" y1="142.24" x2="17.78" y2="142.24" width="0.1524" layer="91"/>
<label x="17.78" y="142.24" size="1.778" layer="95"/>
<pinref part="U$1" gate="G$1" pin="DIO13/TIM1_OUT"/>
<wire x1="205.74" y1="93.98" x2="175.26" y2="93.98" width="0.1524" layer="91"/>
<pinref part="JP13" gate="A" pin="13"/>
</segment>
</net>
<net name="DIO11" class="0">
<segment>
<wire x1="48.26" y1="137.16" x2="17.78" y2="137.16" width="0.1524" layer="91"/>
<label x="17.78" y="137.16" size="1.778" layer="95"/>
<pinref part="U$1" gate="G$1" pin="DIO11/TIM1GT"/>
<wire x1="111.76" y1="81.28" x2="111.76" y2="50.8" width="0.1524" layer="91"/>
<pinref part="JP9" gate="G$1" pin="1"/>
</segment>
</net>
<net name="DIO14" class="0">
<segment>
<wire x1="48.26" y1="147.32" x2="17.78" y2="147.32" width="0.1524" layer="91"/>
<label x="17.78" y="147.32" size="1.778" layer="95"/>
<pinref part="U$1" gate="G$1" pin="DIO14/SIF_CLK"/>
<wire x1="205.74" y1="96.52" x2="175.26" y2="96.52" width="0.1524" layer="91"/>
<pinref part="JP13" gate="A" pin="11"/>
</segment>
</net>
<net name="DIO15" class="0">
<segment>
<wire x1="48.26" y1="149.86" x2="17.78" y2="149.86" width="0.1524" layer="91"/>
<label x="17.78" y="149.86" size="1.778" layer="95"/>
<pinref part="U$1" gate="G$1" pin="DIO15/SIF_D"/>
<wire x1="167.64" y1="96.52" x2="137.16" y2="96.52" width="0.1524" layer="91"/>
<pinref part="JP13" gate="A" pin="12"/>
</segment>
</net>
<net name="DIO16" class="0">
<segment>
<wire x1="93.98" y1="149.86" x2="124.46" y2="149.86" width="0.1524" layer="91"/>
<label x="119.38" y="149.86" size="1.778" layer="95"/>
<pinref part="U$1" gate="G$1" pin="DIO16"/>
<wire x1="175.26" y1="99.06" x2="205.74" y2="99.06" width="0.1524" layer="91"/>
<pinref part="JP13" gate="A" pin="9"/>
</segment>
</net>
<net name="DIO17" class="0">
<segment>
<wire x1="93.98" y1="109.22" x2="124.46" y2="109.22" width="0.1524" layer="91"/>
<label x="119.38" y="109.22" size="1.778" layer="95"/>
<pinref part="U$1" gate="G$1" pin="DIO17/CTS1"/>
<wire x1="137.16" y1="99.06" x2="167.64" y2="99.06" width="0.1524" layer="91"/>
<pinref part="JP13" gate="A" pin="10"/>
</segment>
</net>
<net name="DIO18" class="0">
<segment>
<wire x1="93.98" y1="111.76" x2="124.46" y2="111.76" width="0.1524" layer="91"/>
<label x="119.38" y="111.76" size="1.778" layer="95"/>
<pinref part="U$1" gate="G$1" pin="DIO18/RTS1"/>
<wire x1="175.26" y1="101.6" x2="205.74" y2="101.6" width="0.1524" layer="91"/>
<pinref part="JP13" gate="A" pin="7"/>
</segment>
</net>
<net name="DIO19" class="0">
<segment>
<wire x1="93.98" y1="114.3" x2="124.46" y2="114.3" width="0.1524" layer="91"/>
<label x="119.38" y="114.3" size="1.778" layer="95"/>
<pinref part="U$1" gate="G$1" pin="DIO19/TXD1"/>
<wire x1="137.16" y1="101.6" x2="167.64" y2="101.6" width="0.1524" layer="91"/>
<pinref part="JP13" gate="A" pin="8"/>
</segment>
</net>
<net name="DIO20" class="0">
<segment>
<wire x1="93.98" y1="116.84" x2="124.46" y2="116.84" width="0.1524" layer="91"/>
<label x="119.38" y="116.84" size="1.778" layer="95"/>
<pinref part="U$1" gate="G$1" pin="DIO20/RXD1"/>
<wire x1="175.26" y1="104.14" x2="205.74" y2="104.14" width="0.1524" layer="91"/>
<pinref part="JP13" gate="A" pin="5"/>
</segment>
</net>
<net name="COMP1-" class="0">
<segment>
<wire x1="48.26" y1="96.52" x2="17.78" y2="96.52" width="0.1524" layer="91"/>
<label x="17.78" y="96.52" size="1.778" layer="95"/>
<pinref part="U$1" gate="G$1" pin="COMP1-"/>
<wire x1="167.64" y1="104.14" x2="137.16" y2="104.14" width="0.1524" layer="91"/>
<pinref part="JP13" gate="A" pin="6"/>
</segment>
</net>
<net name="COMP1+" class="0">
<segment>
<wire x1="48.26" y1="99.06" x2="17.78" y2="99.06" width="0.1524" layer="91"/>
<label x="17.78" y="99.06" size="1.778" layer="95"/>
<pinref part="U$1" gate="G$1" pin="COMP1+"/>
<wire x1="205.74" y1="106.68" x2="175.26" y2="106.68" width="0.1524" layer="91"/>
<pinref part="JP13" gate="A" pin="3"/>
</segment>
</net>
<net name="ADC1" class="0">
<segment>
<wire x1="48.26" y1="116.84" x2="17.78" y2="116.84" width="0.1524" layer="91"/>
<label x="17.78" y="116.84" size="1.778" layer="95"/>
<pinref part="U$1" gate="G$1" pin="ADC1"/>
<wire x1="167.64" y1="106.68" x2="137.16" y2="106.68" width="0.1524" layer="91"/>
<pinref part="JP13" gate="A" pin="4"/>
</segment>
</net>
<net name="ADC2" class="0">
<segment>
<wire x1="48.26" y1="119.38" x2="17.78" y2="119.38" width="0.1524" layer="91"/>
<label x="17.78" y="119.38" size="1.778" layer="95"/>
<pinref part="U$1" gate="G$1" pin="ADC2"/>
<wire x1="205.74" y1="109.22" x2="175.26" y2="109.22" width="0.1524" layer="91"/>
<pinref part="JP13" gate="A" pin="1"/>
</segment>
</net>
<net name="ADC3" class="0">
<segment>
<wire x1="48.26" y1="121.92" x2="17.78" y2="121.92" width="0.1524" layer="91"/>
<label x="17.78" y="121.92" size="1.778" layer="95"/>
<pinref part="U$1" gate="G$1" pin="ADC3"/>
<wire x1="167.64" y1="109.22" x2="137.16" y2="109.22" width="0.1524" layer="91"/>
<pinref part="JP13" gate="A" pin="2"/>
</segment>
</net>
<net name="DIO4" class="0">
<segment>
<wire x1="93.98" y1="96.52" x2="124.46" y2="96.52" width="0.1524" layer="91"/>
<label x="119.38" y="96.52" size="1.778" layer="95"/>
<pinref part="U$1" gate="G$1" pin="DIO4/CTS0"/>
<wire x1="93.98" y1="50.8" x2="93.98" y2="81.28" width="0.1524" layer="91"/>
<pinref part="JP2" gate="G$1" pin="1"/>
</segment>
</net>
<net name="SPISWP" class="0">
<segment>
<wire x1="93.98" y1="134.62" x2="124.46" y2="134.62" width="0.1524" layer="91"/>
<label x="119.38" y="134.62" size="1.778" layer="95"/>
<pinref part="U$1" gate="G$1" pin="SPISWP"/>
<wire x1="177.8" y1="134.62" x2="190.5" y2="134.62" width="0.1524" layer="91"/>
<pinref part="JP1" gate="A" pin="14"/>
</segment>
</net>
<net name="SPISSM" class="0">
<segment>
<wire x1="93.98" y1="132.08" x2="124.46" y2="132.08" width="0.1524" layer="91"/>
<label x="119.38" y="132.08" size="1.778" layer="95"/>
<pinref part="U$1" gate="G$1" pin="SPISSM"/>
<wire x1="157.48" y1="134.62" x2="170.18" y2="134.62" width="0.1524" layer="91"/>
<pinref part="JP1" gate="A" pin="13"/>
</segment>
</net>
<net name="ADC4" class="0">
<segment>
<wire x1="48.26" y1="124.46" x2="17.78" y2="124.46" width="0.1524" layer="91"/>
<label x="17.78" y="124.46" size="1.778" layer="95"/>
<pinref part="U$1" gate="G$1" pin="ADC4"/>
<wire x1="170.18" y1="149.86" x2="157.48" y2="149.86" width="0.1524" layer="91"/>
<pinref part="JP1" gate="A" pin="1"/>
</segment>
</net>
<net name="DAC1" class="0">
<segment>
<wire x1="48.26" y1="109.22" x2="17.78" y2="109.22" width="0.1524" layer="91"/>
<label x="17.78" y="109.22" size="1.778" layer="95"/>
<pinref part="U$1" gate="G$1" pin="DAC1"/>
<wire x1="190.5" y1="149.86" x2="177.8" y2="149.86" width="0.1524" layer="91"/>
<pinref part="JP1" gate="A" pin="2"/>
</segment>
</net>
<net name="DAC2" class="0">
<segment>
<wire x1="48.26" y1="111.76" x2="17.78" y2="111.76" width="0.1524" layer="91"/>
<label x="17.78" y="111.76" size="1.778" layer="95"/>
<pinref part="U$1" gate="G$1" pin="DAC2"/>
<wire x1="170.18" y1="147.32" x2="157.48" y2="147.32" width="0.1524" layer="91"/>
<pinref part="JP1" gate="A" pin="3"/>
</segment>
</net>
<net name="COMP2+" class="0">
<segment>
<wire x1="48.26" y1="104.14" x2="17.78" y2="104.14" width="0.1524" layer="91"/>
<label x="17.78" y="104.14" size="1.778" layer="95"/>
<pinref part="U$1" gate="G$1" pin="COMP2+"/>
<wire x1="190.5" y1="147.32" x2="177.8" y2="147.32" width="0.1524" layer="91"/>
<pinref part="JP1" gate="A" pin="4"/>
</segment>
</net>
<net name="COMP2-" class="0">
<segment>
<wire x1="48.26" y1="101.6" x2="17.78" y2="101.6" width="0.1524" layer="91"/>
<label x="17.78" y="101.6" size="1.778" layer="95"/>
<pinref part="U$1" gate="G$1" pin="COMP2-"/>
<wire x1="170.18" y1="144.78" x2="157.48" y2="144.78" width="0.1524" layer="91"/>
<pinref part="JP1" gate="A" pin="5"/>
</segment>
</net>
<net name="DIO2" class="0">
<segment>
<wire x1="93.98" y1="142.24" x2="124.46" y2="142.24" width="0.1524" layer="91"/>
<label x="119.38" y="142.24" size="1.778" layer="95"/>
<pinref part="U$1" gate="G$1" pin="DIO2/SPISEL3"/>
<wire x1="177.8" y1="137.16" x2="190.5" y2="137.16" width="0.1524" layer="91"/>
<pinref part="JP1" gate="A" pin="12"/>
</segment>
</net>
<net name="DIO3" class="0">
<segment>
<wire x1="93.98" y1="144.78" x2="124.46" y2="144.78" width="0.1524" layer="91"/>
<label x="119.38" y="144.78" size="1.778" layer="95"/>
<pinref part="U$1" gate="G$1" pin="DIO3/SPISEL4"/>
<wire x1="157.48" y1="132.08" x2="170.18" y2="132.08" width="0.1524" layer="91"/>
<pinref part="JP1" gate="A" pin="15"/>
</segment>
</net>
<net name="SPISSZ" class="0">
<segment>
<wire x1="93.98" y1="129.54" x2="124.46" y2="129.54" width="0.1524" layer="91"/>
<label x="119.38" y="129.54" size="1.778" layer="95"/>
<pinref part="U$1" gate="G$1" pin="SPISSZ"/>
<wire x1="157.48" y1="139.7" x2="170.18" y2="139.7" width="0.1524" layer="91"/>
<pinref part="JP1" gate="A" pin="9"/>
</segment>
</net>
<net name="SPIMOSI" class="0">
<segment>
<wire x1="93.98" y1="127" x2="124.46" y2="127" width="0.1524" layer="91"/>
<label x="119.38" y="127" size="1.778" layer="95"/>
<pinref part="U$1" gate="G$1" pin="SPIMOSI"/>
<wire x1="177.8" y1="142.24" x2="190.5" y2="142.24" width="0.1524" layer="91"/>
<pinref part="JP1" gate="A" pin="8"/>
</segment>
</net>
<net name="SPICLK" class="0">
<segment>
<wire x1="93.98" y1="121.92" x2="124.46" y2="121.92" width="0.1524" layer="91"/>
<label x="119.38" y="121.92" size="1.778" layer="95"/>
<pinref part="U$1" gate="G$1" pin="SPICLK"/>
<wire x1="177.8" y1="144.78" x2="190.5" y2="144.78" width="0.1524" layer="91"/>
<pinref part="JP1" gate="A" pin="6"/>
</segment>
</net>
<net name="DIO0" class="0">
<segment>
<wire x1="93.98" y1="137.16" x2="124.46" y2="137.16" width="0.1524" layer="91"/>
<label x="119.38" y="137.16" size="1.778" layer="95"/>
<pinref part="U$1" gate="G$1" pin="DIO0/SPISEL1"/>
<wire x1="177.8" y1="139.7" x2="190.5" y2="139.7" width="0.1524" layer="91"/>
<pinref part="JP1" gate="A" pin="10"/>
</segment>
</net>
<net name="DIO1" class="0">
<segment>
<wire x1="93.98" y1="139.7" x2="124.46" y2="139.7" width="0.1524" layer="91"/>
<label x="119.38" y="139.7" size="1.778" layer="95"/>
<pinref part="U$1" gate="G$1" pin="DIO1/SPISEL2"/>
<wire x1="157.48" y1="137.16" x2="170.18" y2="137.16" width="0.1524" layer="91"/>
<pinref part="JP1" gate="A" pin="11"/>
</segment>
</net>
<net name="DIO7" class="0">
<segment>
<wire x1="93.98" y1="104.14" x2="124.46" y2="104.14" width="0.1524" layer="91"/>
<label x="119.38" y="104.14" size="1.778" layer="95"/>
<pinref part="U$1" gate="G$1" pin="DIO7/RXD0"/>
<wire x1="101.6" y1="50.8" x2="101.6" y2="81.28" width="0.1524" layer="91"/>
<pinref part="JP5" gate="G$1" pin="1"/>
</segment>
</net>
<net name="DIO6" class="0">
<segment>
<wire x1="93.98" y1="101.6" x2="124.46" y2="101.6" width="0.1524" layer="91"/>
<label x="119.38" y="101.6" size="1.778" layer="95"/>
<pinref part="U$1" gate="G$1" pin="DIO6/TXD0"/>
<wire x1="99.06" y1="50.8" x2="99.06" y2="81.28" width="0.1524" layer="91"/>
<pinref part="JP4" gate="G$1" pin="1"/>
</segment>
</net>
<net name="DIO5" class="0">
<segment>
<wire x1="93.98" y1="99.06" x2="124.46" y2="99.06" width="0.1524" layer="91"/>
<label x="119.38" y="99.06" size="1.778" layer="95"/>
<pinref part="U$1" gate="G$1" pin="DIO5/RTS0"/>
<wire x1="96.52" y1="50.8" x2="96.52" y2="81.28" width="0.1524" layer="91"/>
<pinref part="JP3" gate="G$1" pin="1"/>
</segment>
</net>
<net name="DIO8" class="0">
<segment>
<wire x1="48.26" y1="129.54" x2="17.78" y2="129.54" width="0.1524" layer="91"/>
<label x="17.78" y="129.54" size="1.778" layer="95"/>
<pinref part="U$1" gate="G$1" pin="DIO8/TIM0GT"/>
<wire x1="104.14" y1="81.28" x2="104.14" y2="50.8" width="0.1524" layer="91"/>
<pinref part="JP6" gate="G$1" pin="1"/>
</segment>
</net>
<net name="VSSA" class="0">
<segment>
<wire x1="73.66" y1="83.82" x2="73.66" y2="88.9" width="0.1524" layer="91"/>
<pinref part="U$1" gate="G$1" pin="VSSA"/>
<wire x1="119.38" y1="50.8" x2="119.38" y2="81.28" width="0.1524" layer="91"/>
<pinref part="JP12" gate="G$1" pin="1"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="GND"/>
<wire x1="71.12" y1="88.9" x2="71.12" y2="83.82" width="0.1524" layer="91"/>
<wire x1="116.84" y1="81.28" x2="116.84" y2="50.8" width="0.1524" layer="91"/>
<pinref part="JP11" gate="G$1" pin="1"/>
</segment>
</net>
<net name="VDD" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="VDD"/>
<wire x1="73.66" y1="160.02" x2="73.66" y2="167.64" width="0.1524" layer="91"/>
<wire x1="114.3" y1="50.8" x2="114.3" y2="81.28" width="0.1524" layer="91"/>
<pinref part="JP10" gate="G$1" pin="1"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
</eagle>
