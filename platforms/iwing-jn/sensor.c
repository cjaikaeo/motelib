#include <stdbool.h>

#include <jendefs.h>
#include <AppHardwareApi.h>

#include <motelib/sys/led.h>
#include <motelib/system.h>
#include <motelib/sensor.h>
#include <motelib/led.h>
#include <motelib/cirbuf.h>

#include "platform.h"
#include "main.h"

static SensorDataReady m_callback; ///< Callback pointer for the most recent ADC read
static SensorType m_sensor; ///< Channel no. for the most recent ADC read
static uint16_t m_value; ///< Store most recent reading
static bool m_pending;

///////////////////////////////////////////////////////////
void _sensorInit()
{
    // Release RTS/CTS pins for other uses
    vAHI_UartSetRTSCTS(E_AHI_UART_0, FALSE);

    vAHI_ApConfigure(
            E_AHI_AP_REGULATOR_ENABLE,
            E_AHI_AP_INT_ENABLE,
            E_AHI_AP_SAMPLE_4,
            E_AHI_AP_CLOCKDIV_500KHZ,
            E_AHI_AP_INTREF
            );

    // Ensure that the regulator has already started
    while (!bAHI_APRegulatorEnabled())
        ;

    m_sensor = 0xFF;
}

///////////////////////////////////////////////////////////
SensorStatus sensorRequestAnalog(SensorType sensor, SensorDataReady readDone)
{
    // Report busy if ADC is currently in used
    if (m_pending) return SENSOR_BUSY;

    uint8_t sensor_src;

    // Map sensor type to Jennic ADC source
    switch (sensor)
    {
        case SENSOR_A1:
            sensor_src = E_AHI_ADC_SRC_ADC_1;
            break;

        case SENSOR_A2:
            sensor_src = E_AHI_ADC_SRC_ADC_2;
            break;

        case SENSOR_A3:
            sensor_src = E_AHI_ADC_SRC_ADC_3;
            vAHI_DioSetPullup(0, 1<<0); // disable pull-up at DIO0
            break;

        case SENSOR_A4:
            sensor_src = E_AHI_ADC_SRC_ADC_4;
            vAHI_DioSetPullup(0, 1<<1); // disable pull-up at DIO1
            break;

        case SENSOR_TEMP:
            sensor_src = E_AHI_ADC_SRC_TEMP;
            break;

        case SENSOR_VOLT:
            sensor_src = E_AHI_ADC_SRC_VOLT;
            break;

        default:
            return SENSOR_BUSY;
    }

    m_sensor   = sensor;
    m_callback = readDone;
    m_pending  = true;

    // Configure ADC and start the conversion
    vAHI_AdcEnable(
            E_AHI_ADC_SINGLE_SHOT,
            E_AHI_AP_INPUT_RANGE_2,
            sensor_src
            );
    vAHI_AdcStartSample();

    return SENSOR_OK;
}

///////////////////////////////////////////////////////////
uint16_t sensorAnalogResult(SensorType sensor)
{
    if (sensor != m_sensor) return 0xFFFF;

    return m_value;
}

///////////////////////////////////////////////////////////
uint8_t sensorReadDigital(SensorType sensor)
{
    // Ensure sensor id is in a valid range
    if (sensor >= SENSOR_D17) return 0;

    // Make the corresponding pin input
    vAHI_DioSetDirection(1<<sensor, 0);  

    return ((1<<sensor) & u32AHI_DioReadInput()) >> sensor;
}

///////////////////////////////////////////////////////////
bool sensorAnalogWaiting(SensorType sensor)
{
    return (sensor == m_sensor) && m_pending;
}

///////////////////////////////////////////////////////////
bool sensorAnalogAvailable(SensorType sensor)
{
    return !m_pending;
}

///////////////////////////////////////////////////////////
void _sensorReadDone()
{
    m_value = u16AHI_AdcRead();
    m_pending = false;
    if (m_callback) m_callback(m_value);
}
