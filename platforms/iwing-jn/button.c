#include <jendefs.h>
#include <AppHardwareApi.h>
#include <motelib/system.h>
#include <motelib/button.h>
#include <motelib/timer.h>

#include "main.h"

#ifndef JENNIC_DEBOUNCE_DELAY
#define JENNIC_DEBOUNCE_DELAY  10  ///< debouncing delay in ms
#endif

static ButtonHandler m_buttonHandler;
static bool m_pressed;
static uint16_t m_timeLastChange;

static void setInterrupt();

void _buttonInit()
{
    vAHI_DioSetDirection(1<<BUTTON_DIO, 0);    // make button pin input
    vAHI_DioSetPullup(1<<BUTTON_DIO, 0);       // enable pull-up at button pin
    vAHI_DioInterruptEnable(1<<BUTTON_DIO, 0); // enable interrupt

    m_buttonHandler = NULL;
    m_timeLastChange = 0;
    m_pressed = buttonIsPressed();
    setInterrupt();
}

void buttonSetHandler(ButtonHandler handler)
{
    m_buttonHandler = handler;
}

bool buttonIsPressed()
{
    return !((1<<BUTTON_DIO) & u32AHI_DioReadInput());
}

static void setInterrupt()
{
    if (m_pressed)
        vAHI_DioInterruptEdge(1<<BUTTON_DIO, 0);   // watch for rising edge
    else
        vAHI_DioInterruptEdge(0, 1<<BUTTON_DIO);   // watch for falling edge
}

void _buttonHandleChange()
{
    uint16_t currentTime;

    // Do nothing if no handler is set
    if (m_buttonHandler == NULL)
        return;

    // implement software contact debouncing
    currentTime = timerTicks();
    if ((uint16_t)(currentTime - m_timeLastChange) < JENNIC_DEBOUNCE_DELAY)
        return;

    m_timeLastChange = currentTime;

    m_pressed = !m_pressed;
    setInterrupt();

    if (m_pressed)
        m_buttonHandler(BUTTON_PRESSED);
    else
        m_buttonHandler(BUTTON_RELEASED);
}
