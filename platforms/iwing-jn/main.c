#include <jendefs.h>
#include <AppHardwareApi.h>
#include <AppQueueApi.h>
#include <motelib/system.h>
#include <motelib/sys/main.h>
#include <motelib/sys/led.h>
#include <motelib/sys/timer.h>
#include <motelib/sys/radio.h>
#include "platform.h"
#include "main.h"

void _sensorInit();
void _sensorReadDone();
void _buttonInit();
void _buttonHandleChange();
void _check_radio_events();
void _uart_poll();
void _uart_rx_data_avail();

PRIVATE void processIncomingHwEvent(AppQApiHwInd_s *hwInd)
{
    switch (hwInd->u32DeviceId)
    {
        case E_AHI_DEVICE_SYSCTRL:
            if (hwInd->u32ItemBitmap & (1<<BUTTON_DIO))
                _buttonHandleChange();
            break;

        case E_AHI_DEVICE_TICK_TIMER:
            _sys_timer_tick();
            break;

        case E_AHI_DEVICE_UART0:
            // XXX should check hwInd->u32ItemBitmap to make sure
            //if (hwInd->u32ItemBitmap == E_AHI_UART_INT_RXDATA)
            _uart_rx_data_avail();
            break;

        case E_AHI_DEVICE_ANALOGUE:
            _sensorReadDone();
            break;
    }
}

PUBLIC void AppColdStart(void)
{
    AppQApiHwInd_s *hwInd;

    // Set system clock divisor to 1 => 32MHz
    bAHI_SetClockRate(3);

    // Initialize integrated peripheral API
    u32AHI_Init();

    // Turn on watchdog timer
    vAHI_WatchdogStart(12); // 16 seconds

    // Initialize App Queue
    u32AppQApiInit(NULL, NULL, NULL);

    // Initialize other modules
    _sensorInit();
    _buttonInit();

    // Call MoteLib's system intialization
    _sys_init_routine();

    // Enter the forever main loop
    for (;;)
    {
        // Call MoteLib's main routine
        _sys_main_routine();

        // Check for incoming radio events
        _check_radio_events();

        // Check for incoming hardware events
        hwInd = psAppQApiReadHwInd();
        if (hwInd != NULL)
        {
            processIncomingHwEvent(hwInd);
            vAppQApiReturnHwIndBuffer(hwInd);
        }

        _uart_poll();  // check and send data in UART output buffer

        // Reset the watchdog timer
        vAHI_WatchdogRestart();
    }
}

PUBLIC void AppWarmStart(void)
{
    AppColdStart();
}
