# vim:syntax=make

###############################
# Prepare essential variables #
###############################
SDK_BASE_DIR ?= /opt/jennic
JENNIC_CHIP  ?= JN5168
JENNIC_STACK ?= MAC
DEBUG_PORT   ?= UART0
JENNIC_SERIAL_DEV ?= /dev/ttyUSB0

include $(SDK_BASE_DIR)/Chip/Common/Build/config.mk
include $(SDK_BASE_DIR)/Platform/Common/Build/config.mk
include $(SDK_BASE_DIR)/Stack/Common/Build/config.mk

PLATFORM_OBJS := AppQueueApi.o main.o timer.o led.o radio.o uart.o button.o sensor.o actor.o
LDLIBS := $(addsuffix _$(JENNIC_CHIP_FAMILY),$(APPLIBS)) $(LDLIBS)
LIBS   += sys/debug.o
CFLAGS += $(DEFAULT_VAR) --std=c99 $(INCFLAGS) -I$(COMPONENTS_BASE_DIR)/AppQueueApi/Include
LDFLAGS +=\
    -Wl,--gc-sections -Wl,-u_AppColdStart -Wl,-u_AppWarmStart -T$(LINKCMD) $(APPOBJS) \
    -Wl,--start-group $(addprefix -l,$(LDLIBS)) -Wl,--end-group
OBJCOPY_FLAGS :=\
    -j .version -j .bir -j .vsr_table -j .vsr_handlers \
    -j .flashheader -j .oad -j .mac -j .heap_location \
    -j .rtc_clt -j .rodata -j .data -j .text -j .bss \
    -j .heap -j .stack -S

##########################################
# Set default variables if not specified #
##########################################
ifndef JENNIC_HIGH_POWER
	JENNIC_HIGH_POWER ?= 0
else
	PLATFORM_REBUILD_OBJS := radio.o
endif

ifndef DEFAULT_ADDR
	DEFAULT_ADDR ?= 0x07
else
	PLATFORM_REBUILD_OBJS := radio.o
endif

ifndef DEFAULT_PANID
	DEFAULT_PANID ?= 0x11
else
	PLATFORM_REBUILD_OBJS := radio.o
endif

ifndef DEFAULT_CHANNEL
	DEFAULT_CHANNEL	?= 0x11
else
	PLATFORM_REBUILD_OBJS := radio.o
endif

DEFAULT_VAR := \
    -DJENNIC_HIGH_POWER=$(JENNIC_HIGH_POWER) \
	-DDEFAULT_ADDR=$(DEFAULT_ADDR) \
	-DDEFAULT_PANID=$(DEFAULT_PANID) \
	-DDEFAULT_CHANNEL=$(DEFAULT_CHANNEL)

######################
# Platform toolchain #
######################
#CC := ba-elf-gcc
#OBJCOPY := ba-elf-objcopy
CC := ba-elf-gcc
OBJCOPY := ba-elf-objcopy

###########################
# Platform-specific rules #
###########################
flash: $(BUILD_DIR)/$(TARGET).bin
	$(info FLASHING $<...)
	@jenprog -t $(JENNIC_SERIAL_DEV) $(BUILD_DIR)/$(TARGET).bin

$(PLATFORM_BUILD_DIR)/AppQueueApi.o: $(COMPONENTS_BASE_DIR)/AppQueueApi/Source/AppQueueApi.c
	$(info COMPILING $< -> $@)
	@mkdir -p $(dir $@)
	@$(CC) -c -o $@ $(CFLAGS) $<
