#include <jendefs.h>
#include <AppHardwareApi.h>
#include <motelib/sys/timer.h>

void _platform_timer_init(void)
{
    /* NOTES: Current implementation uses Tick Timer, not Timer0 */

    vAHI_TickTimerInterval(16000*16); // 16-ms interval
    vAHI_TickTimerConfigure(E_AHI_TICK_TIMER_RESTART);
    vAHI_TickTimerIntEnable(TRUE);

    //vAHI_TimerDIOControl(E_AHI_TIMER_0, FALSE);
    //vAHI_TimerEnable(
    //        E_AHI_TIMER_0, /* Timer ID */
    //        7,             /* Prescaler = 2^7 */
    //        FALSE,         /* IntRiseEnable   */
    //        TRUE,          /* IntPeriodEnable */
    //        FALSE          /* OutputEnable    */
    //        );
    //vAHI_TimerClockSelect(E_AHI_TIMER_0,
    //        FALSE,
    //        FALSE);
    //vAHI_TimerStartRepeat(
    //        E_AHI_TIMER_0,
    //        1000,  /* #pulse before high -- don't care */
    //        2000   /* #pulse before low -- interrupt */
    //        );
}
