#include <string.h>
#include <AppQueueApi.h>
#include <mac_sap.h>
#include <mac_pib.h>

#include <motelib/system.h>
#include <motelib/radio.h>
#include <motelib/sys/radio.h>
#include <motelib/sys/main.h>

//////////////////////////////////////////////////////////////////////
// Global variables
//////////////////////////////////////////////////////////////////////
static void *m_mac;
static MAC_Pib_s *m_macpib;

/**
 * Initializes radio subsystem.  Called by platform-independent radio
 * intialization module
 */
void _platform_radio_init()
{
    /* Set up the MAC handles. Must be called AFTER u32AppQApiInit() */
    m_mac = pvAppApiGetMacHandle();
    m_macpib = MAC_psPibGetHandle(m_mac);

    /* Set Pan ID and short address in PIB (also sets match registers in hardware) */
    MAC_vPibSetPanId(m_mac, DEFAULT_PANID);
    MAC_vPibSetShortAddr(m_mac, DEFAULT_ADDR);

    /* Set the channel (11 to 26) */
    eAppApiPlmeSet (PHY_PIB_ATTR_CURRENT_CHANNEL, DEFAULT_CHANNEL);

    /* Enable receiver to be on when idle */
    MAC_vPibSetRxOnWhenIdle(m_mac, TRUE, FALSE);

    /* Enable high-power module if specified */
#if JENNIC_HIGH_POWER
    vAHI_HighPowerModuleEnable(TRUE, TRUE);
#endif

    /* Allow nodes to associate */
    m_macpib->bAssociationPermit = 1;

    _platform_address = m_macpib->u16ShortAddr_ReadOnly;
    _platform_panid   = m_macpib->u16PanId_ReadOnly;
    _platform_channel = DEFAULT_CHANNEL;
}

//////////////////////////////////////////////////////////////////////
// Radio API Implementation
//////////////////////////////////////////////////////////////////////

/**
 * (Platform-specific function)
 */
void _platform_radio_send(
        Address dst,
        MessageHeader *header,
        const void *data,
        uint8_t len)
{
    MAC_McpsReqRsp_s  sMcpsReqRsp;
    MAC_McpsSyncCfm_s sMcpsSyncCfm;
    uint8 *pu8Payload, i = 0;

    /* Create frame transmission request */
    sMcpsReqRsp.u8Type = MAC_MCPS_REQ_DATA;
    sMcpsReqRsp.u8ParamLength = sizeof(MAC_McpsReqData_s);
    /* Set handle so we can match confirmation to request */
    sMcpsReqRsp.uParam.sReqData.u8Handle = 1;
    /* Use short address for source */
    sMcpsReqRsp.uParam.sReqData.sFrame.sSrcAddr.u8AddrMode = 2;
    sMcpsReqRsp.uParam.sReqData.sFrame.sSrcAddr.u16PanId = m_macpib->u16PanId_ReadOnly;
    sMcpsReqRsp.uParam.sReqData.sFrame.sSrcAddr.uAddr.u16Short = m_macpib->u16ShortAddr_ReadOnly;
    /* Use short address for destination */
    sMcpsReqRsp.uParam.sReqData.sFrame.sDstAddr.u8AddrMode = 2;
    sMcpsReqRsp.uParam.sReqData.sFrame.sDstAddr.u16PanId = m_macpib->u16PanId_ReadOnly;
    sMcpsReqRsp.uParam.sReqData.sFrame.sDstAddr.uAddr.u16Short = dst;

    /* Frame requires no ack if broadcast */
    if (dst == BROADCAST_ADDR)
        sMcpsReqRsp.uParam.sReqData.sFrame.u8TxOptions = 0;
    else
        sMcpsReqRsp.uParam.sReqData.sFrame.u8TxOptions = MAC_TX_OPTION_ACK;

    pu8Payload = sMcpsReqRsp.uParam.sReqData.sFrame.au8Sdu;

    m_macpib->u8Dsn = header->seqno;

    memcpy(pu8Payload, header, sizeof(MessageHeader));

    for (i = sizeof(MessageHeader); i < (len + sizeof(MessageHeader)); i++)
    {
        pu8Payload[i] = *((char*)data++);
    }

    /* Set frame length */
    sMcpsReqRsp.uParam.sReqData.sFrame.u8SduLength = i;

    /* Request transmit */
    vAppApiMcpsRequest(&sMcpsReqRsp, &sMcpsSyncCfm);
}

void _check_radio_events()
{
    MAC_MlmeDcfmInd_s *mlmeInd;
    MAC_McpsDcfmInd_s *mcpsInd;
    MAC_RxFrameData_s *frame;

    /* Check for anything on the MCPS upward queue */
    mcpsInd = psAppQApiReadMcpsInd();
    if (mcpsInd != NULL)
    {
        switch(mcpsInd->u8Type)
        {
        case MAC_MCPS_IND_DATA:  /* Incoming data frame */
            frame = &mcpsInd->uParam.sIndData.sFrame;
            _platform_radio_rxStatus.lqi = frame->u8LinkQuality;
            _sys_radio_receiveDone(
                    frame->sSrcAddr.uAddr.u16Short,
                    (char*)frame->au8Sdu,
                    frame->u8SduLength);
            break;

        case MAC_MCPS_DCFM_DATA: /* Incoming acknowledgement or ack timeout */
            // Report status of latest transmission
            // TODO: set tx-status
            _sys_radio_sendDone(
                    mcpsInd->uParam.sDcfmData.u8Status == MAC_ENUM_SUCCESS
                    ? RADIO_OK : RADIO_FAILED);

        default:
            break;
        }
        vAppQApiReturnMcpsIndBuffer(mcpsInd);
    }

    /* Check for anything on the MLME upward queue */
    mlmeInd = psAppQApiReadMlmeInd();
    if (mlmeInd != NULL)
    {
        switch (mlmeInd->u8Type)
        {
        default:
            break;
        }
        vAppQApiReturnMlmeIndBuffer(mlmeInd);
    }
}
