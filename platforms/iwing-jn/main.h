#ifndef __MAIN_H__
#define __MAIN_H__

#include <stdint.h>

#define LO_BYTE(word)  (word & 0xFF)
#define HI_BYTE(word)  (((uint16_t)word) >> 8)

#define BUTTON_DIO 16

#endif
