#ifndef __PLATFORM_H__
#define __PLATFORM_H__

#include <stdint.h>

#define NUM_LEDS      3
#define PLATFORM_MAX_MSG_LEN 100

#define PLATFORM_SIM

// Define timer tick length in milliseconds
#define PLATFORM_TIMER_TICK_INTERVAL  16

// Intercept printf calls so that output can be redirected as needed
#define printf(fmt,args...) printf_sim(fmt,##args)

typedef enum sensor_type_enum
{
	SENSOR_0 = 0,
	SENSOR_1,
	SENSOR_2,
	SENSOR_3,
	SENSOR_4,
	SENSOR_5
} SensorType;

typedef enum actor_type_enum
{
    ACTOR_0 = 0,
    ACTOR_1,
    ACTOR_2,
    ACTOR_3,
    ACTOR_4,
    ACTOR_5
} ActorType;

typedef enum led_enum
{
	RED,
	GREEN,
	BLUE
}LED;

typedef struct
{
} RadioTxStatus;

typedef struct
{
    uint8_t rssi;
} RadioRxStatus;

typedef uint16_t StorageAddr;

#endif
