#include <motelib/button.h>

static ButtonHandler buttonHandler;
static ButtonStatus buttonStatus;

void _initButtonModule()
{
    buttonStatus = BUTTON_RELEASED;
}

void buttonSetHandler(ButtonHandler handler)
{
    buttonHandler = handler;
}

bool buttonIsPressed()
{
    return buttonStatus == BUTTON_PRESSED;
}

void _buttonStatus(ButtonStatus status)
{
    // See if the status has changed
    if (status != buttonStatus && buttonHandler)
        buttonHandler(status);

    buttonStatus = status;
}
