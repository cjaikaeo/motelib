#include "main.h"
#include <motelib/sensor.h>

#define NUM_SENSORS 32

typedef struct
{
    bool reading;
    SensorDataReady callback;
    uint16_t recentVal;
} SensorInfo;

static uint16_t digitalInputs;
static SensorInfo sensorInfo[NUM_SENSORS];

////////////////////////////////////////////////////////////
void _initSensorModule()
{
    uint16_t i;
    for (i = 0; i < NUM_SENSORS; i++)
    {
        sensorInfo[i].callback  = NULL;
        sensorInfo[i].recentVal = 0;
        sensorInfo[i].reading   = false;
    }
    digitalInputs = 0xFFFF;
}

////////////////////////////////////////////////////////////
SensorStatus sensorRequestAnalog(SensorType sensor, SensorDataReady senseDone)
{
    uint8_t sensorType = sensor;

    // Check if there is any pending read
    if (sensorInfo[sensor].reading)
        return SENSOR_BUSY;
    sensorInfo[sensor].reading = true;
    sensorInfo[sensor].callback = senseDone;
    _send_to_sim(CMD_SENSE, (char*)&sensor, 1);
    return SENSOR_OK;
}

///////////////////////////////////////////////////////////
uint16_t sensorAnalogResult(SensorType sensor)
{
    return sensorInfo[sensor].recentVal;
}

////////////////////////////////////////////////////////////
uint8_t sensorReadDigital(SensorType sensor)
{
    return (digitalInputs & (1<<sensor)) != 0;
}

////////////////////////////////////////////////////////////
bool sensorAnalogWaiting(SensorType sensor)
{
    return sensorInfo[sensor].reading;
}

////////////////////////////////////////////////////////////
bool sensorAnalogAvailable(SensorType sensor)
{
    return !sensorAnalogWaiting(sensor);
}

////////////////////////////////////////////////////////////
void _sensorReady(SensorType sensor, uint16_t value)
{
    sensorInfo[sensor].recentVal = value;
    if (sensorInfo[sensor].callback)
    {
        sensorInfo[sensor].callback(value);
        sensorInfo[sensor].callback = NULL;
    }
    sensorInfo[sensor].reading = false;
}

////////////////////////////////////////////////////////////
void _sensorSetDigital(uint16_t val)
{
    digitalInputs = val;
}
