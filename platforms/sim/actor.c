#include <motelib/actor.h>
#include "main.h"

static uint8_t actorState[256];

void _initActorModule()
{
    uint16_t i;
    for (i = 0; i < 256; i++)
        actorState[i] = 0;
}

void actorSetState(ActorType actor, uint8_t state)
{
    char buf[2];
    buf[0] = actor;
    buf[1] = state;
    actorState[actor] = state;
    _send_to_sim(CMD_SET_ACTOR, buf, 2);
}

uint8_t actorGetState(ActorType actor)
{
    return actorState[actor];
}
