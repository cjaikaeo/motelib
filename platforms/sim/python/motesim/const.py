from motelib import Enum
from topovis import Scene, LineStyle, FillStyle

## Default line style used for simulating radio packet transmissions
TX_STYLE = LineStyle(color=(1,0,0),dash=(5,5))

## Default color of nodes when they are on
NODE_ON_COLOR = (0,0,0)

## Default color of nodes when they are off
NODE_OFF_COLOR = (.8,.8,.8)

## Broadcast address
BROADCAST_ADDR = 0xFFFF

##############
# The remaining variables should not be documented by Doxygen
# @cond
Command = Enum(  # Order must match cmd_enum in main.h exactly
        'CMD_BOOT',
        'CMD_SHUTDOWN',
        'CMD_DEBUG',
        'CMD_RADIO_SEND',
        'CMD_RADIO_SEND_ACK',
        'CMD_RADIO_RECV',
        'CMD_LED',
        'CMD_SENSE',
        'CMD_SET_ACTOR',
        'CMD_SET_BUTTON',
        'CMD_UART_OUTPUT',
        'CMD_UART_INPUT',
        'CMD_DIGITAL_INPUTS',
        'CMD_STORAGE_READ_REQ',
        'CMD_STORAGE_WRITE_REQ',
        'CMD_STORAGE_READ_DONE',
        'CMD_STORAGE_WRITE_DONE',
        )
ButtonStatus = Enum(  # Order must match ButtonStatus enum in api/button.h
        'BUTTON_PRESSED',
        'BUTTON_RELEASED',
        )
RadioStatus = Enum(  # Order must match RadioStatus enum in api/radio.h
        'RADIO_OK',
        'RADIO_FAILED',
        )
# @endcond

