'''
@package motesim.SimGateway

Provides a Gateway class for accessing virtual gateway device in a simulator
'''
import sys, re
import socket
from MoteSim import strToList, listToStr, receiveBlock
from time import sleep
from threading import Thread

## Defines a broadcast address
BROADCAST_ADDR = 0xFFFF

###########################################################
class Gateway(object):
    '''
    Defines a generic class for accessing virtual gateway device in a
    simulator

    Message format
    
     +---------+-----+-----+---------+--------+----------+
     | FrameID | Src | Dst | DataLen | Header |  Data... |
     +---------+-----+-----+---------+--------+----------+
         (1)     (2)   (2)     (2)      (m)        (n)
    
    '''

    ############################
    def __init__(self, device='localhost:30000', autoListen=True):
        '''
        Instantiates a Gateway object that connects to a simulator via the
        specified TCP connection.

        @param device 
        Specifies the connection string in forms of <host>:<port> (e.g.,
        localhost:30000)

        @param autoListen 
        Optionally indicates that the receive method will be invoked
        automatically from a separate thread.  Otherwise, the listen method
        must be called to put the program in a loop that keeps listening to
        incoming messages.
        '''
        m = re.match(r'(.*):(\d+)', device)
        if m is None:
            raise Exception('Error in device string')
        addr = m.group(1)
        port = int(m.group(2))
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.connect((addr, port))
        id_data = receiveBlock(self.socket, 2)
        self.nodeId = ord(id_data[0]) + 256*ord(id_data[1])

        self.seqNo = 0

        self.channel = 0x11  # TODO: retrieve it from sim
        self.panid = 0x22    # TODO: retrieve it from sim

        # start listening thread if autoListen is true
        if autoListen:
            self.listen_thread = Thread(target=self.listen)
            self.listen_thread.setDaemon(True)
            self.listen_thread.start()


    ############################
    def getPanId(self):
        '''
        Returns PAN ID that this gateway is in
        '''
        return self.panid

    ############################
    def getChannel(self):
        '''
        Returns the current channel the gateway is using
        '''
        return self.channel

    ############################
    def getAddress(self):
        '''
        Returns this gateway's address

        @return 16-bit integer representing the address of the virtual gateway
        this object is associated to
        '''
        return self.nodeId

    ############################
    def send(self, dest, msgType, msg):
        '''
        Sends a message with specified type.

        @param dest 
        Indicates destination to which the message is destined.  If the value
        is BROADCAST_ADDR or 0xFFFF, the message is sent in broadcast mode.

        @param msgType
        Specifies the 8-bit type value of the message.

        @param msg
        Contains the message to be sent, which can be either a string or a
        list of 8-bit integers.
        '''
        # prepare frame header
        src = self.getAddress()
        length = len(msg)
        self.seqNo += 1
        data = [
                self.seqNo,
                src%256, src/256,
                dest%256, dest/256,
                length%256, length/256,
                ]

        # prepare message header (see lib/sys/radio.h)
        data += [msgType, self.seqNo]

        # append the message after the header
        if isinstance(msg, str):
            msg = strToList(msg)
        data += msg

        # determine the entire length (msg + header)
        length = len(data)

        # send the message with preceding length
        self.socket.send(listToStr([length%256, length/256] + data))

    ############################
    def receive(self, source, msgType, msg):
        '''
        Gets called when a message is received from radio.  This method
        should be overridden to perform appropriate actions.

        @param source
        Contains the address of the message's source

        @param msgType
        Contains the 8-bit type value

        @param msg
        Points to a string containing the message
        '''
        #print 'Receive from %x: type=%d, msg=%s' % (source, msgType, msg)
        pass

    ############################
    def listen(self):
        '''
        Enter an infinite loop that keeps listening to the radio channel for
        incoming messages.  The receive method is called upon message arrival.
        '''
        while True:
            # ignore the first two bytes specifying length
            receiveBlock(self.socket, 2)

            # process frame header
            data = strToList(receiveBlock(self.socket, 7))
            frameId = data[0]
            src = data[1] + 256*data[2]
            dst = data[3] + 256*data[4]
            msgLen = data[5] + 256*data[6]

            # process message header (see lib/sys/radio.h)
            data = strToList(receiveBlock(self.socket, 2))
            msgType = data[0]
            seqNo   = data[1]

            # read the message body
            msg = receiveBlock(self.socket, msgLen)

            # call the receive method only when I am the destination or the
            # message is broadcast
            if dst == BROADCAST_ADDR or self.getAddress() == dst:
                self.receive(src, msgType, msg)

###########################################################
# The variables defined in the following section should not be documented by
# Doxygen
#
# @cond
if __name__ == '__main__':
    device = 'localhost:30000'
    if len(sys.argv) == 2:
        device = sys.argv[1]
    g = Gateway(device=device)
    print 'Connected to simulator with nodeID = %d.' % g.nodeId

    sleep(2)
    print 'Test sending to broadcast address'
    for i in range(3):
        print 'Sending msg#%d' % i
        g.send(BROADCAST_ADDR, 1, 'goodbye')
        sleep(1)
# @endcond
