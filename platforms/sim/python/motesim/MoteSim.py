'''
@package motesim.MoteSim

Provides classes for simulating WSN written for MoteLib
'''
import subprocess
import sys
from threading import Thread, Timer
import math
import socket
import struct
from time import sleep, time
from topovis import Scene, LineStyle, FillStyle
from topovis.TkPlotter import Plotter
from bisect import insort
from motelib import Enum
from peripheral import Uart, Storage
from const import *

## Stores preconfigured LEDs' positions and colors.
#
# The configuration is contained in a list.  Each of the entries is
# corresponding to the particular LED based on
# the index in the list.  An entry is of the format [[posx,posy],[r,g,b]], where
# (posx,posy) indicates the position relative to the node's center, and
# (r,g,b) indicates the displayed color (0 <= r,g,b <= 1).
LEDS_CONFIG = [
        [ [ 5, 5], [1.0, 0.0, 0.0] ],
        [ [ 0, 5], [0.8, 0.8, 0.0] ],
        [ [-5, 5], [0.0, 0.9, 0.0] ],
        ]

## Stores preconfigured actors' positions and colors
#
# The configuration is stored as a list, whose entries are of the same format
# as ::LEDS_CONFIG.
ACTORS_CONFIG = [
        [ [ 5,-6], [1.0, 0.0, 0.0] ],
        [ [ 0,-6], [0.8, 0.8, 0.0] ],
        [ [-5,-6], [0.0, 0.9, 0.0] ],
        ]

## Specify latency for storage write access in seconds
STORAGE_WRITE_LATENCY = 0.0033

## Specify latency for storage read access in seconds
STORAGE_READ_LATENCY = 0.001

###########################################################
def distance(pos1, pos2):
    '''
    Computes the distance between two given points

    @param pos1 Specifies the (x,y) coordinates of the first point
    @param pos2 Specifies the (x,y) coordinates of the second point
    @return the distance between the two points.
    '''
    dx = pos1[0] - pos2[0]
    dy = pos1[1] - pos2[1]
    return math.sqrt(dx*dx + dy*dy)

###########################################################
def strToList(s):
    '''
    Converts a string to a list of integers corresponding to ASCII code of each
    string character

    @param s Stores a string to be converted

    @return a list containing ASCII code of each charater in the string
    '''
    return [ord(x) for x in s]

###########################################################
def listToStr(lst):
    '''
    Converts a list of integers into a string using ASCII table
    '''
    return ''.join([chr(x) for x in lst])

###########################################################
class LostConnectionException(Exception):
    '''
    Exception raised when a connection is lost or terminated
    '''
    pass

############################
def receiveBlock(conn, length):
    '''
    Blocks until the specified amount of data have been successfully
    received

    @param conn Refers to TCP connection handler
    @param length Specifies the size (byte count) of the expected block
    '''
    pieceList = []
    while length > 0:
        piece = conn.recv(length)
        if len(piece) == 0: # the other end has terminated connection
            raise LostConnectionException()
        length -= len(piece)
        pieceList.append(piece)
    return ''.join(pieceList)

###########################################################
class Node(object):
    '''
    Defines generic node objects that can be either gateways or sensor motes.
    More types can be defined by inheriting this class..
    '''

    ###################
    def __init__(self, txRange=100):
        '''
        The class constructor.

        @param txRange 
        Optionally specifies the transmission range of the node.
        '''
        self.txRange = txRange
        self.id = None
        self.sim = None

    ###################
    def __str__(self):
        return '<%s:%d>' % (self.__class__.__name__, self.id)

    ###################
    def __repr__(self):
        return self.__str__()

    ###################
    def _start(self):
        self.thread = Thread(target=self._processEvents)
        self.thread.setDaemon(True)
        self.thread.start()

    ###################
    def showMessage(self, msg):
        print>>sys.stderr, '[%7.2f]#%d: %s' % (
                time()-self.sim.startTime,
                self.id,
                msg)

    ###################
    def isOn(self):
        '''
        Determines whether the node is currently on.  It should be overridden
        by subclass of Node.

        @return True if the node is currently on; False otherwise.
        '''
        return False

    ###################
    def move(self, x, y):
        '''
        Moves the node to the specified location.

        @param x x-value in the new location's (x,y) coordinates
        @param y y-value in the new location's (x,y) coordinates
        '''
        self.pos = (x,y)
        self.sim._updateNeighborList(self.id)
        if self.sim.gui:
            self.animate(self.sim.scene.nodemove, self.id, x, y)

    ###################
    def _processRadioMsg(self, msg):
        '''
        (Internal use) sends the given message to all neighbors in range.
        Note that msg parameter must be a list, not a string.
        '''
        sim = self.sim

        # TODO make this code endian-independent
        msgInfo = {
                'seqno' : msg[0],
                'src'   : msg[1] + msg[2]*256,
                'dest'  : msg[3] + msg[4]*256,
                'len'   : msg[5]-1,
                'type'  : msg[6],
                'data'  : msg[7:7+msg[5]-1],
                }
        dest = msgInfo['dest']

        if sim.gui:
            self.animateRadioSend(msgInfo)
        else:
            self.showMessage('Transmits packet to %d' % dest)

        # The transmission is considered successful if one of the following
        # conditions is true:
        # 1. The transmission is broadcast
        # 2. The transmission is unicast and the destination receives AND
		#    acknowledges the message successfully (i.e., receiveRadioMsg
		#    returns true)
        if dest == BROADCAST_ADDR:
            success = True
        else:
            success = False
        for (dist,neighbor) in self.sim.neighborList[self.id]:
            if dist <= self.txRange:
                # calculate fake RSSI based on how far the source is from me
                # and normalized it to [0,255]
                rssi = int((self.txRange-dist)/self.txRange*255)

                ret = neighbor.receiveRadioMsg(msg, rssi)

				# if this is unicast, we also have to make sure that ACK will
				# come back successfully by checking the destination's tx
				# range
                if dest == neighbor.id and dist <= neighbor.txRange and ret:
                    success = True
            else:
                # The remaining neighbors are out of reach
                break
        return success

    ###################
    def setActorState(self, actorNo, state):
        '''
        Handle actor state-change event.  The default behavior is blinking the
        corresponding light on the node.  This method should be overridden for
        different effect.

        @param actorNo Specifies the actor's ID
        @param state Specifies the desired actor state
        '''
        if self.sim.gui:
            self.animateActor(actorNo, state)
        else:
            self.showMessage('Actor#%d state -> %d' % (actorNo, state))

    ###################
    def animate(self, cmd, *args, **kwargs):
        '''
        Provides a convenient wrapper to access the Simulator::animate method in the
        Simulator object.

        @param cmd TopoVis's scene scripting command
        @param args Optional positional arguments
        @param args Optional keyword arguments
        '''
        self.sim.animate(cmd, *args, **kwargs)

    ####################
    def animateLeds(self,ledno,state):
        '''
        Animates LED status by displaying dots in various colors.  Dot
        positions and colors can be reconfigured via the ::LEDS_CONFIG variable.

        @param ledno LED's number whose state is to be changed
        @param state New LED's state
        '''
        scene = self.sim.scene
        (x,y) = self.pos
        shape_id = '%d:led%d' % (self.id,ledno)
        if state == 0:
            self.animate(scene.delshape, shape_id)
            return
        if ledno < len(LEDS_CONFIG):
            pos,color = LEDS_CONFIG[ledno]
            x,y = x+pos[0],y+pos[1]
        self.animate(scene.circle, x, y, 2, id=shape_id,
                line=LineStyle(color=color), fill=FillStyle(color=color))

    ####################
    def animateActor(self,actorNo,state):
        '''
        Animates actor status by displaying dots in various colors.  Dot
        positions and colors can be reconfigured via the ::ACTORS_CONFIG
        variable.

        @param actorNo Actor's number whose state is to be changed
        @param state New actor's state
        '''
        scene = self.sim.scene
        (x,y) = self.pos
        shape_id = '%d:actor%d' % (self.id,actorNo)
        if state == 0:
            self.animate(scene.delshape, shape_id)
            return
        if actorNo < len(ACTORS_CONFIG):
            pos,color = ACTORS_CONFIG[actorNo]
            x,y = x+pos[0],y+pos[1]
        self.animate(scene.rect, x-1, y-1, x+1, y+1, id=shape_id,
                line=LineStyle(color=color), fill=FillStyle(color=color))

    ####################
    def animateRadioSend(self, msgInfo):
        '''
        Animates transmission of radio messages.  Currently all messages are
        animated in the same way.  Subclasses of this class can override this
        method to display different effects for certain messages.

        @param msgInfo Dictionary containing message's meta-data, along with
        the data, whose transmission is to be animated.
        '''
        self.animate(self.sim.scene.circle,
                self.pos[0],
                self.pos[1],
                self.txRange,
                line=TX_STYLE,
                delay=.2)

    ####################
    def debug(self, msg):
        '''
        Processes debugging messages passed by invoking the debug function in
        the MoteLib API.  This default method simply prints out the debug
        message, so it should be overridden to provide more information on the
        simulation scene.

        @param msg Debugging message passed from a node
        '''
        self.showMessage('DEBUG: %s' % msg)


###########################################################
class Mote(Node):
    '''
    This class defines a mote object that is used for interacting with
    an external simulated mote program.  Events from and to an external mote
    are streams via stdin and stdout.
    '''

    ###################
    def __init__(self,
            executable, txRange=100, panid=0x2222, channel=0x1A,
            storagePath=None):
        '''
        The class constructor.
        
        @param executable 
        Path to an executable built with MoteLib

        @param txRange 
        Optionally specifies the transmission range of the node.

        @param panid 
        Optionally specifies PAN ID

        @param channel 
        Optionally specifies RF channel

        @param storagePath 
        Optionally specifies path to the file representing the persistent
        storage for the mote.
        '''
        self.proc = subprocess.Popen(
                executable,
                stdin=subprocess.PIPE,
                stdout=subprocess.PIPE)
        Node.__init__(self, txRange)
        self.on = False
        self.uart = Uart(self)
        self.panid = panid
        self.channel = channel
        self.digitalInputs = 0xFFFF

        self.storage = Storage(storagePath)

    ###################
    def _dispatchMsg(self, cmd, msg):
        '''
        (Internal use) dispatches sim messages between the simulator and
        mote processes.
        '''
        if cmd == Command.CMD_LED:
            if self.sim.gui:
                self.animateLeds(msg[0], msg[1])
            else:
                self.showMessage('LED%d state -> %d' % (msg[0], msg[1]))
        elif cmd == Command.CMD_RADIO_SEND:
            if self._processRadioMsg(msg):
                # successful
                self._sendToMoteProc([RadioStatus.RADIO_OK],
                        type=Command.CMD_RADIO_SEND_ACK)
            else:
                # failed
                self._sendToMoteProc([RadioStatus.RADIO_FAILED],
                        type=Command.CMD_RADIO_SEND_ACK)
        elif cmd == Command.CMD_SENSE:
            sensor = msg[0]
            value = self.sense(sensor)
            self._sendToMoteProc(
                    [sensor, value%256, value/256],
                    type=Command.CMD_SENSE)
        elif cmd == Command.CMD_SET_ACTOR:
            self.setActorState(msg[0], msg[1])
        elif cmd == Command.CMD_DEBUG:
            self.debug(listToStr(msg))
        elif cmd == Command.CMD_UART_OUTPUT:
            self.uart.moteOutput(msg)
        elif cmd == Command.CMD_STORAGE_WRITE_REQ:
            addr = msg[0] + 256*msg[1]
            data = msg[2]
            Timer(STORAGE_WRITE_LATENCY,
                    self._performStorageWrite,
                    [addr, data]).start()
        elif cmd == Command.CMD_STORAGE_READ_REQ:
            addr = msg[0] + 256*msg[1]
            Timer(STORAGE_READ_LATENCY,
                    self._performStorageRead,
                    [addr]).start()

    ###################
    def _processEvents(self):
        '''
        (Internal use) reads and processes a message from stream.
        +-----+-------+----------------------+
        | len |  cmd  |   msg (optional)...  |
        +-----+-------+----------------------+
           1      1             len
        '''
        while True:
            buf = strToList(self.proc.stdout.read(2))
            if not buf: return None
            length = buf[0]
            cmd = buf[1]
            msg = strToList(self.proc.stdout.read(length))
            self._dispatchMsg(cmd, msg)

    ###################
    def _sendToMoteProc(self, msg, type=None):
        '''
        (Internal use) sends a specified message to the process associated
        with this mote object.
        '''
        if type is not None:  # send with header
            length = len(msg)
            msg = [length, type] + msg
        self.proc.stdin.write(listToStr(msg))

    ###################
    def isOn(self):
        '''
        Determines whether the mote is currently on.
        '''
        return self.on

    ###################
    def boot(self, id=None):
        '''
        Turns the mote on with node ID optionally specified.

        @param id Optionally specifies the ID (i.e., address) of the node
        '''
        if id is None:
            id = self.id
        data = struct.pack('<HHBH',
                id, self.panid, self.channel, self.storage.size())
        self._sendToMoteProc(strToList(data), type=Command.CMD_BOOT)
        self.on = True

        if self.sim.gui:
            self.animate(self.sim.scene.nodecolor, self.id, *NODE_ON_COLOR)
        else:
            self.showMessage('Booted')

    ###################
    def shutdown(self):
        '''
        Turns the mote off.
        '''
        self._sendToMoteProc([], type=Command.CMD_SHUTDOWN)
        self.on = False

        if self.sim.gui:
            self.animate(self.sim.scene.nodecolor, self.id, *NODE_OFF_COLOR)
            for i in range(3):  # turn off all LEDs
                self.animateLeds(i,0)
        else:
            self.showMessage('Shutdown')

    ###################
    def receiveRadioMsg(self, msg, rssi):
        '''
        Automatically called when this mote receives a radio packet.
        This method can be overridden to add effects of packet losses, e.g.,
        by probabilistically returning false.

        @param msg Message being received.
        '''
        if self.isOn():
            self._sendToMoteProc(msg + [rssi], type=Command.CMD_RADIO_RECV)

            # In the default simulation model, transmissions are always
            # successful.  If this method is to be overridden, make sure it
            # returns true if and only if _sendToMoteProc is called
            return True
        else:
            return False

    ####################
    def sense(self,sensor):
        '''
        Automatically called when a sensor reading request is issued on the
        mote.  It currently always returns 0 and should be overridden for
        simulating sensor measurement.

        @param sensor
        Sensor's ID whose measurement reading is being requested
        '''
        return 0

    ####################
    def pressButton(self):
        '''
        Emulates a button press (and hold) event on the mote.
        '''
        self._sendToMoteProc([ButtonStatus.BUTTON_PRESSED],
                type=Command.CMD_SET_BUTTON)

    ####################
    def releaseButton(self):
        '''
        Emulates a button release event on the mote.
        '''
        self._sendToMoteProc([ButtonStatus.BUTTON_RELEASED],
                type=Command.CMD_SET_BUTTON)

    ####################
    def digitalInput(self, bit, val=None):
        '''
        Set or get current specified digital input pin

        @param bit specifies the input pin number
        @param bitval if set, set the bit to the given value (0 or 1);
        otherwise the method returns the current bit value
        '''
        if val is None:
            return (self.digitalInputs & (1<<bit)) >> bit
        else:
            if val == 1:
                self.digitalInputs |= (1<<bit)
            else:
                self.digitalInputs &= ~(1<<bit)
            self._sendToMoteProc(
                    [self.digitalInputs % 256, self.digitalInputs/256],
                    type=Command.CMD_DIGITAL_INPUTS)

    ####################
    def _performStorageWrite(self, addr, data):
        self.storage.write(addr, data)
        self._sendToMoteProc([], type=Command.CMD_STORAGE_WRITE_DONE)

    ####################
    def _performStorageRead(self, addr):
		data = self.storage.read(addr)
		self._sendToMoteProc([data], type=Command.CMD_STORAGE_READ_DONE)


###########################################################
class Gateway(Node):
    '''
    Defines a gateway object that is used for interacting with an external
    simulated gateway program.  Events from and to an external gateway node
    are streams via a TCP connection.
    '''

    ###################
    def __init__(self, startPort=30000, txRange=100):
        '''
        The class constructor.
        
        @param pos 
        Tuple (x,y) specifying the gateway location on the scene.
        
        @param startPort
        Optionally sets the starting TCP port to listen to.  If this port is
        not available, the next port will be tried in order.

        @param txRange
        Optionally sets the gateway's transmission range.
        '''
        self.txRange = txRange
        self.id = None
        self.sim = None
        self.listen_port = startPort
        self.conn = None

    ###################
    def _start(self):
        '''
        (Internal use) starts a thread to listen for incoming connection from
        simulated gateways.
        '''
        if self.sim.gui:
            self.animate(self.sim.scene.nodewidth, self.id, 3)
        else:
            self.showMessage('Turned on as a gateway')

        self.thread = Thread(target=self._monitorClient)
        self.thread.setDaemon(True)
        self.thread.start()

    ###################
    def _monitorClient(self):
        '''
        (Internal use) listen to incoming connection from simulated gateways.
        '''
        scene = self.sim.scene
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        while True:
            try: # try binding port from the specified start port number
                sock.bind(('', self.listen_port))
                break
            except socket.error:
                self.listen_port += 1

        # one client at a time
        sock.listen(1)

        while True:
            # no current connection
            self.conn = None

            # wait for TCP connection request from external gateway app
            self.debug('Gateway starts listening on port %d' %
                    self.listen_port)
            self.conn, addr = sock.accept()
            self.debug('Establish gateway connection with %s:%s' % addr)
            if self.sim.gui:
                self.animate(scene.nodecolor, self.id, *NODE_ON_COLOR)

            # send node ID on the connection right away
            self.conn.send(listToStr([self.id%256, self.id/256]))

            try:
                while True:
                    # Wait for the first incoming two bytes which specify the
                    # header+message length
                    data = strToList(receiveBlock(self.conn, 2))
                    length = data[0] + 256*data[1]

                    # Read the entire header+message and broadcast it
                    msg = receiveBlock(self.conn, length)
                    self._processRadioMsg(strToList(msg))
            except LostConnectionException:
                self.debug('Gateway connection terminated')
                if self.sim.gui:
                    self.animate(scene.nodecolor, self.id, *NODE_OFF_COLOR)
            except Exception:
                self.debug('Gateway Socket Error (please investigate)')
                if self.sim.gui:
                    self.animate(scene.nodecolor, self.id, *NODE_OFF_COLOR)

    ###################
    def isOn(self):
        '''
        Determines whether the gateway is currently on.
        '''
        return (self.conn is not None)

    ###################
    def receiveRadioMsg(self, msg, rssi):
        '''
        Automatically called when this gateway receives a radio packet.  This
        method can be overridden to add effects of packet losses, e.g., by
        probabilistically returning false.
        '''
        if self.conn is not None:
            length = len(msg)
            self.conn.send(listToStr([length%256, length/256]+msg))

            # In the default simulation model, gateways always receive
            # messages successfully.
            return True
        else:
            return False # Gateway is not online


###########################################################
class Simulator(object):
    '''
    Defines a simulator object that hosts nodes of various kinds (gateways,
    motes).  Currently, it uses TopoVis's TkPlotter as a visualization engine.
    TopoVis's simulation scene can also be accessed from this object's
    attribute.
    '''

    ############################
    def __init__(self, gui=True):
        '''
        The class constructor.
        '''
        self.nodes = {}
        self.neighborList = {}
        self.gui = gui

        if self.gui:
            # Setup an animating canvas
            self.scene = Scene(realtime=True)
            self.tkplot = Plotter(windowTitle='MoteSim')
            self.scene.addPlotter(self.tkplot)

        # set startTime to None to indicate that the simulation hasn't started
        self.startTime = None

    ############################
    def addNode(self, node, pos, id=None):
        '''
        Adds a new node to the simulation.
        
        @param node 
        The node object to be added.  It must be an instant of the Node class
        or any of its subclasses.

        @param pos 
        A tuple (x,y) specifying the node position on the simulated scene.
        '''
        if id is None:
            if len(self.nodes) == 0:
                id = 0
            else:
                id = max(self.nodes.keys())+1
        if id in self.nodes.keys():
            raise Exception('Duplicate node ID %d' % id)
        node.id = id
        node.sim = self
        node.pos = pos
        self.nodes[id] = node
        self._updateNeighborList(id)
        return node

    ############################
    def _updateNeighborList(self, id):
        '''
        (Internal use) Maintains each node's neighbor list by sorted distance
        after affected by addition or relocation of node with ID id
        '''
        me = self.nodes[id]

        # (re)sort other nodes' neighbor lists by distance
        for n in self.nodes.values():
            # skip this node
            if n is me:
                continue

            nlist = self.neighborList[n.id]

            # remove this node from other nodes' neighbor lists
            for i,(dist,neighbor) in enumerate(nlist):
                if neighbor is me:
                    del nlist[i]
                    break

            # then insert it while maintaining sort order by distance
            insort(nlist, (distance(n.pos, me.pos), me))

        # construct this node's neighbor list
        self.neighborList[id] = [(distance(n.pos, me.pos), n)
                for n in self.nodes.values() if n is not me]
        self.neighborList[id].sort()

    ############################
    def setTxRange(self, id, txRange):
        '''
        Sets the transmission range of a node with index id.  This can also be
        done by altering the txRange attribute of the node itself.

        @param id The ID of the node to change the transmission range
        @param txRange The new transmission range
        '''
        self.nodes[id].txRange = txRange

    ############################
    def run(self, bootMotes=True, script=None):
        '''
        Enters simulation main loop which waits and processes events from
        nodes.
        
        @param bootMotes 
        Indicates whether all motes should be booted automatically once the
        simulation starts.
        
        @param script
        Optionally specifies a function that will be executed in a separate
        thread to simulate external stimuli for the simulation scene.
        '''

        # record current timestamp
        self.startTime = time()

        if self.gui:
            # draw nodes on animating canvas
            scene = self.scene
            for m in self.nodes.values():
                scene.execute(0, scene.node, m.id, m.pos[0], m.pos[1])
                scene.execute(0, scene.nodecolor, m.id, *NODE_OFF_COLOR)

        # start processing events from each node
        for m in self.nodes.values():
            m._start()

        # if bootMotes is true, boot all the motes
        if bootMotes:
            for m in self.nodes.values():
                if isinstance(m, Mote): m.boot()

        # start processing events from environments
        if script is not None:
            thrEnv = Thread(target=script)
            thrEnv.setDaemon(True)
            thrEnv.start()

        if self.gui:
            # enter Tk main loop
            self.tkplot.tk.mainloop()
        else:
            raw_input()

        # kill all the mote processes
        for m in self.nodes.values():
            if isinstance(m, Mote):
                print "Killing Mote#%d process..." % m.id
                m.proc.kill()

    ###################
    def animate(self, cmd, *args, **kwargs):
        '''
        Invokes a TopoVis animation command.  See TopoVis documentation for
        available commands.

        @param cmd TopoVis's scene scripting command
        @param args Optional positional arguments
        @param kargs Optional keyword arguments
        '''
        if self.startTime is None:
            simTime = 0
        else:
            simTime = time()-self.startTime
        self.scene.execute(simTime, cmd, *args, **kwargs)

    ###################
    def ipython_console(self):
        sim = self
        import IPython
        IPython.embed(
                banner1='Welcome to MoteSim Shell (IPython Console)...',
                exit_msg='Leaving MoteSim'
                )

        if self.gui:
            self.tkplot.tk.quit()

    ###################
    def console(self):
        import code
        console = code.InteractiveConsole({'sim':self})
        print 'Welcome to MoteSim Shell (Python Console)...'
        console.interact()

        if self.gui:
            self.tkplot.tk.quit()

    ###################
    def simple_console(self):
        import code
        print 'Welcome to MoteSim Shell (Simple Console)...'
        sim = self
        while True:
            sys.stdout.write('>>> ')
            cmd = sys.stdin.readline()
            if len(cmd) == 0:
                break
            try:
                eval(code.compile_command(cmd))
            except Exception,e:
                print e

        if self.gui:
            self.tkplot.tk.quit()

###########################################################
