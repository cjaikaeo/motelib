import sys
from motesim.uarttcp import UartTcp, LostConnectionException

############################################################
if __name__ == '__main__':
    if len(sys.argv) != 3:
        print 'Usage: %s <serial-device>[@baudrate] <ip:port>' % sys.argv[0]
        print
        print 'Example: %s /dev/ttyUSB0@115200 localhost:12345' % sys.argv[0]
        exit(1)

    uart_info = sys.argv[1].split('@')
    if len(uart_info) == 1:
        uart = uart_info[0]
        baud = 9600
    else:
        uart = uart_info[0]
        baud = int(uart_info[1])

    ip,port = sys.argv[2].split(':')
    port = int(port)

    print 'UART-TCP bridge started for %s <-> %s:%d' % (uart, ip, port)
    print 'Baud rate: %d' % baud
    print 'Press ^C to exit.'

    try:
        UartTcp(uart, ip, port, baud=baud).run()
    except KeyboardInterrupt:
        pass
