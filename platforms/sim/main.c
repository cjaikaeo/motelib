#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <sys/select.h>
#include <assert.h>

#include <motelib/sys/main.h>
#include <motelib/sys/storage.h>
#include "main.h"

bool _isBooted = 0;

void _send_to_sim(Command cmd, char *msg, uint8_t len)
{
    uint8_t buf[MAX_CMD_LEN];
    uint16_t total_len = len+2;

    // Message format
    //
    // +-----+-------+-----------------------+
    // | len |  cmd  |   data (optional)...  |
    // +-----+-------+-----------------------+
    //    1      1       0 - (MAX_CMD_LEN-2)

    memcpy(buf, &len, 1);
    buf[1] = cmd;
    memcpy(buf+2, msg, len);
    uint16_t actual = fwrite(buf, 1, total_len, stdout);
    assert(actual == total_len);
    fflush(stdout);
}

void _dispatch_msg(Command cmd, uint8_t *msg, uint8_t len)
{
    if (cmd == CMD_BOOT && !_isBooted)
    {
        memcpy(&_platform_address, msg, sizeof(Address));
        msg += sizeof(Address);
        memcpy(&_platform_panid  , msg, sizeof(uint16_t));
        msg += sizeof(uint16_t);
        memcpy(&_platform_channel, msg, sizeof(uint8_t));
        msg += sizeof(uint8_t);
        memcpy(&_platform_storage_size, msg, sizeof(StorageAddr));

        _isBooted = 1;
    }
    else if (_isBooted)
    {
        switch (cmd)
        {
            // radio send request received by sim
            case CMD_RADIO_SEND_ACK:
                _sys_radio_sendDone(msg[0]);
                break;
            // radio message received from sim
            case CMD_RADIO_RECV:
                _radio_receive(msg);
                break;
            // sim reports sensor read ready
            case CMD_SENSE:
                _sensorReady(msg[0], msg[1] + msg[2]*256);
                break;
            // sim requests for setting button status
            case CMD_SET_BUTTON:
                _buttonStatus(msg[0]);
                break;
            // sim requests for shutdown
            case CMD_SHUTDOWN:
                _reset();
                break;
            // receiving data from UART input
            case CMD_UART_INPUT:
                _uart_receive(msg, len);
                break;
            // receiving digital sensor settings
            case CMD_DIGITAL_INPUTS:
                _sensorSetDigital(msg[0] + msg[1]*256);
                break;
            // storage subsystem write ready
            case CMD_STORAGE_WRITE_DONE:
                _storage_write_done();
                break;
            // storage subsystem read done
            case CMD_STORAGE_READ_DONE:
                _storage_read_done(msg[0]);
                break;
        }
    }
}
 
int _recv_from_sim()
{
    uint8_t buf[MAX_CMD_LEN];
    uint8_t len;
    uint8_t type;
    int ret;
    fd_set readfds;
    struct timeval timeout;

    // Wait until there is something available from stdin or until timed out
    FD_SET(0, &readfds);
    timeout.tv_sec = 0;
    timeout.tv_usec = 1000;
    ret = select(1, &readfds, NULL, NULL, &timeout);

    if (ret > 0) // something is available from stdin
    {
        if (fread(buf, 2, 1, stdin) > 0)
        {
            len = buf[0];
            type = buf[1];
            if (len) fread(buf, len, 1, stdin);
            _dispatch_msg(type, buf, len);
            return 1;
        }
        else return 0; // the other end of the pipe is closed
    }
    else if (ret == 0) // timeout
        return 1;
    else // error, probably the pipe is closed
        return 0;
}

void _reset()
{
    _isBooted = 0;

    _initSensorModule();
    _initActorModule();
    _initButtonModule();
    _storage_init();

    // Wait until the system is booted by the simulator
    while (_recv_from_sim())
    {
        if (_isBooted) break;
    }

    // Initialize MoteLib's main system
    _sys_init_routine();
}

int printf_sim(char *fmt, ...)
{
    char buf[1024];
    int len;

    // gather variable-length argument list and pass it to vsprintf()
    va_list args;
    va_start(args, fmt);
    len = vsprintf(buf, fmt, args);
    va_end(args);

    uartWrite(buf, len);

    return len;
}

int main()
{
    _reset();

    while (_recv_from_sim())
    {
        if (!_isBooted) continue;
        _sys_main_routine();
        _uart_checkOutputBuffer();
        if (_timerFired)
        {
            pthread_mutex_lock(&_timerMutex);
            _timerFired = false;
            pthread_mutex_unlock(&_timerMutex);
            _sys_timer_tick();
        }
    }

    return 0;
}
