#include <string.h>
#include <stdarg.h>

#include "main.h"
#include <motelib/system.h>

#ifndef NDEBUG
void debug(char *fmt, ...)
{
    char buf[MAX_CMD_LEN];
    va_list args;
    va_start(args, fmt);
    vsprintf(buf, fmt, args);
    va_end(args);
    _send_to_sim(CMD_DEBUG, buf, strlen(buf));
}
#endif
