#include <stdbool.h>
#include <pthread.h>

#include "main.h"
#include <motelib/timer.h>
#include <motelib/sys/timer.h>

static void *_tickHandler(void *arg);
static pthread_t thrTimer = 0;
bool _timerFired;
pthread_mutex_t _timerMutex = PTHREAD_MUTEX_INITIALIZER;

void _platform_timer_init()
{
    // see if thread is still active from the last reboot
    if (thrTimer)
    {
        pthread_cancel(thrTimer); // kill it
        pthread_join(thrTimer, NULL); // make sure it's dead
    }

    _timerFired = false;
    pthread_create(&thrTimer, NULL, _tickHandler, NULL);
}

void *_tickHandler(void* arg)
{
    while (1)
    {
        usleep(1000*PLATFORM_TIMER_TICK_INTERVAL);
        if (_isBooted)
        {
            pthread_mutex_lock(&_timerMutex);
            _timerFired = true;
            pthread_mutex_unlock(&_timerMutex);
        }
    }
}
