#ifndef __MAIN_H__
#define __MAIN_H__

#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <pthread.h>

#include <motelib/system.h>
#include <motelib/led.h>
#include <motelib/timer.h>
#include <motelib/radio.h>
#include <motelib/sensor.h>
#include <motelib/button.h>
#include <motelib/sys/radio.h>

#define MAX_CMD_LEN 255

typedef void (*funcptr)();

typedef enum cmd_enum
{
    CMD_BOOT = 0,
    CMD_SHUTDOWN,
    CMD_DEBUG,
    CMD_RADIO_SEND,
    CMD_RADIO_SEND_ACK,
    CMD_RADIO_RECV,
    CMD_LED,
    CMD_SENSE,
    CMD_SET_ACTOR,
    CMD_SET_BUTTON,
    CMD_UART_OUTPUT,
    CMD_UART_INPUT,
    CMD_DIGITAL_INPUTS,
    CMD_STORAGE_READ_REQ,
    CMD_STORAGE_WRITE_REQ,
    CMD_STORAGE_READ_DONE,
    CMD_STORAGE_WRITE_DONE,
} Command;

void _send_to_sim(Command cmd, char *msg, uint8_t len);
int _recv_from_sim();
void _reset();

extern bool _isBooted;
extern bool _timerFired;
extern pthread_mutex_t _timerMutex;

/* System internal */
void _setAddress(Address addr);

/* Led internal */
void _initLedModule();

/* Timer internal */
void _initTimerModule();

/* Radio internal */
void _radio_receive(uint8_t *msg);

/* Sensor internal */
void _initSensorModule();
void _sensorReady(SensorType sensor, uint16_t value);

/* Actor internal */
void _initActorModule();

/* Button internal */
void _initButtonModule();
void _buttonStatus(ButtonStatus status);

/* Uart internal */
void _uart_checkOutputBuffer();

/* Storage internal */
void _storage_init();
void _storage_write_done();
void _storage_read_done(uint8_t data);

#endif
