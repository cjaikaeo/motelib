#include <avr/interrupt.h> 
#include "platform.h"
//#include "main.h"
#include <system.h>
#include <timer.h> 	
#include <main.h>
#include <led.h>
#include <actor.h>
#include <radio.h>
#include <sensor.h>
#include <util/delay.h>

//===============Declare Global Variable==============================================================

//===============ACTOR================================================================================
void ACTOR_Init();

//===============LED==================================================================================
void Led_Init();

//==============Timer=================================================================================
void Timer0_Init(void);
void chkTimer();

//==============Radio=================================================================================
void USART_Init(unsigned int baud);
void atCommandRequest(char *atcommand,char *parameters,uint16_t len);
void collectFrame();

//==============Sensor================================================================================
void Sensor_Init();
void readQueue();

//==============Function==============================================================================
#ifndef NDEBUG
void debug(char *fmt, ...)
{
}
#endif

//===============main=================================================================================
int main()
{
    _ready4Boot = 0;
    Timer0_Init();
    Led_Init();
    ACTOR_Init();
    Sensor_Init();
    USART_Init(77);  // baud = 9600
    char at[2];
    at[0]='M';at[1]='Y';
    uint32_t cnt = 0;
//=======================================================================================================
    while(1)
    {
        cnt++;
        if(_ready4Boot)
        {
            _ready4Boot = 0;
            boot();
            break;
        }
        else if(cnt > 100000)
        {
            cnt = 0;
            atCommandRequest(at,at,0x0000);
        }
    }
//=======================================================================================================
    while(1)
    {
        
        if(_interruptTimer)
        {
            chkTimer();
        }
        else if(_inQueue)
        {
            readQueue();
        }
    }
    return 0;
}

