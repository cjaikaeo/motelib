#include "actor.h"
#include "platform.h"
#include "avr/io.h"

void ACTOR_Init()
{
    DDRB = 0b0000111;
    PORTB = 0x00;
}

void setActorState(ActorType actor, uint8_t state)
{
    uint8_t shift = 2 - actor;
    if(state == 0)
        PORTB &= ~(1 << shift);
    else if(state == 1)
        PORTB |= 1 << shift;
}

uint8_t getActorState(ActorType actor)
{
    uint8_t shift = 2 - actor;
    return (PORTB & actor) >> shift;
}
