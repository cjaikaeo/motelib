#include <system.h>
#include <main.h>
#include "platform.h"
#include <sensor.h>
#include <led.h>
#include <avr/io.h> 
#include <avr/interrupt.h> 

typedef struct list
{
    SensorType sensor;
    SenseDone senseDone;
    struct list *next;
}Queue;

static uint8_t ready[8];
static uint8_t _ready;
static Queue queue[7];
static Queue *fs,*ls;
static SenseDone _senseDone;
static uint8_t sensorNo;

void Sensor_Init()
{
    ADMUX = 0x40; // Ext. vref
    ADCSRA = 0x8E; // ADC ena | intr ena, prescale 64    
    uint8_t i;
    for(i = 0;i < 8;i++)
        ready[i] = 1;
    _ready = 1;
    _inQueue = 0;
    for(i = 0;i < 7;i++)
    {
        if(i == 6)
        {
            queue[i].next = &queue[0];
            fs = &queue[0];
            ls = &queue[0];
        }
        else
        {
            queue[i].next = &queue[i+1];
        }
    }
}

void read_ADC(SensorType sensor, SenseDone senseDone)
{
    ready[sensor] = 0;
    _ready = 0;
    sensorNo = sensor;
    _senseDone = senseDone;
    ADMUX |= sensor; // bit 3..0 select channel 
    ADCSRA |= (1<<ADSC); // Start conversion !
}

void readQueue()
{
    Queue *tmp;
    _inQueue = 0;
    tmp = fs;
    fs = fs->next;
    read_ADC(tmp->sensor,tmp->senseDone);
}

Status sense(SensorType sensor, SenseDone senseDone)
{
    if(ready[sensor] == 1 && _ready == 1)
    {
        read_ADC(sensor, senseDone);
    }
    else if(ready[sensor] == 1 && _ready == 0)
    {
        if(ls->next == fs)
            return _ready;
        else
        {
            ls->sensor = sensor;
            ls->senseDone = senseDone;
            ls = ls->next;
        }            
    }
    return ready[sensor];
}

ISR(ADC_vect)
{
    uint16_t value = ADCL|(ADCH<<8);    

    ready[sensorNo] = 1;
    _senseDone(value);
    if(fs != ls)
        _inQueue = 1;
    _ready = 1;    
}

