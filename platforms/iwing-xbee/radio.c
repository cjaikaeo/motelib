#include <system.h>
#include <radio.h>
#include <led.h>
#include <main.h>

#include <avr/interrupt.h> 
#include <avr/io.h> 

static uint8_t _byte;
static uint8_t returnedStatus;
static uint8_t frameID;
static Address myAddr;
static SendDone _sendDone;
static uint8_t bufferIndex;
static uint8_t bufferLen;
static char _buffer[110];
static Receive _receive;

typedef enum FrameType_enum
{
    TX_REQUEST = 0x01,
    TX_STATUS = 0x89,
    AT_COMMAND = 0x08,
    AT_COMMAND_QUEUE = 0x09,
    AT_COMMAND_RESPONSE = 0x88,
    RX_PACKET = 0x81
}FrameType;

void USART_Init(unsigned int baud)
{
//set baud rate
    UBRR0L = (unsigned char) baud;
    UBRR0H = (unsigned char) (baud >> 8);
//enable receiver and transmitter
    UCSR0B = (1 << RXEN0)|(1 << TXEN0);
//enable TX complete and RX complete and global interrupt
    UCSR0B |= (1 << RXCIE0)|(1 << TXCIE0);
//set frame format : UCSZ2:0 = 011//8data, URSEL = 0, UMSEL = 0
    UCSR0C = (3 << UCSZ00);
    frameID = 0x77;
    returnedStatus = 0x00;
    myAddr = 0xFFFF;
    bufferIndex = 0;
    bufferLen = 0;
    _receive = NULL;
    sei();
}

void setAddress(Address address)
{
    myAddr = address;
    _ready4Boot = 1;
}

Address getAddress()
{
    return myAddr;
}

void _sendBuffer()
{
    UDR0 = _buffer[bufferIndex];                //send indexth byte of buffer.
    bufferIndex++;                              //increase index.
    if(bufferIndex >= bufferLen)                //clear buffer if buffer is empty.
    {
        bufferIndex = 0;
        bufferLen = 0;
        _buffer[0] = 0;
    }
}

void atCommandRequest(char *atcommand,char *parameters,uint16_t len)//len = length of parameters.
{
    uint16_t t_len = len + 4;                   //4 bytes=apiID:1byte + frameID:1byte + atcommand:2byte.
    uint8_t chksum = 0xFF - 0x08 - frameID - *atcommand - *(atcommand+1);     
    uint8_t countLen = 0;
    _buffer[countLen++] = 0x7E;                 //start frame
    _buffer[countLen++] = t_len >> 8;           //store MSB of length of frame to buffer
    _buffer[countLen++] = t_len & 0xFF;         //store LSB of length of frame to buffer
    _buffer[countLen++] = 0x08;                 //store API ID of frame to buffer
    _buffer[countLen++] = frameID;              //store frame ID to buffer
    _buffer[countLen++] = *atcommand;           //store first character of AT command to buffer
    _buffer[countLen++] = *(atcommand+1);       //store second character of AT command to buffer
    while(len-- > 0)    
    {
        chksum -= *parameters;
        _buffer[countLen++] = *parameters++;    //store each character of parameter to buffer
    }
    _buffer[countLen++] = chksum;               //store chaeck sum of frame to buffer
    bufferLen = countLen;
    _sendBuffer();                              //send first byte of buffer
    return;
}

Status send(Address dst,MessageType type,char *msg,uint16_t len,SendDone sendDone)
{    
    if(returnedStatus == 0x00){
        _sendDone = sendDone;
        uint8_t countLen = 0;
        uint16_t t_len = len + 6;               //5 bytes for API feilds and 1 byte for adding the message type with data.
        uint8_t chksum = 0xFF - 0x01 - frameID - (dst >> 8) - dst - type;
        _buffer[countLen++] = 0x7E;             //start frame
        _buffer[countLen++] = t_len >> 8;       //store MSB of length of frame to _buffer
        _buffer[countLen++] = t_len & 0xFF;     //store LSB of length of frame
        _buffer[countLen++] = 0x01;             //store APT ID to _buffer
        _buffer[countLen++] = frameID;          //store frame id to _buffer
        _buffer[countLen++] = dst >> 8;         //store MSB of destination address to _buffer
        _buffer[countLen++] = dst & 0xFF;       //store LSB of destination address to _buffer
        _buffer[countLen++] = 0x00;             //store option of frame to _buffer
        _buffer[countLen++] = type;             //store type of message to _buffer
        while(len-- > 0)
        {
            chksum -= *msg;
            _buffer[countLen++] = *msg++;       //store each character of msg to buffer
        }
        _buffer[countLen++] = chksum;           //store check sum of frame to buffer
        bufferLen = countLen;
        _sendBuffer();                          //send first byte of buffer
        return 1;
    }
    else
    {
        return 0;
    }
}

void chkFrame(uint16_t len, char *frame)
{
    uint16_t tmp = 0;
    uint8_t temp;
    temp = *frame++;
    if(temp == AT_COMMAND_RESPONSE)                 //AT_COMMAND_RESPONSE
    {
        *frame++;
        temp = *frame++;
        tmp = (temp << 8)|*frame++;
        *frame++;
        switch(tmp)
        case 0x4D59:                                //character of AT command is "MY"
            temp = *frame++;
            Address address = (temp << 8)|*frame++;
            setAddress(address);
//        break;
    }
    else if(temp == TX_STATUS)                      //TX_STATUS
    {
        *frame++;        
        temp = *frame;
        returnedStatus = temp;
        if(_sendDone != NULL)
        {
            if(temp == 0)
            {
                _sendDone(READY);
            }
            else
            {    
                _sendDone(NOT_READY);
            }
        }
    }
    else if(temp == RX_PACKET)                      //RX_PACKET
    {
        temp = *frame++;
        Address src = (temp << 8)|*frame++;
        frame+=2;
        uint8_t type = *frame++;
        len = len - 6;
        if(_receive != NULL)
            _receive(src,type,frame,len);         //Calls the event handler which implemented by users
    }
    return;
}

void collectFrame()
{
    uint8_t byte = _byte;
    static uint8_t    thirdByteCnt = 0x00;
    static uint16_t len = 0x00;
    static uint8_t chksum = 0x00;
    static char frame[105];
    static uint16_t cnt = 0x0000;
    if(byte == 0x7E && thirdByteCnt == 0x00)
    {
        thirdByteCnt = 0x01;
    }
    else if(thirdByteCnt == 0x01)
    {
        len = (byte << 8);
        thirdByteCnt++;
    }
    else if(thirdByteCnt == 0x02)
    {
        len |= byte;
        thirdByteCnt++;
    }
    else if(thirdByteCnt == 0x03)
    {
        if(cnt < len)
        {
            frame[cnt++] = byte;
            chksum += byte;
        }
        else
        {
            chksum += byte;
            if(chksum == 0xFF){
                chksum = 0x00;
                thirdByteCnt = 0x00;
                cnt = 0x0000;
                chkFrame(len, frame);

            }
            chksum = 0x00;
            thirdByteCnt = 0x00;
            cnt = 0x0000;
        }
    }
}

void setReceiveHandler(Receive receive)
{
    _receive = receive;
}

void getTxStatus(TxStatus* status)
{
	// TODO to be implemented
}

void getRxStatus(RxStatus* status)
{
	// TODO to be implemented
}

ISR(USART_RX_vect)
{
    _byte = UDR0;
    collectFrame();
}

ISR(USART_TX_vect)
{
    if(bufferIndex < bufferLen)
    {
        _sendBuffer();
    }
}

