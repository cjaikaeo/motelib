#include "system.h"
#include "settings.h"
#include "timer.h"
#include "main.h"
#include <avr/io.h> 
#include <avr/interrupt.h> 
#include <compat/deprecated.h>

typedef struct TimerBuf{
    uint16_t interval;
    TimerFired fired;
    TimerType type;
}TimerBuf;

static unsigned int counter[MAX_TIMERS];
static TimerBuf buf[MAX_TIMERS];
static uint8_t count;


void Timer0_Init(void)
{
    count = 0;
    TCCR0B = (0<<CS02)|(1<<CS01)|(1<<CS00);
// Timer(s)/Counter(s) Interrupt(s) initialization
    TIMSK0 = (1<<TOIE0);
//Initailize timer counter.
    TCNT0 = 86;
}

Timer createTimer()
{
    if (count++ >= MAX_TIMERS)
    {
        count = MAX_TIMERS;
        return INVALID_TIMER;
    }
    return count - 1;
}

void startTimer(Timer timer, TimerType type, uint16_t intv, TimerFired timerFired)
{
    if(timer != INVALID_TIMER)
    {
        buf[timer].type = type;
        buf[timer].interval = intv;
        buf[timer].fired = timerFired;
        counter[timer] = 0;
    }
    return;
}

void stopTimer(Timer timer)
{
    buf[timer].fired = NULL;
}

void chkTimer()
{
    uint8_t i;
    for(i = 0;i < count;i++){
        if(counter[i]++ > buf[i].interval && buf[i].fired != NULL)
        {
            TimerFired fired = buf[i].fired;
            counter[i] = 0;
//            if(buf[i].fired != NULL)
//                buf[i].fired();
            if(buf[i].type == ONESHOT)
                buf[i].fired = NULL;
	    if(fired != NULL)
                fired();
        }
    }
    _interruptTimer = 0;
    return;
}

ISR(TIMER0_OVF_vect)
{
    static uint8_t counter = 0;
    if((counter++)%2==0)
        TCNT0 = 69;
    else
        TCNT0 = 68;
    _interruptTimer = 1;
}
