#!/usr/bin/env python

import sys
import usb
import argparse

# USB VID/PID for mote while running bootloader
USB_ID = (0x16c0, 0x05dc)

# Request constants for USBasp
USBASP_FUNC_READEEPROM  = 7
USBASP_FUNC_WRITEEEPROM = 8

# EEPROM addresses storing mote parameters
# See ../platform.h
EEPROM_NODE_ADDR  = 0x10
EEPROM_PAN_ID     = 0x12
EEPROM_RF_CHANNEL = 0x14
EEPROM_HIGH_POWER_RF = 0x15
EEPROM_TX_POWER = 0x16

##################################################
class Mote:

    def __init__(self):
        self.board = None
        for bus in usb.busses():
            for dev in bus.devices:
                if (dev.idVendor,dev.idProduct) == USB_ID:
                    # TODO: Make sure the device is running USBasp
                    self.board = dev.open()
        if not self.board:
            raise Exception("Cannot find a mote.")

    ####################################
    def read_eeprom(self, addr, length):
        reqType = usb.TYPE_VENDOR | usb.RECIP_DEVICE | usb.ENDPOINT_IN
        return self.board.controlMsg(
                reqType, USBASP_FUNC_READEEPROM, length, value=addr)

    ####################################
    def write_eeprom(self, addr, values):
        reqType = usb.TYPE_VENDOR | usb.RECIP_DEVICE | usb.ENDPOINT_OUT
        return self.board.controlMsg(
                reqType, USBASP_FUNC_WRITEEEPROM, values, value=addr)

    ####################################
    def address(self, address=None):
        '''
        Get or set the node's address
        @param address if specified, indicates the node's new address
        '''
        if address is None:
            (low,hi) = self.read_eeprom(EEPROM_NODE_ADDR, 2)
            return hi*256 + low
        else:
            self.write_eeprom(EEPROM_NODE_ADDR, [address%256, address/256])

    ####################################
    def panid(self, panid=None):
        '''
        Get or set the node's PAN ID
        @param panid if specified, indicates the node's new PAN ID
        '''
        if panid is None:
            (low,hi) = self.read_eeprom(EEPROM_PAN_ID, 2)
            return hi*256 + low
        else:
            self.write_eeprom(EEPROM_PAN_ID, [panid%256, panid/256])

    ####################################
    def channel(self, channel=None):
        '''
        Get or set the node's RF channel
        @param channel if specified, indicates the node's new RF channel
        '''
        if channel is None:
            buf = self.read_eeprom(EEPROM_RF_CHANNEL, 1)
            return buf[0]
        else:
            self.write_eeprom(EEPROM_RF_CHANNEL, [channel])

    ####################################
    def hpflag(self, flag=None):
        '''
        Get or set the node's high-power flag
        @param flag if specified, 1 enables high-power module and other values
        disables high-power module.
        '''
        if flag is None:
            buf = self.read_eeprom(EEPROM_HIGH_POWER_RF, 1)
            return buf[0]
        else:
            if flag is not 1:
                flag = 0
            self.write_eeprom(EEPROM_HIGH_POWER_RF, [flag])

    ####################################
    def txpower(self, txpower=None):
        '''
        Get or set the node's transmission power
        @param txpower if specified, configure the mote's default transmission
        power level.  Valid range is from 0 (min power) to 31 (max power).
        Actual transmission power in dB can be computed by the method
        computeTxPower().
        '''
        if txpower is None:
            buf = self.read_eeprom(EEPROM_TX_POWER, 1)
            return buf[0]
        else:
            self.write_eeprom(EEPROM_TX_POWER, [txpower])

    ####################################
    def computeTxPower(self, txpower):
        '''
        Compute actual transmission power in dB from txpower level.
        '''
        if txpower > 31:
            return 'INVALID'
        txpower = 31 - txpower
        largeScale = txpower >> 3
        smallScale = txpower & 0x07

        # From MRF24J40 datasheet
        #  <5-4>: Large Scale Control for TX Power bits
        #      11 = -30 dB
        #      10 = -20 dB
        #      01 = -10 dB
        #      00 =   0 dB
        #  <3-0>: Small Scale Control for TX Power bits
        #      000 =    0 dB
        #      001 = -0.5 dB
        #      010 = -1.2 dB
        #      011 = -1.9 dB
        #      100 = -2.8 dB
        #      101 = -3.7 dB
        #      110 = -4.9 dB
        #      111 = -6.3 dB
        value = -[0,10,20,30][largeScale] -  \
                [0,0.5,1.2,1.9,2.8,3.7,4.9,6.3][smallScale]
        return '%.1f dB' % value

##################################################
if __name__ == '__main__':
    parser = argparse.ArgumentParser(
            description='Read or set Mote configurations.')
    parser.add_argument('--read',
            action='store_true',
            help='Read current Mote configuration')
    parser.add_argument('--address',
            dest='address',
            help='Set Mote address')
    parser.add_argument('--panid',
            dest='panid',
            help='Set Mote PAN ID')
    parser.add_argument('--channel',
            dest='channel',
            help='Set Mote RF channel')
    parser.add_argument('--high-power',
            dest='hpflag',
            help='Enable high-power module (0 - disable, 1 - enable)')
    parser.add_argument('--tx-power',
            dest='txpower',
            help='Set TX power for MRF24J40 (0 - min power, 31 - max power)')

    args = parser.parse_args()

    # arguments should all be valid up to this point,
    # so try to connect to a mote
    mote = Mote()

    modified = False

    if args.address is not None:
        mote.address(int(args.address, 0))
        modified = True

    if args.panid is not None:
        mote.panid(int(args.panid, 0))
        modified = True

    if args.channel is not None:
        mote.channel(int(args.channel, 0))
        modified = True

    if args.hpflag is not None:
        mote.hpflag(int(args.hpflag, 0))
        modified = True

    if args.txpower is not None:
        mote.txpower(int(args.txpower, 0))
        modified = True

    if not modified or args.read:
        address = mote.address()
        panid   = mote.panid()
        channel = mote.channel()
        hpf     = mote.hpflag()
        txpower = mote.txpower()
        print 'address: %d (0x%04x)' % (address, address)
        print 'panid: %d (0x%04x)' % (panid, panid)
        print 'channel: %d (0x%02x)' % (channel, channel)
        if hpf is 1:
            print 'high-power: ENABLED'
        else:
            print 'high-power: DISABLED'
        print 'tx-power: %d (%s)' % (txpower, mote.computeTxPower(txpower))

