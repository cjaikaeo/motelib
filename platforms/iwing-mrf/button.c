#include <avr/io.h>
#include <avr/interrupt.h> 

#include <motelib/system.h>
#include <motelib/button.h>

#include "main.h"

static ButtonHandler m_buttonHandler;

void Button_Init()
{
    m_buttonHandler = NULL;
    DDRD   &= ~(1 << PIND3); // make PD3 input
    PORTD  |= (1 << PIND3);  // enable pull-up resistor on PD3
    PCMSK2 |= (1<<PCINT19);  // enable pin-change 19 (PD3)
    PCICR  |= (1<<PCIE2);    // enable pin-change interrupt group 2 (PCINT23..16)
}

void buttonSetHandler(ButtonHandler handler)
{
    m_buttonHandler = handler;
}

bool buttonIsPressed()
{
    return bit_is_clear(PIND, PIND3);
}

void Button_PressedCallback()
{
    // Note: no need to check the handler for null value as it has been
    //       checked by the interrupt handler
    m_buttonHandler(BUTTON_PRESSED);
}

void Button_ReleasedCallback()
{
    // Note: no need to check the handler for null value as it has been
    //       checked by the interrupt handler
    m_buttonHandler(BUTTON_RELEASED);
}

ISR(PCINT2_vect)
{
    if (m_buttonHandler)
    {
        if (PIND & (1<<PIND3)) // button is released
        {
            FLAG_NONATOMIC(f_buttonReleased);
        }
        else
        {
            FLAG_NONATOMIC(f_buttonPressed);
        }
    }
}
