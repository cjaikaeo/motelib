#include <avr/io.h>

#include <motelib/led.h>
#include <motelib/sys/led.h>

void _platform_led_init()
{
    DDRD = 0b11100000;
    PORTD |= 0b11100000;
}

uint8_t ledGet(uint8_t no)
{
    uint8_t shift = 5 + no;
    return !((PORTD >> shift)&0x1);
}

void ledSet(uint8_t no, uint8_t status)
{
    uint8_t shift = 5 + no;
    if(status == 1)
        PORTD &= ~(1 << shift);
    else if(status == 0)
        PORTD |= 1 << shift;
}

void ledToggle(uint8_t no)
{
    uint8_t shift = 5 + no;
    PORTD ^= 1 << shift;
}
