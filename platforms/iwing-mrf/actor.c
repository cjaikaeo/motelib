#include <avr/io.h>

#include <motelib/actor.h>

#include "main.h"

void Actor_Init()
{
}

void actorSetState(ActorType actor, uint8_t state)
{
    // make the pin output
    SET_BIT(DDRC, actor);

    if (state) 
        SET_BIT(PORTC, actor);  // activate actor
    else
        CLR_BIT(PORTC, actor); // deactivate actor
}

uint8_t actorGetState(ActorType actor)
{
    return BIT_SET(PORTC, actor);
}
