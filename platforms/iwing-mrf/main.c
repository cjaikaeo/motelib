#include <stdio.h>
#include <string.h>

#include <avr/interrupt.h>
#include <avr/wdt.h>
#include <avr/io.h>

#include <motelib/system.h>
#include <motelib/timer.h> 	
#include <motelib/led.h>
#include <motelib/actor.h>
#include <motelib/radio.h>
#include <motelib/sensor.h>
#include <motelib/uart.h>
#include <motelib/sys/main.h>
#include <motelib/sys/led.h>
#include <motelib/sys/timer.h>
#include <motelib/sys/radio.h>

#include "platform.h"
#include "main.h"

//////////////////////////////////////////////////////////////////////
// Global variables
//////////////////////////////////////////////////////////////////////
volatile Flags _flags;
static int uart_putchar(char c, FILE *stream);

static FILE platformStdout = FDEV_SETUP_STREAM(uart_putchar, NULL, _FDEV_SETUP_WRITE);

/**
 * Check the flags shared by interrupt handlers in other modules and the main
 * loop and handle them accordingly.  This macro will reset the flag before
 * calling the handler.
 */
#define CHECK_FLAG_AND_RESPOND(flag,handler) \
    if (IS_FLAGGED(flag))    \
    {                        \
        UNFLAG_ATOMIC(flag); \
        handler();           \
    }

/**
 * Sends a character out via UART.
 */
static int uart_putchar(char c, FILE *stream)
{
    uartWriteByte(c);
    if (c == '\n') uartWriteByte('\r');
    return 0;
}

//===============main=====================================================
int main()
{
    // Clear all flags
    memset((void*)&_flags, 0, sizeof(_flags));

    // Initialize all modules
    Actor_Init();
    Sensor_Init();
    Button_Init();

    // Enable watchdog
    wdt_enable(WDTO_1S);

    // Hook stdout/stderr to UART
    stdout = &platformStdout;
    stderr = &platformStdout;

    // Call MoteLib's system intialization
    _sys_init_routine();

    // Enable global interrupt
    sei();

    // Enter the forever main loop
    while(1)
    {
        // Call MoteLib's main routine
        _sys_main_routine();

        //
        // Platform-specific routine
        //
        CHECK_FLAG_AND_RESPOND(f_timerInterrupt,    _sys_timer_tick);
        CHECK_FLAG_AND_RESPOND(f_sensorDataReady,   Sensor_Callback);
        CHECK_FLAG_AND_RESPOND(f_buttonPressed,     Button_PressedCallback);
        CHECK_FLAG_AND_RESPOND(f_buttonReleased,    Button_ReleasedCallback);
        CHECK_FLAG_AND_RESPOND(f_radioInterrupt,    Radio_CheckInterrupt);
        CHECK_FLAG_AND_RESPOND(f_radioTxFifoLoaded, Radio_StartTx);
#ifndef UART_VIA_USB
        CHECK_FLAG_AND_RESPOND(f_uartReadDone,      _uart_readDone);
#endif
        _uart_poll();  // check and send data in UART output buffer

        wdt_reset(); // Reset watchdog timer
    }
    return 0;
}

