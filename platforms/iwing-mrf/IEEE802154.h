#ifndef __IEEE802154_H__
#define __IEEE802154_H__

enum
{
  IEEE154_TYPE_BEACON  = 0,
  IEEE154_TYPE_DATA    = 1,
  IEEE154_TYPE_ACK     = 2,
  IEEE154_TYPE_MAC_CMD = 3,
};

enum 
{
  IEEE154_ADDR_NONE  = 0,
  IEEE154_ADDR_SHORT = 2,
  IEEE154_ADDR_EXT   = 3,
};

typedef union
{
    uint8_t flat;
    struct 
    {
		uint8_t frame_type : 3;
		uint8_t security_enable : 1;
		uint8_t frame_pending : 1;
		uint8_t ack_request : 1;
		uint8_t intrapan : 1;
		uint8_t reserved : 3;
		uint8_t dst_addr_mode : 2;
		uint8_t frame_version : 2;
		uint8_t src_addr_mode : 2;
    } bits;
} IEEE154_FCF;

#endif
