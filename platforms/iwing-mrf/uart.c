#include <stdio.h>

#define BAUD UART_BAUD

#include <motelib/uart.h>
#include <motelib/cirbuf.h>
#include <motelib/sys/uart.h>

#include <avr/io.h>
#include <util/setbaud.h>


#include "main.h"

#define PRIVATE_RX_BUF_SIZE 8

// One byte is preserved for distinguishing empty buffer from full buffer
static char rxBuf[PRIVATE_RX_BUF_SIZE];
static uint8_t rxBufHead, rxBufTail;

#define ADVANCE_RXBUF_HEAD  rxBufHead = (rxBufHead+1)%PRIVATE_RX_BUF_SIZE
#define ADVANCE_RXBUF_TAIL  rxBufTail = (rxBufTail+1)%PRIVATE_RX_BUF_SIZE
#define RXBUF_EMPTY         (rxBufHead == rxBufTail)
#define RXBUF_FULL          ((rxBufTail+1)%PRIVATE_RX_BUF_SIZE == rxBufHead)

/**
 * Initializes UART module with the specified baud rate BAUD
 */
void _platform_uart_init()
{
    UBRR0H = UBRRH_VALUE;
    UBRR0L = UBRRL_VALUE;
    UCSR0C = (3 << UCSZ00);  // 8-bit, no parity, 1-stop

    rxBufHead = rxBufTail = 0;
}

////////////////////////////////////////////////////////
void _platform_uart_enable(bool tx, bool rx)
{
    if (tx)
        // Enable TX module
        SET_BIT(UCSR0B, TXEN0);
    else
        // Disable TX module
        CLR_BIT(UCSR0B, TXEN0);

    if (rx)
    {
        // Enable RX module and RX interrupt
        SET_BIT(UCSR0B, RXEN0);
        SET_BIT(UCSR0B, RXCIE0);
    }
    else
    {
        // Disable RX module and RX interrupt
        CLR_BIT(UCSR0B, RXEN0);
        CLR_BIT(UCSR0B, RXCIE0);
    }
}

////////////////////////////////////////////////////////
void _uart_poll()
{
    // Make sure there is data to be written and the write register is
    // ready
    if (!cirbufEmpty(&_sys_uart_cb_out) && bit_is_set(UCSR0A, UDRE0))
    {
        UDR0 = cirbufReadByte(&_sys_uart_cb_out);
    }
}

////////////////////////////////////////////////////////
void _uart_readDone()
{
    // Append recently read bytes into the buffer
    while (!RXBUF_EMPTY)
    {
        // If the buffer is already full, remove the oldest byte
        if (cirbufFull(&_sys_uart_cb_in))
            cirbufReadByte(&_sys_uart_cb_in);

        cirbufWriteByte(&_sys_uart_cb_in, rxBuf[rxBufHead]);

        // Notify sys if marker watch is in progress and a specified marker is
        // encounter
        if (_sys_uart_useMarker && (rxBuf[rxBufHead] == _sys_uart_marker))
            _sys_uart_notifyMarkerFound();

        ADVANCE_RXBUF_HEAD;
    }
}

////////////////////////////////////////////////////////
ISR(USART_RX_vect)
{
    char rxByte;

    // read byte from rx register 
    rxByte = UDR0;

    // store rx byte it in private rx buffer, if there is room
    if (!RXBUF_FULL)
    {
        rxBuf[rxBufTail] = rxByte;
        ADVANCE_RXBUF_TAIL;
    }

    // Inform the main loop
    FLAG_NONATOMIC(f_uartReadDone);
}
