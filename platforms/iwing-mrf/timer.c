#include <avr/io.h> 
#include <avr/interrupt.h> 
#include <compat/deprecated.h>

#include <motelib/sys/timer.h>

#include "main.h"

void _platform_timer_init()
{
    // Clock source = ClkIO/1024
    TCCR0B = (1<<CS02)|(0<<CS01)|(1<<CS00);

    // Enable Timer0 overflow interrupt
    TIMSK0 = (1<<TOIE0);

    // Start counting with desired overflow interval
    TCNT0 = 69;
}

ISR(TIMER0_OVF_vect)
{
    static uint8_t leap = 0;

    // The desired initial value for TCNT0 to generate a required overflow
    // interval must be:
    //  - 162.25 for 8 ms
    //  - 68.5 for 16 ms

    TCNT0 = 68 + leap;
    leap ^= 1; // leap every 2 ticks

    FLAG_NONATOMIC(f_timerInterrupt);
}
