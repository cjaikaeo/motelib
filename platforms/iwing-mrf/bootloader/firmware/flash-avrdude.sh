#!/bin/bash

read -p "Please put SLOW CLK jumper on the programming board..."
avrdude -c usbasp -p atmega328p -U hfuse:w:0xd8:m -U lfuse:w:0xdf:m -U efuse:w:0x06:m
avrdude -c usbasp -p atmega328p -U lock:w:0x3f:m

read -p "Please remove SLOW CLK jumper from the programming board..."
avrdude -c usbasp -p atmega328p -U flash:w:main.hex:i
