#ifndef __PLATFORM_H__
#define __PLATFORM_H__

#include <stdint.h>

#define PLATFORM_IWING_MRF

// Maximun length of message
#define PLATFORM_MAX_MSG_LEN   100  

// Timer tick length in milliseconds
#define PLATFORM_TIMER_TICK_INTERVAL  16

// SPI clock divider for MCU<->RF interface
#ifndef MRF24_SCLK_DIV
#define MRF24_SCLK_DIV 4
#endif

typedef enum
{
    SENSOR_0 = 0,
    SENSOR_1,
    SENSOR_2,
    SENSOR_3,
    SENSOR_4,
    SENSOR_5,
    SENSOR_INT_TEMP = 8, ///< Internal temperature
    SENSOR_VCC = 14      ///< Supply voltage
} SensorType;

typedef enum
{
    ACTOR_0 = 0,
    ACTOR_1,
    ACTOR_2,
    ACTOR_3,
    ACTOR_4,
    ACTOR_5
} ActorType;

typedef struct
{
    uint8_t num_retries:2;
    uint8_t cca_fail:1;
} RadioTxStatus;

typedef struct
{
    uint8_t lqi;
    uint8_t rssi;
} RadioRxStatus;


#endif
