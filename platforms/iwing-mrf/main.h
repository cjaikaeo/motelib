#ifndef __MAIN_H__
#define __MAIN_H__

#include <avr/io.h>
#include <util/atomic.h>

#define SET_BIT(port, bit)  ((port) |= _BV(bit))
#define CLR_BIT(port, bit)  ((port) &= ~_BV(bit))
#define FLIP_BIT(port, bit) ((port) ^= _BV(bit))
#define BIT_SET(port, bit)  (((port) & _BV(bit)) != 0)

#define LO_BYTE(word)  (word & 0xFF)
#define HI_BYTE(word)  (((uint16_t)word) >> 8)

/**
 * EEPROM location storing default node address
 */
#define EEPROM_NODE_ADDR  ((const uint16_t*) 0x10)

/**
 * EEPROM location storing default PAN ID
 */
#define EEPROM_PAN_ID     ((const uint16_t*) 0x12)

/**
 * EEPROM location storing default RF channel
 */
#define EEPROM_RF_CHANNEL ((const uint8_t*)  0x14)

/**
 * EEPROM location storing high-power RF flag
 */
#define EEPROM_HIGH_POWER_RF ((const uint8_t*)  0x15)

/**
 * EEPROM location storing transmission power
 */
#define EEPROM_TX_POWER ((const uint8_t*)  0x16)

/**
 * @typdef
 * Defines flags used by various modules to inform the main loop
 */
typedef struct
{
    uint8_t f_timerInterrupt  : 1;
    uint8_t f_sensorDataReady : 1;
    uint8_t f_buttonPressed   : 1;
    uint8_t f_buttonReleased  : 1;
    uint8_t f_radioInterrupt  : 1;
    uint8_t f_radioTxFifoLoaded : 1;
    uint8_t f_uartReadDone  : 1;
} Flags;

extern volatile Flags _flags;

/**
 * Macro to atomically set the specified flag in the shared flag set.
 */
#define FLAG_ATOMIC(x)  {ATOMIC_BLOCK(ATOMIC_FORCEON){ _flags.x = 1; }}

/**
 * Macro to atomically clear the specified flag in the shared flag set.
 */
#define UNFLAG_ATOMIC(x)  {ATOMIC_BLOCK(ATOMIC_FORCEON){ _flags.x = 0; }}

/**
 * Macro to non-atomically set the specified flag in the shared flag set.
 * This macro is supposed to be called within an ISR.
 */
#define FLAG_NONATOMIC(x)  { _flags.x = 1; }

/**
 * Macro to non-atomically clear the specified flag in the shared flag set.
 * This macro is supposed to be called within an ISR.
 */
#define UNFLAG_NONATOMIC(x)  { _flags.x = 0; }


/**
 * Macro to check the status of the specified shared flag.
 *
 * @todo Investigate if this should be wrapped in an atomic block
 */
#define IS_FLAGGED(x) (_flags.x)

//////////////////////////////////////////////////////////////////////
// Function prototypes
//////////////////////////////////////////////////////////////////////
void Uart_Init();
void Uart_ReadDone();
void Actor_Init();
void Led_Init();
void Button_Init();
void Button_PressedCallback();
void Button_ReleasedCallback();
void Radio_CheckInterrupt();
void Radio_StartTx();
void Sensor_Init();
void Sensor_ReadQueue();
void Sensor_Callback();
void _uart_readDone();
void _uart_poll();

#endif
