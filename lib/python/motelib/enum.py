###########################################################
class Enum(object):
    '''
    Defines and allows creation of enumerated types.

    Example

    >>> RequestType = Enum('OPEN', ('CLOSE',20), 'PAUSE')
    >>> RequestType.OPEN
    0
    >>> RequestType.CLOSE
    20
    >>> RequestType.PAUSE
    21
    '''

    ############################
    def __init__(self, *names):
        '''
        The class constructor.

        @param names Variable-length string arguments.  Each of which becomes
        an attribute containing a unique value for this Enum object.
        '''
        self._count = 0
        self._add(names)

    ############################
    def _add(self, names):
        for x in names:
            if type(x) == tuple:
                x,self._count = x
            setattr(self, x, self._count)
            self._count += 1


