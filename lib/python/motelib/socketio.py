import socket

###########################################################
class LostConnectionException(Exception):
    '''
    Exception raised when a connection is lost or terminated
    '''
    pass

##############################################
class SocketIO(object):
    '''
    Emulate file-like object from a socket connection
    '''
    def __init__(self, conn):
        '''
        Class constructor

        @param conn
        Can be a string of the form <addr>:<port> or an already established
        stream socket connection
        '''
        if type(conn) == str:
            addr,port = conn.split(':')
            port = int(port)
            self.conn = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self.conn.connect((addr, port))
        else:
            self.conn = conn

    def read(self, size=1):
        '''
        Attempt to read and return <size> bytes from socket.  If timed out, it
        returns whatever it has collected.

        In case connection is lost, LostConnectionException will be thrown
        '''
        pieceList = []
        while size > 0:
            try:
                # in case of synchronization loss and timed out, return
                # whatever we have been collecting
                piece = self.conn.recv(size)
            except socket.timeout, e:
                break

            if len(piece) == 0: # the other end has terminated connection
                raise LostConnectionException()

            size -= len(piece)
            pieceList.append(piece)
        return ''.join(pieceList)

    def write(self, data):
        '''
        Send <data> to the socket
        '''
        self.conn.send(data)

    def getTimeout(self, timeout):
        '''
        Get timeout for read/write operation in floating seconds.
        '''
        return self.conn.gettimeout()

    def setTimeout(self, timeout):
        '''
        Set timeout for read/write operation in floating seconds.  If
        <timeout> is None, these operations will block until they succeed
        '''
        self.conn.settimeout(timeout)

