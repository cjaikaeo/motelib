'''
@package XbeeGateway

Provides a Gateway class for accessing XBee device as a WSN gateway.
'''
import serial
from sys import stdout, argv
from threading import Thread
from time import sleep
from base import BaseGateway


###########################################################
class XbeeGateway(BaseGateway) :
    '''
    Defines a generic class for accessing XBee device as a WSN gateway.
    '''

    ############################
    def __init__(self, device, baudrate=9600, **kwargs):
        '''
        Instantiates a Gateway object that connects to the specified device.
        
        @param device 
        Specifies the device (e.g., /dev/ttyUSB0) to which an XBee module is
        attached

        @param baudrate
        Optionally specifies the baud rate to communicate with an XBee

        @param recvHistorySize
        Optionally specifies the number of recorded (source,frame-id) pairs to
        check for duplicates
        '''
        BaseGateway.__init__(**kwargs)

        self.serial = serial.Serial(device, baudrate=baudrate)
        self.serial.open()
        self.serial.flushInput()
        self.address = 0
        
        at = [0x7e, 0, 0x04, 0x08, 0x01, ord('M'), ord('Y')]
        checksum = 0xFF - (sum(at[3:])%256)
        at.append(checksum)
        if type(at) is list:
            at = ''.join([chr(x) for x in at])
        self.serial.write(at)
        
        if autoListen:
            self.listen_thread = Thread(target=self.listen)
            self.listen_thread.setDaemon(True)
            self.listen_thread.start()

        # Wait a moment so that XBee's address is successfully obtained before
        # returning
        sleep(.1)

    ############################
    def sendATCommand(self, cmd):
        '''
        Send AT command to XBee module via API mode

        @param cmd two-character AT command
        '''
        at = '\x7e\x00\x04\x08\x01' + cmd
        checksum = 0xFF - sum([ord(x) for x in at[3:]])%256
        at += chr(checksum)
        self.serial.write(at)

    ############################
    def sendATCommand(self, cmd):
        '''
        Send AT command to XBee module via API mode

        @param cmd two-character AT command
        '''
        at = '\x7e\x00\x04\x08\x01' + cmd
        checksum = 0xFF - sum([ord(x) for x in at[3:]])%256
        at += chr(checksum)
        self.serial.write(at)

    ############################
    def receiveAPIFrame(self):
        pass

    ############################
    def getAddress(self):
        '''
        Returns this gateway's address

        @return 16-bit integer representing this gateway's address
        '''
        return self.address

    ############################
    def getPanId(self):
        '''
        Returns PAN ID that this gateway is in
        '''
        return self.panid

    ############################
    def getChannel(self):
        '''
        Returns the current channel the gateway is using
        '''
        return self.channel

    ############################
    def send(self, dest, msg):
        '''
        Sends a message with specified type.

        @param dest 
        Indicates destination to which the message is destined.  If the value
        is BROADCAST_ADDR or 0xFFFF, the message is sent in broadcast mode.

        @param msg
        Contains a string representing the message to be sent
        '''
        if type(msg) is str:
            msg = [ord(x) for x in msg]
        length = len(msg)+6
        api_id = 0x01
        option = 0x01
        frame = [
                0x7e,
                length/256,
                length%256,
                api_id,
                self.frame_id,
                dest/256,
                dest%256,
                option,
                msgType,
                self.frame_id]
        frame += msg
        checksum = 0xFF - (sum(frame[3:])%256)
        frame.append(checksum)
        if type(frame) is list:
            frame = ''.join([chr(x) for x in frame])
        self.serial.write(frame)
        self.frame_id += 1

    ############################
    def receive(self, source, msgType, msg):
        '''
        Gets called when a message is received from radio.  This method
        should be overridden to perform appropriate actions.

        @param source
        Contains the address of the message's source

        @param msgType
        Contains the 8-bit type value

        @param msg
        Points to a string containing the message
        '''
        pass

    ############################
    def listen(self):
        '''
        Enters an infinite loop that keeps listening to the radio channel for
        incoming messages.  The receive() method is called upon message arrival.
        '''
        while True:
            c = self.serial.read()
            if ord(c) == 0x7e: # start frame delis = serial.Serial(device)miter
                length   = ord(self.serial.read())*256 + ord(self.serial.read())
                api_id   = ord(self.serial.read())
                if api_id == 0x88:
                    self.serial.read(4)
                    self.address = ord(self.serial.read())*256 + ord(self.serial.read())
                    #print '%s' % self.address
                elif api_id == 0x81:
                    src      = ord(self.serial.read())*256 + ord(self.serial.read())
                    #print 'src: %s' % src                    
                    self.serial.read(2)
                    # TODO: incorporate auto-generated code to interpret
                    # MoteLib's message header from $(MOTELIB)/lib/sys/radio.h
                    msgType  = ord(self.serial.read())
                    #print 'msgType : %d' % msgType
                    frame_id = ord(self.serial.read())
                    msg      = ''.join([x for x in self.serial.read(length-7)])
                    checksum = self.serial.read()
                    #print 'msg: [1]%x [2]%x [3]%x [4]%x [5]%x' % (ord(msg[0]),ord(msg[1]),ord(msg[2]),ord(msg[3]),ord(msg[4]))
                    #print 'chksum : %d' % ord(checksum)

                    # Make sure this is not a duplicate before reporting
                    # reception
                    if (src,frame_id) not in self.recvHistoryList:
                        self.recvHistoryList.append((src,frame_id))
                        if len(self.recvHistoryList) > self.recvHistorySize:
                            self.recvHistoryList.pop()
                        self.receive(src, msgType, msg)

    ############################

###########################################################
# @cond
if __name__ == '__main__':
    device = '/dev/ttyUSB0'
    if len(argv) == 2:
        device = argv[1]
    g = Gateway(device=device)
    print 'Test sending to broadcast address'
    for i in range(3):
        print 'Sending msg#%d' % i
        g.send(BROADCAST_ADDR, 1, 'goodbye')
    while True:
        c = raw_input()
        if c is 'q':
            break
# @endcond
