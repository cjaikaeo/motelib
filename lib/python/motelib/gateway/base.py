'''
@package GenericGateway

Provides a base class for various gateway implementation
'''

from threading import Thread
from struct import Struct

## Defines a broadcast address
BROADCAST_ADDR = 0xFFFF

###########################################################
class BaseGateway(object):
    '''
    Defines a base class for various gateway implementation
    '''
    ############################
    def __init__(self, recvHistorySize=20):
        '''
        Initializes a base gateway object
        
        @param recvHistorySize
        Optionally specifies the number of recorded (source,frame-id) pairs to
        check for duplicates
        '''
        self.frame_id = 0x01
        self.recvHistoryList = []
        self.recvHistorySize = recvHistorySize

    ############################
    def send(self, dest, msgType, msg):
        '''
        Sends a message with specified type.

        @param dest 
        Indicates destination to which the message is destined.  If the value
        is BROADCAST_ADDR or 0xFFFF, the message is sent in broadcast mode.

        @param msgType
        Specifies the 8-bit type value of the message.

        @param msg
        Contains the message to be sent, which can be either a string or a
        list of 8-bit integers.
        '''
        self._platform_send(dest, msg)
