#include <motelib/system.h>
#include <motelib/sys/main.h>
#include <motelib/sys/radio.h>
#include <motelib/sys/uart.h>

Address  _platform_address;
uint16_t _platform_panid;
uint8_t  _platform_channel;

static LoopRoutinePtr m_loopRoutinePtr;

//////////////////////////////////////////////////
void setLoopRoutine(LoopRoutinePtr proc)
{
    m_loopRoutinePtr = proc;
}

//////////////////////////////////////////////////
Address getAddress()
{
    return _platform_address;
}

//////////////////////////////////////////////////
uint16_t getPanId()
{
    return _platform_panid;
}

//////////////////////////////////////////////////
uint8_t getChannel()
{
    return _platform_channel;
}

/**
 * To be called by platform during initialization
 */
void _sys_init_routine()
{
    m_loopRoutinePtr = NULL;

    _sys_timer_init();
    _sys_led_init();
    _sys_radio_init();
    _sys_uart_init();

    // Invoke application boot routine
    boot();
}

/**
 * To be called by platform during main loop
 */
void _sys_main_routine()
{
    _sys_radio_pollTxQueue();
	_sys_uart_pollInput();

    // Call application-specific loop routine
    if (m_loopRoutinePtr != NULL)
        m_loopRoutinePtr();
}
