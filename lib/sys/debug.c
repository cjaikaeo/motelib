#include <stdio.h>
#include <stdarg.h>

#ifndef NDEBUG
void debug(char *fmt, ...)
{
    // gather variable-length argument list and pass it to vfprintf()
    va_list args;
    va_start(args, fmt);
    vfprintf(stderr, fmt, args);
    va_end(args);
}
#endif

