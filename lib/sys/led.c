#include <motelib/system.h>
#include <motelib/led.h>
#include <motelib/sys/led.h>

void _sys_led_init()
{
    _platform_led_init();
    ledSetValue(0);
}

void ledSetValue(uint8_t val)
{
    ledSet(0, val & 0x01);
    ledSet(1, (val & 0x02)>>1);
    ledSet(2, (val & 0x04)>>2);
}
