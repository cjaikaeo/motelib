#include <motelib/system.h>
#include <motelib/timer.h>
#include <motelib/sys/timer.h>
#include <platform.h>

static Timer *firstTimer; ///< Points to the first timer handler
static Timer *lastTimer; ///< Points to the last timer handler
static uint16_t ticks;  ///< Maintains 16-bit tick count in ms

/**
 * Initializes timer subsystem
 */
void _sys_timer_init()
{
	firstTimer = NULL;
	lastTimer = NULL;
    _platform_timer_init();
}

/**
 * Called by platform when one tick has passed
 */
void _sys_timer_tick()
{
    Timer *t;
    for (t = firstTimer; t != NULL; t = t->next)
    {
        if (t->remained >= PLATFORM_TIMER_TICK_INTERVAL)
            t->remained -= PLATFORM_TIMER_TICK_INTERVAL;

		// check if this timer has expired
        if (t->remained < PLATFORM_TIMER_TICK_INTERVAL)
        {
            TimerFired fired = t->fired;
            if (t->type == TIMER_ONESHOT)
            {
                t->fired = NULL;
                t->remained = 0;
            }
            else
            {
                t->remained += t->interval;
            }
            if (fired) fired(t);
        }
    }
    ticks += PLATFORM_TIMER_TICK_INTERVAL;
}

/**
 * Timer API Implementation
 */
void timerCreate(Timer *timer)
{
	// Put new timer at the end of the linked list
	if (lastTimer != NULL)
		lastTimer->next = timer;
	timer->next = NULL;
	lastTimer = timer;

	// If this is the first timer, point the first timer pointer to this one
	if (firstTimer == NULL)
		firstTimer = timer;
}

/**
 * Timer API Implementation
 */
void timerStart(Timer *timer, TimerType type, uint16_t interval, TimerFired timerFired)
{
    if (interval == 0) return; // do nothing for invalid interval

	timer->type     = type;
	timer->interval = interval;
	timer->fired    = timerFired;
	timer->remained = timer->interval;
}

/**
 * Timer API Implementation
 */
void timerStop(Timer *timer)
{
	timer->fired = NULL;
}

/**
 * Timer API Implementation
 */
uint16_t timerTicks()
{
    return ticks;
}
