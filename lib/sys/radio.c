#include <string.h>

#include <motelib/system.h>
#include <motelib/radio.h>
#include <motelib/cirbuf.h>
#include <motelib/sys/radio.h>

#include <platform.h>

//////////////////////////////////////////////////////////////////////
// Global variables
//////////////////////////////////////////////////////////////////////
static ReceiveInfo m_recvHistory[RADIO_RX_HISTORY_SIZE];
static uint8_t m_prevHistIndex; ///< Index of the previously seen message
static uint8_t m_histIndex;     ///< Index of the next available history slot
static uint8_t  m_currentFrameId; ///< ID of the next frame to be transmitted
static RadioRxHandler m_receiveFnPtr;  ///< Receive handler function
static RadioTxDone m_sendDoneFnPtr; ///< Function to get notified when send is done
static RadioStackStatus m_status;

RadioRxStatus _platform_radio_rxStatus;
RadioTxStatus _platform_radio_txStatus;

// Structure of TX buf record
// +-----+------+--------+-----+--------+
// | Dst | Type | TxDone | Len | Msg... |
// +-----+------+--------+-----+--------+
static CirBuf m_txbuf;  ///< Circular buffer storing tx data
static char   txbuf[RADIO_TX_BUF_SIZE];

//////////////////////////////////////////////////////////////////////
// Function Prototypes
//////////////////////////////////////////////////////////////////////
static void initReceiveHistory();

//////////////////////////////////////////////////////////////////////
// Implementation
//////////////////////////////////////////////////////////////////////

/**
 * Initializes radio subsystem.  Called by main module before entering the
 * main loop.
 */
void _sys_radio_init()
{
    memset((void*)&m_status, 0, sizeof(m_status));

    // Initialize global vars.
    m_currentFrameId = 0x01;
    m_receiveFnPtr = NULL;

    cirbufInit(&m_txbuf, txbuf, RADIO_TX_BUF_SIZE);

    initReceiveHistory();

    // platform-specific initialization
    _platform_radio_init();
}

/**
 * Initialize data structures and variables that keep history of received
 * frames for duplicate checking
 */
static void initReceiveHistory()
{
    uint8_t i;
    for (i = 0; i < RADIO_RX_HISTORY_SIZE; i++)
    {
        m_recvHistory[i].source = BROADCAST_ADDR;
        m_recvHistory[i].frameId = 0;
    }
    m_prevHistIndex = 0xFF;
    m_histIndex = 0;
}

/**
 * Records frame information and checks for duplicates.
 * @param src Address of the sender of this frame
 * @param frameId Frame ID (i.e., sequence number) of this frame
 * @retval 0 This frame is a duplicate
 * @retval 1 This frame is new
 */
uint8_t _radio_recordAndCheckDuplicate(Address src, uint8_t frameId)
{
    uint8_t newSource = 0, newFrame = 0;

    // Check if a frame with the same source and ID has been previously seen
    if (m_prevHistIndex == 0xFF) // history is empty
        newSource = 1;
    else
    {
        // See if this frame comes from the same source as previous
        if (m_recvHistory[m_prevHistIndex].source != src)
        {
            // If not, scan the history to see if this source has been seen
            newSource = 1;
            for (m_prevHistIndex = 0;
                 m_prevHistIndex < RADIO_RX_HISTORY_SIZE;
                 m_prevHistIndex++)
            {
                if (m_recvHistory[m_prevHistIndex].source == src)
                {
                    // Seen this source before
                    newSource = 0;
                    break;
                }
            }
        }
        else
            newSource = 0;

        // At this point if the source has been seen, m_prevHistIndex must
        // already point to the corresponding history slot, so check that slot
        // to see if the frame is new
        if (!newSource)
            newFrame = (m_recvHistory[m_prevHistIndex].frameId != frameId);
    }

    if (newSource)
    {
        m_recvHistory[m_histIndex].source = src;
        m_prevHistIndex = m_histIndex;
        m_histIndex = (m_histIndex+1) % RADIO_RX_HISTORY_SIZE;
    }
    m_recvHistory[m_prevHistIndex].frameId = frameId;

    return (newSource || newFrame);
}

/**
 * Accepts message from platform and reports to the event handler implemented
 * by users
 * @param src Address of source node.
 * @param frameId Sequence number found in the frame header (may not be
 *                available in some platforms)
 * @param msg Address of the first byte of received message
 * @param len Length of received message. 
 */
void _sys_radio_receiveDone(Address src, void* msg, uint8_t len)
{
    MessageHeader hdr;
    uint8_t i, checksum;

    // TODO:
    // for interoperability, we should adaptively determine the existence of
    // this field by matching the field's value against TinyOS's 6LOWPAN
    // Network Id

    // Extract MoteLib's headers
    memcpy(&hdr, msg, sizeof(MessageHeader));

    msg += sizeof(MessageHeader);
    len -= sizeof(MessageHeader)+1;

    // verify checksum
    checksum = hdr.type;
    for (i = 0; i < len; i++)
    {
        checksum += ((uint8_t*)msg)[i];
    }
    checksum += ((uint8_t*)msg)[len];

    // reject message with wrong checksum
    if (checksum != 0) return;

    // check for duplicates and ignore them
    if (!_radio_recordAndCheckDuplicate(src, hdr.seqno))
        return;

    // Calls the event handler implemented by users while skipping MoteLib's
    // header
    if (m_receiveFnPtr != NULL)
    {
        m_receiveFnPtr(src, hdr.type, msg, len);
    }
}

/**
 * Receives status from platform about the most recent transmission
 * @param status Status of the most recent transmission
 */
void _sys_radio_sendDone(RadioStatus status)
{
    m_status.sending = 0;

    //Calls the event handler implemented by users
    if (m_sendDoneFnPtr != NULL) m_sendDoneFnPtr(status);
}

/**
 * (API function)
 * Implements Radio API's <code>radioGetTxStatus()</code>
 */
void radioGetTxStatus(RadioTxStatus* status)
{
    memcpy(status, &_platform_radio_txStatus, sizeof(RadioTxStatus));
}

/**
 * (API function)
 * Implements Radio API's <code>radioGetRxStatus()</code>
 */
void radioGetRxStatus(RadioRxStatus* status)
{
    memcpy(status, &_platform_radio_rxStatus, sizeof(RadioRxStatus));
}

/**
 * (API function)
 * Implements Radio API's <code>radioSetRxHandler()</code>
 */
void radioSetRxHandler(RadioRxHandler rxHandler)
{
    m_receiveFnPtr = rxHandler;
}

/**
 * (API function)
 * Implements Radio API's <code>radioRequestTx()</code>
 */
RadioStatus radioRequestTx(
        Address dst,
        MessageType type,
        const void *msg,
        uint8_t len,
        RadioTxDone txDone)
{
    return radioRequestTxWithHeader(dst, type, NULL, 0, msg, len, txDone);
}

/**
 * (API function)
 * Implements Radio API's <code>radioRequestTx()</code>
 */
RadioStatus radioRequestTxWithHeader(
        Address dst,
        MessageType type,
        const void *header,
        uint8_t headerLen,
        const void *msg,
        uint8_t len,
        RadioTxDone txDone)
{
    uint8_t totalLen = len + headerLen;

    // Sanity check; preserve one byte for checksum
    if (totalLen > (PLATFORM_MAX_MSG_LEN-1))
        return RADIO_FAILED;

    // See if there is enough space in the buffer
    if (cirbufFree(&m_txbuf) < 
            sizeof(Address) + sizeof(MessageType) + sizeof(RadioTxDone) +
            sizeof(uint8_t) + totalLen)
    {
        return RADIO_FAILED;
    }

    // Append request to the tx buffer
    cirbufWrite(&m_txbuf, &dst     , sizeof(Address));
    cirbufWrite(&m_txbuf, &type    , sizeof(MessageType));
    cirbufWrite(&m_txbuf, &txDone  , sizeof(RadioTxDone));
    cirbufWrite(&m_txbuf, &totalLen, sizeof(uint8_t));
    cirbufWrite(&m_txbuf, header   , headerLen);
    cirbufWrite(&m_txbuf, msg      , len);

    return RADIO_OK;
}

/**
 * Start transmitting a message over radio.  This function assumes that the
 * radio is not currently sending and sanity check of the request has been
 * checked a priori
 */
static void radioStartTx(
        Address dst,
        MessageType type,
        const void *msg,
        uint8_t len,
        RadioTxDone txDone)
{
    MessageHeader hdr;

    m_sendDoneFnPtr = txDone;
    m_status.sending = 1;  // No more send request allowed until done

    // Prepare MoteLib's message header
#ifdef TINYOS_IFRAME_TYPE
    hdr.network = TINYOS_6LOWPAN_NETWORK_ID;
#endif
    hdr.type = type;
    hdr.seqno = m_currentFrameId++;

    _platform_radio_send(dst, &hdr, msg, len);
}

/**
 * Checks TX queue and begin transmitting if the radio is available
 * @param status Status of the most recent transmission
 */
void _sys_radio_pollTxQueue()
{
    Address     dst;
    MessageType type;
    uint8_t     len, i, checksum;
    RadioTxDone txDone;
    char        buf[PLATFORM_MAX_MSG_LEN];

    // Return immediately if radio is sending or the queue is empty
    if (m_status.sending || cirbufEmpty(&m_txbuf)) return;

    // Get next request from the circular buffer and start transmitting
    cirbufRead(&m_txbuf, &dst   , sizeof(Address));
    cirbufRead(&m_txbuf, &type  , sizeof(MessageType));
    cirbufRead(&m_txbuf, &txDone, sizeof(RadioTxDone));
    cirbufRead(&m_txbuf, &len   , sizeof(uint8_t));
    cirbufRead(&m_txbuf, buf    , len);

    checksum = type;
    for (i = 0; i < len; i++)
    {
        checksum += buf[i];
    }
    buf[len] = -checksum;

    radioStartTx(dst, type, buf, len+1, txDone);
}
