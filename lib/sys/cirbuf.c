#include <string.h>

#include <motelib/system.h>
#include <motelib/cirbuf.h>

///////////////////
void cirbufInit(CirBuf *cb, void *buffer, uint16_t size)
{
    // make size a power of two for better performance
    size >>= 1;
    size |= (size >> 1);
    size |= (size >> 2);
    size |= (size >> 4);
    size |= (size >> 8);
    size += 1;

    cb->firstFree = 0;
    cb->free      = size;
    cb->size      = size;
    cb->buffer    = buffer;
}

///////////////////
uint8_t cirbufWriteByte(CirBuf *cb, uint8_t data)
{
    if (cirbufFull(cb)) return 0;
    cb->free--;
    cb->buffer[cb->firstFree] = data;
    cb->firstFree++;
    cb->firstFree &= (cb->size-1); // eqv. to modulo cb->size
    return 1;
}

///////////////////
uint16_t cirbufWrite(CirBuf *cb, const void *data, uint16_t len)
{
    uint16_t bytesToBoundary;

    // overflow protection
    if (cb->free < len) len = cb->free;

    if (len == 0) return 0;

    // Make sure we are not writing beyond the buffer boundary
    bytesToBoundary = cb->size - cb->firstFree;

    if (bytesToBoundary >= len)
        memcpy(&cb->buffer[cb->firstFree], data, len);
    else
    {
        // split write into two chunks
        // (1) from first-free to boundary
        memcpy(&cb->buffer[cb->firstFree], data, bytesToBoundary);

        // (2) from 0 to remaining bytes (after wrap-around)
        memcpy(cb->buffer, data+bytesToBoundary, len-bytesToBoundary);
    }

    // Update properties accordingly
    cb->free -= len;
    // As long as cb->size is a power of two, the following statement is
    // equivalent to:
    //    cb->firstFree = (cb->firstFree + len) % cb->size;
    cb->firstFree = (cb->firstFree + len) & (cb->size-1);

    return len;
}

///////////////////
uint8_t cirbufReadByte(CirBuf *cb)
{
    uint16_t firstIndex;
    if (cirbufEmpty(cb)) return 0;
    firstIndex = (cb->firstFree + cb->free) & (cb->size-1);
    cb->free++;
    return cb->buffer[firstIndex];
}

///////////////////
uint16_t cirbufRead(CirBuf *cb, void *data, uint16_t len)
{
    uint16_t firstIndex;
    uint16_t bytesToBoundary;

    // limit read to only stored bytes
    if ( (cb->size - cb->free) < len ) len = cb->size - cb->free; 
    if (len == 0) return 0;

    // Locate the first stored byte
    // As long as cb->size is a power of two, the following statement is
    // equivalent to:
    //    firstIndex = (cb->firstFree + cb->free) % cb->size;
    firstIndex = (cb->firstFree + cb->free) & (cb->size - 1);

    // Make sure we are not writing beyond the buffer boundary
    bytesToBoundary = cb->size - firstIndex;

    // Store contents in the provided memory location only when it is not NULL
    if (data)
    {
        if (bytesToBoundary >= len)
            memcpy(data, &cb->buffer[firstIndex], len);
        else
        {
            // split read into two chunks
            // (1) from first-free to boundary
            memcpy(data, &cb->buffer[firstIndex], bytesToBoundary);

            // (2) from 0 to remaining bytes (after wrap-around)
            memcpy(data+bytesToBoundary, cb->buffer, len-bytesToBoundary);
        }
    }

    // Update properties accordingly
    cb->free += len;

    return len;
}

///////////////////
void cirbufClear(CirBuf *cb)
{
    cb->firstFree = 0;
    cb->free      = cb->size;
}
